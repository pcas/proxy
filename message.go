// Message is used to communicate with a client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package proxy

import (
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/ulid"
	"encoding/json"
	"fmt"
	"sort"
	"time"
)

// Message is the unstructured message type.
type Message map[string]interface{}

//////////////////////////////////////////////////////////////////////
// Message functions
//////////////////////////////////////////////////////////////////////

// OnlyKeys asserts that only (a subset of) the given keys are set.
func (m Message) OnlyKeys(k ...string) error {
	if len(m) == 0 {
		return nil
	} else if len(k) == 0 {
		for key := range m {
			return fmt.Errorf("key '%s' is invalid", key)
		}
	}
	keys := make([]string, len(k))
	copy(keys, k)
	sort.Strings(keys)
	for key := range m {
		idx := sort.SearchStrings(keys, key)
		if idx == len(keys) || keys[idx] != key {
			return fmt.Errorf("key '%s' is invalid", key)
		}
	}
	return nil
}

// RequireKeys asserts that the given keys are set.
func (m Message) RequireKeys(k ...string) error {
	if len(k) == 0 {
		return nil
	} else if len(m) == 0 {
		return fmt.Errorf("missing key '%s'", k[0])
	}
	for _, key := range k {
		if _, ok := m[key]; !ok {
			return fmt.Errorf("missing key '%s'", key)
		}
	}
	return nil
}

// Keys returns the keys, sorted in lex order.
func (m Message) Keys() []string {
	if m == nil {
		return nil
	}
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return keys
}

// ToMessage attempts to return the entry for the given key as a Message. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToMessage(key string) (Message, bool, error) {
	// Sanity check
	if m == nil {
		return nil, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return nil, false, nil
	}
	// Attempt the conversion
	mm, ok := x.(map[string]interface{})
	if !ok {
		return nil, true, fmt.Errorf("unable to convert value for '%s' to a Message", key)
	}
	return Message(mm), true, nil
}

// ToMessageSlice attempts to return the entry for the given key as a []Message. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToMessageSlice(key string) ([]Message, bool, error) {
	// Sanity check
	if m == nil {
		return nil, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return nil, false, nil
	}
	// Is this already a slice of messages?
	if S, ok := x.([]Message); ok {
		return S, true, nil
	}
	// Since we're typically coming via JSON, allow for a slice of interfaces
	if T, ok := x.([]interface{}); ok {
		S := make([]Message, 0, len(T))
		for i, y := range T {
			s, ok := y.(map[string]interface{})
			if !ok {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of Messages: error converting entry %d", key, i)
			}
			S = append(S, Message(s))
		}
		return S, true, nil
	}
	// Give up
	return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of Messages", key)
}

// ToString attempts to return the entry for the given key as a string. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToString(key string) (string, bool, error) {
	// Sanity check
	if m == nil {
		return "", false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return "", false, nil
	}
	// Attempt the conversion
	s, err := convert.ToString(x)
	if err != nil {
		return "", true, fmt.Errorf("unable to convert value for '%s' to a string: %w", key, err)
	}
	return s, true, nil
}

// ToStringSlice attempts to return the entry for the given key as a []string. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToStringSlice(key string) ([]string, bool, error) {
	// Sanity check
	if m == nil {
		return nil, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return nil, false, nil
	}
	// Is this already a slice of strings?
	if S, ok := x.([]string); ok {
		return S, true, nil
	}
	// Since we're typically coming via JSON, allow for a slice of interfaces
	if T, ok := x.([]interface{}); ok {
		S := make([]string, 0, len(T))
		for i, y := range T {
			s, err := convert.ToString(y)
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of strings: error converting entry %d: %w", key, i, err)
			}
			S = append(S, s)
		}
		return S, true, nil
	}
	// Give up
	return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of strings", key)
}

// ToInt attempts to return the entry for the given key as an int. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToInt(key string) (int, bool, error) {
	// Sanity check
	if m == nil {
		return 0, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return 0, false, nil
	}
	// Allow for the possibility that x is a json.Number
	if k, ok := x.(json.Number); ok {
		k, err := k.Int64()
		if err != nil {
			return 0, true, fmt.Errorf("unable to convert value for '%s' to an int: %w", key, err)
		}
		n, err := convert.ToInt(k)
		if err != nil {
			return 0, true, fmt.Errorf("unable to convert value for '%s' to an int: %w", key, err)
		}
		return n, true, nil
	}
	// Attempt the conversion
	n, err := convert.ToInt(x)
	if err != nil {
		return 0, true, fmt.Errorf("unable to convert value for '%s' to an int: %w", key, err)
	}
	return n, true, nil
}

// ToIntSlice attempts to return the entry for the given key as a []int. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToIntSlice(key string) ([]int, bool, error) {
	// Sanity check
	if m == nil {
		return nil, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return nil, false, nil
	}
	// Is this already a slice of ints?
	if S, ok := x.([]int); ok {
		return S, true, nil
	}
	// Is this a slice of json.Numbers?
	if T, ok := x.([]json.Number); ok {
		S := make([]int, 0, len(T))
		for i, y := range T {
			s, err := y.Int64()
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of int: error converting entry %d: %w", key, i, err)
			}
			n, err := convert.ToInt(s)
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of int: error converting entry %d: %w", key, i, err)
			}
			S = append(S, n)
		}
		return S, true, nil
	}
	// Since we're typically coming via JSON, allow for a slice of interfaces
	if T, ok := x.([]interface{}); ok {
		S := make([]int, 0, len(T))
		for i, y := range T {
			s, err := convert.ToInt(y)
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of ints: error converting entry %d: %w", key, i, err)
			}
			S = append(S, s)
		}
		return S, true, nil
	}
	// Give up
	return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of ints", key)
}

// ToInt64 attempts to return the entry for the given key as an int64. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToInt64(key string) (int64, bool, error) {
	// Sanity check
	if m == nil {
		return 0, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return 0, false, nil
	}
	// Allow for the possibility that x is a json.Number
	if k, ok := x.(json.Number); ok {
		n, err := k.Int64()
		if err != nil {
			return 0, true, fmt.Errorf("unable to convert value for '%s' to an int64: %w", key, err)
		}
		return n, true, nil
	}
	// Attempt the conversion
	n, err := convert.ToInt64(x)
	if err != nil {
		return 0, true, fmt.Errorf("unable to convert value for '%s' to an int64: %w", key, err)
	}
	return n, true, nil
}

// ToInt64Slice attempts to return the entry for the given key as a []int64. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToInt64Slice(key string) ([]int64, bool, error) {
	// Sanity check
	if m == nil {
		return nil, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return nil, false, nil
	}
	// Is this already a slice of int64s?
	if S, ok := x.([]int64); ok {
		return S, true, nil
	}
	// Is this a slice of ints?
	if T, ok := x.([]int); ok {
		S := make([]int64, 0, len(T))
		for _, s := range T {
			S = append(S, int64(s))
		}
		return S, true, nil
	}
	// Is this a slice of json.Numbers?
	if T, ok := x.([]json.Number); ok {
		S := make([]int64, 0, len(T))
		for i, y := range T {
			s, err := y.Int64()
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of int64s: error converting entry %d: %w", key, i, err)
			}
			S = append(S, s)
		}
		return S, true, nil
	}
	// Since we're typically coming via JSON, allow for a slice of interfaces
	if T, ok := x.([]interface{}); ok {
		S := make([]int64, 0, len(T))
		for i, y := range T {
			s, err := convert.ToInt64(y)
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of int64s: error converting entry %d: %w", key, i, err)
			}
			S = append(S, s)
		}
		return S, true, nil
	}
	// Give up
	return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of int64s", key)
}

// ToFloat64 attempts to return the entry for the given key as a float64. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToFloat64(key string) (float64, bool, error) {
	// Sanity check
	if m == nil {
		return 0, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return 0, false, nil
	}
	// Allow for the possibility that x is a json.Number
	if k, ok := x.(json.Number); ok {
		n, err := k.Float64()
		if err != nil {
			return 0, true, fmt.Errorf("unable to convert value for '%s' to a float64: %w", key, err)
		}
		return n, true, nil
	}
	// Attempt the conversion
	n, err := convert.ToFloat64(x)
	if err != nil {
		return 0, true, fmt.Errorf("unable to convert value for '%s' to a float64: %w", key, err)
	}
	return n, true, nil
}

// ToFloat64Slice attempts to return the entry for the given key as a []float64. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToFloat64Slice(key string) ([]float64, bool, error) {
	// Sanity check
	if m == nil {
		return nil, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return nil, false, nil
	}
	// Is this already a slice of float64s?
	if S, ok := x.([]float64); ok {
		return S, true, nil
	}
	// Is this a slice of json.Numbers?
	if T, ok := x.([]json.Number); ok {
		S := make([]float64, 0, len(T))
		for i, y := range T {
			s, err := y.Float64()
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of float64s: error converting entry %d: %w", key, i, err)
			}
			S = append(S, s)
		}
		return S, true, nil
	}
	// Since we're typically coming via JSON, allow for a slice of interfaces
	if T, ok := x.([]interface{}); ok {
		S := make([]float64, 0, len(T))
		for i, y := range T {
			s, err := convert.ToFloat64(y)
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of float64s: error converting entry %d: %w", key, i, err)
			}
			S = append(S, s)
		}
		return S, true, nil
	}
	// Give up
	return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of float64s", key)
}

// ToBool attempts to return the entry for the given key as a boolean. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToBool(key string) (bool, bool, error) {
	// Sanity check
	if m == nil {
		return false, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return false, false, nil
	}
	// Attempt the conversion
	b, err := convert.ToBool(x)
	if err != nil {
		return false, true, fmt.Errorf("unable to convert value for '%s' to a bool: %w", key, err)
	}
	return b, true, nil
}

// ToBoolSlice attempts to return the entry for the given key as a []bool. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
func (m Message) ToBoolSlice(key string) ([]bool, bool, error) {
	// Sanity check
	if m == nil {
		return nil, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return nil, false, nil
	}
	// Is this already a slice of bools?
	if S, ok := x.([]bool); ok {
		return S, true, nil
	}
	// Since we're typically coming via JSON, allow for a slice of interfaces
	if T, ok := x.([]interface{}); ok {
		S := make([]bool, 0, len(T))
		for i, y := range T {
			s, err := convert.ToBool(y)
			if err != nil {
				return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of bools: error converting entry %d: %w", key, i, err)
			}
			S = append(S, s)
		}
		return S, true, nil
	}
	// Give up
	return nil, true, fmt.Errorf("unable to convert value for '%s' to a slice of bools", key)
}

// ToDuration attempts to return the entry for the given key as a time.Duration. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
//
// A Duration is defined by the JSON:
//
//	integer|string
//
// In the case where Duration is given by an integer, it gives the number of nanoseconds in the duration. In the case where Duration is given by a string, this will be converted to a time.Duration via time.ParseDuration. See [time#ParseDuration] for details on the format.
func (m Message) ToDuration(key string) (time.Duration, bool, error) {
	// Sanity check
	if m == nil {
		return 0, false, nil
	}
	// Recover the entry
	x, ok := m[key]
	if !ok {
		return 0, false, nil
	}
	// Allow for the possibility that x is a json.Number
	if k, ok := x.(json.Number); ok {
		n, err := k.Int64()
		if err != nil {
			return 0, true, fmt.Errorf("unable to convert value for '%s' to a time.Duration: %w", key, err)
		}
		return time.Duration(n), true, nil
	}
	// If x is a string then attempt to parse it
	if s, ok := x.(string); ok {
		d, err := time.ParseDuration(s)
		if err != nil {
			return 0, true, fmt.Errorf("unable to convert value for '%s' to a time.Duration: %w", key, err)
		}
		return d, true, nil
	}
	// We fall back to attempting to convert x to an int64
	n, err := convert.ToInt64(x)
	if err != nil {
		return 0, true, fmt.Errorf("unable to convert value for '%s' to a time.Duration: %w", key, err)
	}
	return time.Duration(n), true, nil
}

// ToULID attempts to return the entry for the given key as a ulid.ULID. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion. Any record will be validated before being returned.
//
// A ULID is defined by the JSON:
//
//	string
//
// which must consist of exactly 26 characters taken from the alphabet:
//
//	0123456789ABCDEFGHJKMNPQRSTVWXYZ
//
// A ULID is used to encode a ulid.ULID: see [bitbucket.org/pcastools/ulid].
func (m Message) ToULID(key string) (ulid.ULID, bool, error) {
	s, ok, err := m.ToString(key)
	if err != nil || !ok {
		return ulid.Nil, ok, err
	}
	u, err := ulid.FromString(s)
	if err != nil {
		return ulid.Nil, true, fmt.Errorf("invalid ULID '%s': %w", key, err)
	}
	return u, true, nil
}
