/*
Package version is an endpoint for version information.

# Operations

We describe the operations permitted by this endpoint.

# get_version

The Arguments are empty. The Result is of the form:

	{
		"GoArch": string,
		"GoOS": string,
		"Major": integer,
		"Minor": integer,
		"Patch": integer,
		"GitCommit": string,
		"GitDirty": boolean,
		"Date": integer
	}

The values of GoArch and GoOS are equal to the constants [runtime.GOARCH] and [runtime.GOOS], respectively. For the remaining values, see [bitbucket.org/pcastools/version] for details. Here Date is given by the nanoseconds elapsed since January 1, 1970 UTC (i.e. as a Unix time).
*/
package version

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
