// version defines the endpoint for version data

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package version

import (
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/version"
	"context"
	"fmt"
	"runtime"
)

// endpoint is the version endpoint.
type endpoint int

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init registers this endpoint.
func init() {
	proxy.Description{
		Name: "version",
		Operations: []string{
			"get_version",
		},
		Create: func(_ context.Context, _ *proxy.Config, _ log.Interface) (proxy.Endpoint, error) {
			return endpoint(0), nil
		},
	}.Register()
}

// getVersion handles get_version requests.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"GoArch": string,
//		"GoOS": string,
//		"Major": integer,
//		"Minor": integer,
//		"Patch": integer,
//		"GitCommit": string,
//		"GitDirty": boolean,
//		"Date": integer
//	}
//
// The values of GoArch and GoOS are equal to the constants [runtime.GOARCH] and [runtime.GOOS], respectively. For the remaining values, see [bitbucket.org/pcastools/version] for details. Here Date is given by the nanoseconds elapsed since January 1, 1970 UTC (i.e. as a Unix time).
func getVersion(args proxy.Message) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	v := version.GetInfo()
	return proxy.Message{
		"GoArch":    runtime.GOARCH,
		"GoOS":      runtime.GOOS,
		"Major":     v.Major,
		"Minor":     v.Minor,
		"Patch":     v.Patch,
		"GitCommit": v.GitCommit,
		"GitDirty":  v.GitDirty,
		"Date":      v.Date.UnixNano(),
	}, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// Close closes the endpoint.
func (e endpoint) Close() error {
	return nil
}

// Handle handles requests for operation 'op' with arguments 'args'.
func (e endpoint) Handle(_ context.Context, op string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	switch op {
	case "get_version":
		return getVersion(args)
	default:
		return nil, fmt.Errorf("unknown operation: %s", op)
	}
}
