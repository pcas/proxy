// pcas-magma is a wrapper that allows access to the pcas infrastructure (logging, etc.) from Magma.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bufio"
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	// Register the proxy endpoints
	_ "bitbucket.org/pcas/proxy/fs"
	_ "bitbucket.org/pcas/proxy/gob"
	_ "bitbucket.org/pcas/proxy/kvdb"
	_ "bitbucket.org/pcas/proxy/log"
	_ "bitbucket.org/pcas/proxy/metrics"
	_ "bitbucket.org/pcas/proxy/mongodb"
	_ "bitbucket.org/pcas/proxy/mysql"
	_ "bitbucket.org/pcas/proxy/postgres"
	_ "bitbucket.org/pcas/proxy/queue"
	_ "bitbucket.org/pcas/proxy/rangedb"
	_ "bitbucket.org/pcas/proxy/sqlite"
	_ "bitbucket.org/pcas/proxy/version"
)

// magmaCmd is the process that runs magma
var magmaCmd *exec.Cmd

// once makes sure we only call the shutdown code once.
var once sync.Once

//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////

// stringsToIntegerPair takes a slice of two strings and attempts to
// convert them to integers, returning the result
func stringsToIntegerPair(S []string) (a int, b int, err error) {
	if len(S) != 2 {
		err = errors.New("the slice should have two elements")
	} else if a, err = strconv.Atoi(S[0]); err != nil {
		a = 0
	} else if b, err = strconv.Atoi(S[1]); err != nil {
		b = 0
	}
	return
}

// childProcesses returns a slice containing the PIDs of all processes
// with PPID p.  Any errors are logged to lg.
func childProcesses(lg log.Interface, p int) []int {
	lg = log.PrefixWith(lg, "[childProcesses]")
	psCmd := exec.Command("ps", "-opid", "-oppid")
	psStdout, err := psCmd.StdoutPipe()
	if err != nil {
		lg.Printf("Error getting stdout from ps: %v", err)
		return nil
	} else if err := psCmd.Start(); err != nil {
		lg.Printf("Error starting ps command: %v", err)
		return nil
	}
	lg.Printf("Looking for children of parent process %v", p)
	// read and verify the header
	scanner := bufio.NewScanner(psStdout)
	if !scanner.Scan() {
		lg.Printf("Empty output from ps command")
		return nil
	}
	if err := scanner.Err(); err != nil {
		lg.Printf("Error whilst reading header from ps command: %v", err)
		return nil
	}
	if fields := strings.Fields(scanner.Text()); len(fields) != 2 ||
		fields[0] != "PID" ||
		fields[1] != "PPID" {
		lg.Printf("Unexpected header format from ps: %v", fields)
		return nil
	}
	// read the rest of the output
	pids := make([]int, 0, 4)
	for scanner.Scan() {
		fields := strings.Fields(scanner.Text())
		pid, ppid, err := stringsToIntegerPair(fields)
		if err != nil {
			lg.Printf("Error converting %v to integer pair: %v", fields, err)
			return nil
		} else if ppid != p {
			// the PPID doesn't match, so skip this line
			continue
		} else {
			pids = append(pids, pid)
		}
	}
	if err := scanner.Err(); err != nil {
		lg.Printf("Error scanning output from ps: %v", err)
		return nil
	}
	lg.Printf("Found child processes: %v", pids)
	return pids
}

// registerMagmaCleanup registers a cleanup function to kill the magma process.
func registerMagmaCleanup(lg log.Interface) {
	cleanup.Add(func() error {
		if magmaCmd != nil && magmaCmd.Process != nil {
			lg.Printf("Killing magma process")
			// kill all the child processes
			for _, pid := range childProcesses(lg, magmaCmd.Process.Pid) {
				lg.Printf("Sending SIGTERM to Magma child process %v", pid)
				syscall.Kill(pid, syscall.SIGTERM)
			}
			lg.Printf("Sending SIGTERM to main Magma process")
			magmaCmd.Process.Signal(syscall.SIGTERM)
		}
		return nil
	})
}

func runMagma(port int, path string, lg log.Interface) <-chan struct{} {
	// Create the done channel
	doneC := make(chan struct{})
	// Prepare the magma command, connecting its stdin, stdout, and stderr to
	// the corresponding os.std*
	magmaCmd = exec.Command(path, flag.Args()...)
	magmaCmd.Env = append(os.Environ(), fmt.Sprintf("PCAS_MAGMA_PORT=%d", port))
	magmaCmd.Stdin = os.Stdin
	magmaCmd.Stdout = os.Stdout
	magmaCmd.Stderr = os.Stderr
	// Register the cleanup function to shutdown magma
	registerMagmaCleanup(lg)
	// Launch magma in a new go routine
	go func(doneC chan<- struct{}) {
		defer close(doneC)
		lg.Printf("Starting magma")
		now := time.Now()
		err := magmaCmd.Run()
		if err != nil {
			lg.Printf("Magma exited after %s with error: %v", time.Since(now), err)
		} else {
			lg.Printf("Magma exited after %s", time.Since(now))
		}
	}(doneC)
	// Return the done channel
	return doneC
}

//////////////////////////////////////////////////////////////////////
// signal handlers
//////////////////////////////////////////////////////////////////////

// sigtermHandler calls cancel on SIGTERM.
func sigtermHandler(cancel context.CancelFunc, lg log.Interface) {
	c := make(chan os.Signal, 1)
	go func() {
		<-c
		signal.Stop(c)
		lg.Printf("Received SIGTERM")
		cancel()
	}()
	signal.Notify(c, syscall.SIGTERM)
}

// sigintHandler passes SIGINT on to Magma.
func sigintHandler(lg log.Interface) {
	c := make(chan os.Signal, 32)
	go func() {
		for sig := range c {
			lg.Printf("Received SIGINT")
			if magmaCmd != nil && magmaCmd.Process != nil {
				if err := magmaCmd.Process.Signal(sig); err != nil {
					lg.Printf("Error passing SIGINT to Magma: %v", err)
				}
			}
		}
	}()
	signal.Notify(c, syscall.SIGINT)
}

//////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handlers
	if opts.Daemon {
		cleanup.CancelOnSignal(cancel, lg)
	} else {
		sigtermHandler(cancel, lg)
		sigintHandler(lg)
	}
	// Recover and log any server panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Start listening on a random port
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		return fmt.Errorf("error creating TCP listener: %w", err)
	}
	// Run Magma and bind the done channel to the cancel function
	if !opts.Daemon {
		doneC := runMagma(l.Addr().(*net.TCPAddr).Port, opts.MagmaPath, lg)
		defer contextutil.NewChannelLink(doneC, cancel).Stop()
	}
	// Build the options
	proxyOpts := []proxy.Option{
		proxy.Log(lg),
	}
	if opts.SSLDisabled {
		proxyOpts = append(proxyOpts, proxy.SSLDisabled())
	} else {
		proxyOpts = append(proxyOpts, proxy.SSLCert(opts.SSLCert))
	}
	// Run the proxy
	return proxy.Run(ctx, l, proxyOpts...)
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
