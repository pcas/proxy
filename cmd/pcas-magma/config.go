// Config handles configuration and flag-parsing

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/version"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

// Options describes the options.
type Options struct {
	Daemon      bool   // Run as a daemon?
	MagmaPath   string // The path to magma
	SSLCert     []byte // The SSL certificate
	SSLDisabled bool   // Is SSL disabled?
}

// The default path to magma.
const (
	DefaultMagmaPath = ""
)

// Name is the name of the executable
var Name = "pcas-magma"

//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse and validate the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		MagmaPath: getMagmaPath(DefaultMagmaPath),
	}
}

// getMagmaPath returns the path to magma, or the value def (for default) if magma cannot be found.
func getMagmaPath(def string) string {
	// try to read MAGMA_PATH from the environment
	if s, ok := os.LookupEnv("MAGMA_PATH"); ok {
		return s
	}
	// try to find it on PATH
	if s, err := exec.LookPath("magma"); err == nil {
		return s
	}
	return def
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	var logToStderr bool
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s is a wrapper that allows access to the pcas infrastructure (logging, etc.) from Magma.\n\nUsage: %s [options]", Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Bool("daemon", &opts.Daemon, opts.Daemon, "Run as daemon", ""),
		flag.String("magma-path", &opts.MagmaPath, opts.MagmaPath, "The path to Magma", "The default value of the flag -magma-path can be specified using the environment variable MAGMA_PATH."),
		flag.Bool("log-to-stderr", &logToStderr, false, "Log to stderr", ""),
		&version.Flag{AppName: Name},
	)
	// Add the proxy flag set
	flag.AddSets(proxy.FlagSets()...)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Parse the flags
	flag.Parse()
	// Sanity check
	if !opts.Daemon && strings.TrimSpace(opts.MagmaPath) == "" {
		return errors.New("cannot find magma")
	}
	// Recover the SSL client details
	opts.SSLDisabled = sslClientSet.Disabled()
	opts.SSLCert = sslClientSet.Certificate()
	// Set the loggers
	if logToStderr {
		log.SetLogger(log.Stderr)
	}
	return nil
}
