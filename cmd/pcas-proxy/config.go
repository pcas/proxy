// Config handles configuration and logging for pcas-proxy.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/proxy/cmd/pcas-proxy/service"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"context"
	"fmt"
	"math"
	"os"
	"time"
)

// Options describes the configuration options.
type Options struct {
	SSLDisabled       bool   // Is SSL disabled for outgoing connections?
	SSLCert           []byte // The SSL certificate for outgoing connections
	SSLKeyCert        []byte // The server's SSL (public) certificate for incoming connections
	SSLKey            []byte // The server's SSL (private) key for incoming connections
	MaxNumConnections int    // The maximum number of incoming connections
}

// Name is the name of the executable.
const Name = "pcas-proxy"

// The default values for the arguments.
const (
	DefaultMaxNumConnections = 4096
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Return the default options
	return &Options{
		MaxNumConnections: DefaultMaxNumConnections,
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if opts.MaxNumConnections <= 0 || opts.MaxNumConnections > math.MaxInt32 {
		return fmt.Errorf("invalid maximum number of connections (%d)", opts.MaxNumConnections)
	}
	return nil
}

// proxyFlagSet returns the flag set for the proxy options.
func proxyFlagSet() flag.Set {
	s := flag.NewBasicSet("Proxy options", service.Flags()...)
	s.SetUsageFooter("The value of the flags -*-bind-address and -*-server-address should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\".")
	return s
}

// setLoggers starts the loggers as indicated by the flags.
func setLoggers(sslClientSet *sslflag.ClientSet, logdSet *logdflag.LogSet) error {
	// Note the client SSL settings
	sslDisabled := sslClientSet.Disabled()
	cert := sslClientSet.Certificate()
	// Note the loggers we'll use
	toStderr := logdSet.ToStderr()
	toServer := logdSet.ToServer()
	// Create the config
	c := logdSet.ClientConfig()
	c.SSLDisabled = sslDisabled
	c.SSLCert = cert
	// Create a context with 10 second timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	// Start the loggers
	return logdflag.SetLogger(ctx, toStderr, toServer, c, Name)
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf(`%s provides a proxy for the pcas infrastructure (logging etc.).

Usage: %s [options]`, Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Int("max-num-connections", &opts.MaxNumConnections, opts.MaxNumConnections, "The maximum number of connections", ""),
		&version.Flag{AppName: Name},
	)
	// Create and add the proxy flag set
	flag.AddSet(proxyFlagSet())
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the the standard SSL client set
	sslClientSet := &sslflag.ClientSet{}
	flag.AddSet(sslClientSet)
	// Create and add the standard SSL server set
	sslServerSet := &sslflag.ServerSet{}
	flag.AddSet(sslServerSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL client details
	opts.SSLDisabled = sslClientSet.Disabled()
	opts.SSLCert = sslClientSet.Certificate()
	// Recover the SSL server details
	opts.SSLKey = sslServerSet.Key()
	opts.SSLKeyCert = sslServerSet.Certificate()
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Set the loggers
	return setLoggers(sslClientSet, logdSet)
}
