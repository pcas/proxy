// Monitor provides the monitor service.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package monitor

import (
	"bitbucket.org/pcas/logger/monitor/monitord"
	"bitbucket.org/pcas/proxy/cmd/pcas-proxy/service"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/log"
	"context"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the service.
func init() {
	if err := (&service.Info{
		Name:           "monitor",
		DefaultTCPPort: monitord.DefaultTCPPort,
		DefaultWSPort:  monitord.DefaultWSPort,
		Create:         create,
		Addr:           monitord.DefaultConfig().Address,
		Env:            "PCAS_MONITOR_ADDRESS",
	}).Register(); err != nil {
		panic(err)
	}
}

// create creates a monitord server.
func create(ctx context.Context, opts *service.Options, lg log.Interface) (grpcutil.Server, error) {
	// Create the client config
	cfg := monitord.DefaultConfig()
	cfg.Address = opts.Address
	cfg.SSLDisabled = opts.SSLDisabled
	cfg.SSLCert = opts.SSLCert
	// Create the client
	c, err := monitord.NewClient(ctx, cfg)
	if err != nil {
		return nil, err
	}
	c.SetLogger(log.PrefixWith(lg, "[client]"))
	// Register a cleanup function for the client
	cleanup.Add(c.Close)
	// Build the server options
	serverOpts := []monitord.Option{
		monitord.Connection(c),
	}
	if len(opts.SSLKey) == 0 {
		lg.Printf("SSL is disabled: connections to this server are not encrypted")
	} else {
		serverOpts = append(serverOpts, monitord.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Create the server
	s, err := monitord.NewServer(serverOpts...)
	if err != nil {
		return nil, err
	}
	s.SetLogger(log.PrefixWith(lg, "[server]"))
	// Return the server
	return s, nil
}
