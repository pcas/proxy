// Metrics provides the metrics service.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metrics

import (
	"bitbucket.org/pcas/metrics/metricsdb"
	"bitbucket.org/pcas/proxy/cmd/pcas-proxy/service"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/log"
	"context"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the service.
func init() {
	if err := (&service.Info{
		Name:           "metrics",
		DefaultTCPPort: metricsdb.DefaultTCPPort,
		DefaultWSPort:  metricsdb.DefaultWSPort,
		Create:         create,
		Addr:           metricsdb.DefaultConfig().Address,
		Env:            "PCAS_METRICS_ADDRESS",
	}).Register(); err != nil {
		panic(err)
	}
}

// create creates a metricsdb server.
func create(ctx context.Context, opts *service.Options, lg log.Interface) (grpcutil.Server, error) {
	// Create the client config
	cfg := metricsdb.DefaultConfig()
	cfg.Address = opts.Address
	cfg.SSLDisabled = opts.SSLDisabled
	cfg.SSLCert = opts.SSLCert
	// Create the client
	c, err := metricsdb.NewClient(ctx, cfg)
	if err != nil {
		return nil, err
	}
	c.SetLogger(log.PrefixWith(lg, "[client]"))
	// Register a cleanup function for the client
	cleanup.Add(c.Close)
	// Build the server options
	serverOpts := []metricsdb.Option{
		metricsdb.Metrics(c),
	}
	if len(opts.SSLKey) == 0 {
		lg.Printf("SSL is disabled: connections to this server are not encrypted")
	} else {
		serverOpts = append(serverOpts, metricsdb.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Create the server
	s, err := metricsdb.NewServer(serverOpts...)
	if err != nil {
		return nil, err
	}
	s.SetLogger(log.PrefixWith(lg, "[server]"))
	// Return the server
	return s, nil
}
