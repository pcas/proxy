// Kvdb provides the kvdb service.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdb

import (
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/proxy/cmd/pcas-proxy/service"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/log"
	"context"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the service.
func init() {
	if err := (&service.Info{
		Name:           "kvdb",
		DefaultTCPPort: kvdb.DefaultTCPPort,
		DefaultWSPort:  kvdb.DefaultWSPort,
		Create:         create,
		Addr:           kvdb.DefaultConfig().Address,
		Env:            "PCAS_KVDB_ADDRESS",
	}).Register(); err != nil {
		panic(err)
	}
}

// create creates a kvdb server.
func create(ctx context.Context, opts *service.Options, lg log.Interface) (grpcutil.Server, error) {
	// Create the client config
	cfg := kvdb.DefaultConfig()
	cfg.Address = opts.Address
	cfg.SSLDisabled = opts.SSLDisabled
	cfg.SSLCert = opts.SSLCert
	// Create the client
	c, err := kvdb.Open(ctx, cfg)
	if err != nil {
		return nil, err
	}
	// Register a cleanup function for the client
	cleanup.Add(c.Close)
	// Build the server options
	serverOpts := []kvdb.Option{
		kvdb.Connection(c),
	}
	if len(opts.SSLKey) == 0 {
		lg.Printf("SSL is disabled: connections to this server are not encrypted")
	} else {
		serverOpts = append(serverOpts, kvdb.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Create the server
	s, err := kvdb.New(serverOpts...)
	if err != nil {
		return nil, err
	}
	s.SetLogger(log.PrefixWith(lg, "[server]"))
	// Return the server
	return s, nil
}
