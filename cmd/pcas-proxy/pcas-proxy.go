// Pcas-proxy implements a proxy for the pcas infrastructure.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/proxy/cmd/pcas-proxy/service"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"os"

	// Register the services
	_ "bitbucket.org/pcas/proxy/cmd/pcas-proxy/fs"
	_ "bitbucket.org/pcas/proxy/cmd/pcas-proxy/kvdb"
	_ "bitbucket.org/pcas/proxy/cmd/pcas-proxy/log"
	_ "bitbucket.org/pcas/proxy/cmd/pcas-proxy/metrics"
	_ "bitbucket.org/pcas/proxy/cmd/pcas-proxy/monitor"
	_ "bitbucket.org/pcas/proxy/cmd/pcas-proxy/queue"
	_ "bitbucket.org/pcas/proxy/cmd/pcas-proxy/rangedb"
)

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer func() {
		if e := cleanup.Run(); err == nil {
			err = e
		}
	}()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Recover and log any server panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("panic in main: %v", e)
			}
		}
	}()
	// Build the options
	serviceOpts := []service.Option{
		service.SSLCert(opts.SSLCert),
		service.MaxNumConnections(opts.MaxNumConnections),
		service.Log(lg),
	}
	if opts.SSLDisabled {
		serviceOpts = append(serviceOpts, service.SSLDisabled())
	}
	if len(opts.SSLKey) != 0 {
		serviceOpts = append(serviceOpts, service.SSLCertAndKey(opts.SSLKeyCert, opts.SSLKey))
	}
	// Run the services
	return service.Serve(ctx, serviceOpts...)
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
