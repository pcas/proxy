// Serviceoptions provides configuration options for the services.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package service

import (
	"bitbucket.org/pcastools/log"
	"errors"
)

// Option sets options on a server.
type Option interface {
	apply(*servicesOptions) error
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*servicesOptions) error
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *servicesOptions) error {
	return h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*servicesOptions) error) *funcOption {
	return &funcOption{
		f: f,
	}
}

// servicesOptions are the options for the services.
type servicesOptions struct {
	SSLDisabled       bool          // Is SSL disabled for outgoing connections?
	SSLCert           []byte        // The SSL certificate for outgoing connections
	SSLKeyCert        []byte        // The server's SSL (public) certificate for incoming connections
	SSLKey            []byte        // The server's SSL (private) key for incoming connections
	MaxNumConnections int           // The max number of incoming connections on a socket
	Log               log.Interface // The log
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// copyByteSlice returns a copy of the given slice.
func copyByteSlice(S []byte) []byte {
	T := make([]byte, len(S))
	copy(T, S)
	return T
}

/////////////////////////////////////////////////////////////////////////
// servicesOptions functions
/////////////////////////////////////////////////////////////////////////

// CreateOptions returns the corresponding Options.
func (opts *servicesOptions) Options() *Options {
	return &Options{
		SSLCert:    copyByteSlice(opts.SSLCert),
		SSLKeyCert: copyByteSlice(opts.SSLKeyCert),
		SSLKey:     copyByteSlice(opts.SSLKey),
	}
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseServicesOptions parses the given optional functions.
func parseServicesOptions(options ...Option) (*servicesOptions, error) {
	// Create the default options
	opts := &servicesOptions{
		Log: log.Discard,
	}
	// Set the options
	for _, h := range options {
		if err := h.apply(opts); err != nil {
			return nil, err
		}
	}
	// Loogs good
	return opts, nil
}

// SSLDisabled marks SSL as disabled for outgoing connections.
func SSLDisabled() Option {
	return newFuncOption(func(opts *servicesOptions) error {
		opts.SSLDisabled = true
		return nil
	})
}

// SSLCert adds the given SSL certificate for outgoing connections.
func SSLCert(crt []byte) Option {
	crt = copyByteSlice(crt)
	return newFuncOption(func(opts *servicesOptions) error {
		opts.SSLCert = crt
		return nil
	})
}

// SSLCertAndKey adds the given SSL public certificate and private key pair for incoming connections.
func SSLCertAndKey(crt []byte, key []byte) Option {
	// Copy the certificate and key
	crt, key = copyByteSlice(crt), copyByteSlice(key)
	// Return the option
	return newFuncOption(func(opts *servicesOptions) error {
		if len(crt) == 0 {
			return errors.New("missing SSL certificate")
		} else if len(key) == 0 {
			return errors.New("missing SSL private key")
		}
		opts.SSLKeyCert = crt
		opts.SSLKey = key
		return nil
	})
}

// MaxNumConnections sets the maximum number of incoming connections per socket.
func MaxNumConnections(n int) Option {
	return newFuncOption(func(opts *servicesOptions) error {
		if n < 0 {
			return errors.New("maximum number of connections must be a non-negative integer")
		}
		opts.MaxNumConnections = n
		return nil
	})
}

// Log sets the destination log.
func Log(lg log.Interface) Option {
	return newFuncOption(func(opts *servicesOptions) error {
		if lg == nil {
			lg = log.Discard
		}
		opts.Log = lg
		return nil
	})
}
