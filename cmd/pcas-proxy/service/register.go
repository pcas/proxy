// Service defines a proxy service.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package service

import (
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"net"
	"sort"
)

// Info is the info describing a service.
type Info struct {
	Name           string           // The service name
	DefaultTCPPort int              // The default TCP port
	DefaultWSPort  int              // The default websocket port
	Create         CreateServerFunc // The creation func
	Addr           *address.Address // The default server address
	Env            string           // The env name used to set the server address
}

// registeredServices is the map of registered services.
var registeredServices map[string]*service

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialises the map.
func init() {
	registeredServices = make(map[string]*service)
}

// serve starts the service svr listening over l.
func serve(svr grpcutil.Server, l net.Listener, lg log.Interface) error {
	lg.Printf("Started serving")
	err := svr.Serve(l)
	if err != nil {
		lg.Printf("Stopped serving with error: %v", err)
	} else {
		lg.Printf("Stopped serving")
	}
	return err
}

// gracefulStop stops the services gracefully. It stops the services from accepting new connections and blocks until all pending tasks are finished. This should only be called after the flags have been parsed.
func gracefulStop() {
	// Create the communication channel
	doneC := make(chan struct{})
	// Ask each service to stop
	n := 0
	for _, s := range registeredServices {
		n++
		go func(s *service) {
			s.GracefulStop()
			doneC <- struct{}{}
		}(s)
	}
	// Wait for the services to stop
	for n != 0 {
		n--
		<-doneC
	}
}

/////////////////////////////////////////////////////////////////////////
// Info functions
/////////////////////////////////////////////////////////////////////////

// Validate validates the data.
func (info *Info) Validate() error {
	if info == nil {
		return errors.New("illegal nil-valued info")
	} else if len(info.Name) == 0 {
		return errors.New("the name must not be empty")
	} else if info.DefaultTCPPort < 1 || info.DefaultTCPPort > 65535 {
		return fmt.Errorf("the default TCP port number (%d) must be between 1 and 65535", info.DefaultTCPPort)
	} else if info.DefaultWSPort < 1 || info.DefaultWSPort > 65535 {
		return fmt.Errorf("the default web socket port number (%d) must be between 1 and 65535", info.DefaultWSPort)
	} else if info.Create == nil {
		return errors.New("the creation function must not be nil")
	} else if info.Addr == nil {
		return errors.New("the address must not be nil")
	}
	// Looks good
	return nil
}

// Register registers a service.
func (info *Info) Register() error {
	// Create the service
	s, err := newService(info)
	if err != nil {
		return err
	}
	// Register the service
	if _, ok := registeredServices[info.Name]; ok {
		return errors.New("service already registered")
	}
	registeredServices[info.Name] = s
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Flags returns the flags for the services.
func Flags() []flag.Flag {
	// Build the slice of flags
	flags := make([]flag.Flag, 0, 3*len(registeredServices))
	for _, s := range registeredServices {
		flags = append(flags, s.Flags()...)
	}
	// Sort the flags in lex order
	sort.Slice(flags, func(i int, j int) bool {
		return flags[i].Name() < flags[j].Name()
	})
	return flags
}

// Serve starts the services accepting connections. Serve blocks until an error occurs, or until the context fires (which triggers a graceful shutdown). This should only be called after the flags have been parsed.
func Serve(ctx context.Context, options ...Option) error {
	// Parse the options
	opts, err := parseServicesOptions(options...)
	if err != nil {
		return err
	}
	// If the context fires stop the servers
	go func() {
		<-ctx.Done()
		gracefulStop()
	}()
	// Create a communication channel
	errC := make(chan error, len(registeredServices))
	// Start the services
	n := 0
	for _, s := range registeredServices {
		// Is the service enabled?
		if s.IsEnabled() {
			// Set the logger
			s.SetLogger(log.PrefixWith(opts.Log, "[%s]", s.Name()))
			// Recover the listener
			l, err := s.Listener(opts)
			if err != nil {
				return err
			}
			// Defer closing the listener
			defer l.Close() // Ignore any error
			// Recover the server
			svr, err := s.Server(ctx, opts)
			if err != nil {
				return err
			}
			// Start serving
			n++
			go func(svr grpcutil.Server, l net.Listener, lg log.Interface) {
				errC <- serve(svr, l, lg)
			}(svr, l, s.Log())
		}
	}
	// Start the web servers
	go listenAndServe(opts) // Ignore any error
	// Return the first error
	for n != 0 {
		n--
		if err := <-errC; err != nil {
			return err
		}
	}
	return nil
}
