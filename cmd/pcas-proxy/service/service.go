// Service defines a proxy service.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package service

import (
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/listenutil"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"
	"sync"
)

// Options are the options passed to a server on creation.
type Options struct {
	Address     *address.Address // The address
	SSLDisabled bool             // Is SSL disabled for outgoing connections?
	SSLCert     []byte           // The SSL certificate for outgoing connections
	SSLKeyCert  []byte           // The server's SSL (public) certificate for incoming connections
	SSLKey      []byte           // The server's SSL (private) key for incoming connections
}

// CreateServerFunc is a function to create a server.
type CreateServerFunc func(context.Context, *Options, log.Interface) (grpcutil.Server, error)

// service represents a service.
type service struct {
	log.BasicLogable
	name           string           // The service name
	defaultTCPPort int              // The default TCP port
	defaultWSPort  int              // The default web socket port
	create         CreateServerFunc // The creation func
	enabled        bool             // Is this service enabled
	bindAddr       *address.Address // The bind address
	serverAddr     *address.Address // The server address
	serverEnv      string           // The server environment variable name
	m              sync.Mutex       // Controls access to the following
	l              net.Listener     // The listener for this service
	svr            grpcutil.Server  // The server for this service
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// addressWithPort ensures that the address a has a port. If not, the default port from s is used.
func addressWithPort(s *service, a *address.Address) *address.Address {
	// Is there anything to do?
	if a.HasPort() {
		return a
	}
	// Create the port number for this scheme
	var port int
	switch a.Scheme() {
	case "ws":
		port = s.DefaultWSPort()
	default:
		port = s.DefaultTCPPort()
	}
	// Build the new uri
	uri := a.Scheme() + "://" + a.Hostname() + ":" + strconv.Itoa(port) + a.EscapedPath()
	// Return the modified address
	newaddr, err := address.New(uri)
	if err != nil {
		return a
	}
	return newaddr
}

// addressEnvUsage returns a standard usage message for the address flag -f that reads its default value from the environment variable env.
func addressEnvUsage(f string, env string) string {
	var status string
	// read the environment variable and try to parse it
	val, ok := os.LookupEnv(env)
	if ok {
		_, err := address.New(val)
		if err == nil {
			status = fmt.Sprintf("current value \"%s\", which is valid", val)
		} else {
			status = fmt.Sprintf("current value \"%s\", which is invalid", val)
		}
	} else {
		status = "currently unset"
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (%s).", f, env, status)
}

// createTCPListener returns a new TCP listener.
func createTCPListener(a *address.Address, defaultPort int, maxNumConnections int, lg log.Interface) (net.Listener, error) {
	port := defaultPort
	if a.HasPort() {
		port = a.Port()
	}
	return listenutil.TCPListener(a.Hostname(), port,
		listenutil.MaxNumConnections(maxNumConnections),
		listenutil.Log(lg),
	)
}

// createWebsocketListener returns a new websocket listener.
func createWebsocketListener(a *address.Address, defaultPort int, lg log.Interface) net.Listener {
	port := defaultPort
	if a.HasPort() {
		port = a.Port()
	}
	return getServer(a.Hostname(), port).CreateListener(a.EscapedPath(), lg)
}

/////////////////////////////////////////////////////////////////////////
// service functions
/////////////////////////////////////////////////////////////////////////

// newService returns a new service with the given data.
func newService(info *Info) (*service, error) {
	// Sanity check
	if err := info.Validate(); err != nil {
		return nil, err
	}
	// Create the default bind address
	bindAddr, err := address.New("ws://localhost:" + strconv.Itoa(info.DefaultWSPort) + "/" + info.Name)
	if err != nil {
		return nil, fmt.Errorf("unable to create default bind address: %w", err)
	}
	// Create the service
	return &service{
		name:           info.Name,
		defaultTCPPort: info.DefaultTCPPort,
		defaultWSPort:  info.DefaultWSPort,
		create:         info.Create,
		bindAddr:       bindAddr,
		serverAddr:     info.Addr,
		serverEnv:      info.Env,
	}, nil
}

// Name returns the service name.
func (s *service) Name() string {
	if s == nil {
		return ""
	}
	return s.name
}

// DefaultTCPPort returns the default TCP port.
func (s *service) DefaultTCPPort() int {
	if s == nil {
		return 0
	}
	return s.defaultTCPPort
}

// DefaultWSPort returns the default web socket port.
func (s *service) DefaultWSPort() int {
	if s == nil {
		return 0
	}
	return s.defaultWSPort
}

// Flags returns the flags for this service.
func (s *service) Flags() []flag.Flag {
	// Sanity check
	if s == nil {
		return nil
	}
	// Create the usage text for the server flag
	var usage string
	if len(s.serverEnv) != 0 {
		usage = addressEnvUsage(s.Name()+"-server-address", s.serverEnv)
	}
	// Create the flags
	return []flag.Flag{
		flag.Bool(
			s.Name(),
			&s.enabled, s.enabled,
			"Enable the "+s.Name()+" proxy",
			"",
		),
		address.NewFlag(
			s.Name()+"-bind-address",
			&s.bindAddr, s.BindAddress(),
			"The "+s.Name()+" bind address",
			"",
		),
		address.NewFlag(
			s.Name()+"-server-address",
			&s.serverAddr, s.ServerAddress(),
			"The "+s.Name()+" server address",
			usage,
		),
	}
}

// BindAddress returns the bind address for this service. This should only be called after the flag has been parsed.
func (s *service) BindAddress() *address.Address {
	if s == nil {
		return nil
	}
	return addressWithPort(s, s.bindAddr)
}

// ServerAddress returns the server address for this service. This should only be called after the flag has been parsed.
func (s *service) ServerAddress() *address.Address {
	if s == nil {
		return nil
	}
	return addressWithPort(s, s.serverAddr)
}

// IsEnabled returns true iff this service is enabled. This should only be called after the flag has been parsed.
func (s *service) IsEnabled() bool {
	return s != nil && s.enabled
}

// Listener returns the listener for this service. This should only be called after the flag has been parsed.
func (s *service) Listener(opts *servicesOptions) (net.Listener, error) {
	// Sanity check
	if !s.IsEnabled() {
		return nil, errors.New("service not enabled")
	}
	// Acquire a lock
	s.m.Lock()
	defer s.m.Unlock()
	// Have we already created a listener?
	if s.l != nil {
		return s.l, nil
	}
	// Provide some logging feedback
	a := s.BindAddress()
	s.Log().Printf("Starting listener on %s", a.URI())
	// Create the listener
	var l net.Listener
	var err error
	switch a.Scheme() {
	case "tcp":
		l, err = createTCPListener(a, s.DefaultTCPPort(), opts.MaxNumConnections, s.Log())
	case "ws":
		l = createWebsocketListener(a, s.DefaultWSPort(), log.PrefixWith(s.Log(), "[websocket]"))
	default:
		err = errors.New("unsupported URI scheme: " + a.Scheme())
	}
	if err != nil {
		s.Log().Printf("Error starting listener: %v", err)
		return nil, err
	}
	s.Log().Printf("Listening on %s", a)
	// Cache the listener and return
	s.l = l
	return l, nil
}

// Server returns the server for this service. This should only be called after the flag has been parsed.
func (s *service) Server(ctx context.Context, opts *servicesOptions) (grpcutil.Server, error) {
	// Sanity check
	if !s.IsEnabled() {
		return nil, errors.New("service not enabled")
	}
	// Acquire a lock
	s.m.Lock()
	defer s.m.Unlock()
	// Have we already created a server?
	if s.svr != nil {
		return s.svr, nil
	}
	// Create the options
	sopts := opts.Options()
	sopts.Address = s.ServerAddress()
	// Create the server
	svr, err := s.create(ctx, sopts, s.Log())
	if err != nil {
		return nil, err
	}
	// Cache the server and return
	s.svr = svr
	return svr, nil
}

// GracefulStop stops the service gracefully.
func (s *service) GracefulStop() {
	// Sanity check
	if !s.IsEnabled() {
		return
	}
	// Acquire a lock
	s.m.Lock()
	defer s.m.Unlock()
	// Stop the server
	if s.svr != nil {
		s.Log().Printf("Stopping service...")
		s.svr.GracefulStop()
		s.Log().Printf("Service stopped")
	}
}
