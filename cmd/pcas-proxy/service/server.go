// Server defines a http.Server registration system.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package service

import (
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/listenutil"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// shutdownTimeout is the length of time we allow a server for a graceful shutdown.
const shutdownTimeout = 3 * time.Second

// server represents a http.Server with WSListener listeners.
type server struct {
	*http.Server
	mux      *http.ServeMux
	register []*listenutil.WSListener
}

// registeredServers is a map of registered servers. Keys are hostname:port pairs.
var registeredServers map[string]*server

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init initialises the map.
func init() {
	registeredServers = make(map[string]*server)
}

// webserverListener returns a TCP listener
func webserverListener(addr string, maxNumConnections int, lg log.Interface) (net.Listener, error) {
	// Recover the address and port number
	idx := strings.IndexByte(addr, ':')
	if idx == -1 {
		return nil, errors.New("no port number specified")
	} else if idx == len(addr)-1 {
		return nil, errors.New("unable to parse port number")
	}
	address := addr[:idx]
	port, err := strconv.Atoi(addr[idx+1:])
	if err != nil {
		return nil, fmt.Errorf("unable to parse port number: %w", err)
	}
	// Start a listener
	return listenutil.TCPListener(address, port,
		listenutil.MaxNumConnections(maxNumConnections),
		listenutil.Log(lg),
	)
}

// listenAndServe listens and serves for each server.
func listenAndServe(opts *servicesOptions) error {
	// Create a communication channel
	n := len(registeredServers)
	errC := make(chan error, n)
	// Start listen and serve for each server
	for _, s := range registeredServers {
		go func(s *server, maxNumConnections int, lg log.Interface) {
			// Create the listener
			l, err := webserverListener(s.Addr, maxNumConnections, lg)
			if err != nil {
				errC <- err
				return
			}
			// Start serving
			lg.Printf("HTTP server listening on %s", s.Addr)
			err = s.Serve(l)
			if err == http.ErrServerClosed {
				lg.Printf("HTTP server closed on %s", s.Addr)
			} else {
				lg.Printf("HTTP server closed on %s with error: %v", s.Addr, err)
			}
			errC <- err
		}(s, opts.MaxNumConnections, opts.Log)
	}
	// Return the first non-trivial error
	for i := 0; i < n; i++ {
		if err := <-errC; err != http.ErrServerClosed {
			return err
		}
	}
	return http.ErrServerClosed
}

/////////////////////////////////////////////////////////////////////////
// server function
/////////////////////////////////////////////////////////////////////////

// getServer returns the server for given hostname and port.
func getServer(hostname string, port int) *server {
	// Create the address
	addr := hostname + ":" + strconv.Itoa(port)
	// Do we already have this server in the cache?
	s, ok := registeredServers[addr]
	if !ok {
		// Create a new server
		mux := http.NewServeMux()
		s = &server{
			Server: &http.Server{
				Addr:    addr,
				Handler: mux,
			},
			mux: mux,
		}
		// Register a shutdown function for cleanup
		cleanup.Add(s.shutdownFunc())
		// Cache this server
		registeredServers[addr] = s
	}
	// Return the server
	return s
}

// shutdownFunc returns a function to shutdown the server.
func (s *server) shutdownFunc() func() error {
	return func() error {
		ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()
		if err := s.Shutdown(ctx); err != nil {
			s.Close() // Ignore any error
		}
		return nil
	}
}

// CreateListener registers a web socket listener with the server on the given path.
func (s *server) CreateListener(path string, lg log.Interface) net.Listener {
	l, handler := listenutil.WebsocketListener(lg)
	s.mux.HandleFunc(path, handler)
	s.register = append(s.register, l)
	return l
}

// Serve serves over l.
func (s *server) Serve(l net.Listener) error {
	err := s.Server.Serve(l)
	if err != http.ErrServerClosed {
		for _, l := range s.register {
			l.CloseWithErr(err) // Ignore any error
		}
	} else {
		for _, l := range s.register {
			l.Close() // Ignore any error
		}
	}
	return err
}
