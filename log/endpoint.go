// endpoint defines the endpoint for log requests

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package log

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"io"
)

// storage is a [logger.LogMessager] with a close method.
type storage interface {
	logger.LogMessager
	io.Closer
}

// endpoint is the logging endpoint.
type endpoint struct {
	id    string             // The identifier for log messages
	cfg   *logd.ClientConfig // The default client config
	cache *cache             // The cache of open connections
}

// handleLoggerFunc is a handle function that takes a logger.
type handleLoggerFunc func(context.Context, logger.LogMessager, string, proxy.Message, log.Interface) (proxy.Message, error)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init registers this endpoint.
func init() {
	// Create the flag set
	flags := logdflag.NewSet(nil)
	// Register the endpoint
	proxy.Description{
		Name: "log",
		Operations: []string{
			"defaults",
			"log",
		},
		Create: func(ctx context.Context, opts *proxy.Config, lg log.Interface) (proxy.Endpoint, error) {
			// Create the config
			cfg := flags.ClientConfig()
			cfg.SSLDisabled = opts.SSLDisabled
			cfg.SSLCert = opts.SSLCert
			// Return the new endpoint
			return newEndpoint(ctx, cfg, lg)
		},
		FlagSet: flags,
	}.Register()
}

// createConfig modifies (a copy of) the client configuration, with values updated with those in msg.
func createConfig(cfg *logd.ClientConfig, msg proxy.Message) (*logd.ClientConfig, error) {
	// Sanity check on the keys
	if err := msg.OnlyKeys("Address"); err != nil {
		return nil, err
	}
	// Pass to a copy
	cfg = cfg.Copy()
	// Add any options set by the user
	if s, ok, err := msg.ToString("Address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	// Validate the configuration before returning
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// Close closes the endpoint.
func (e *endpoint) Close() error {
	return e.cache.Close()
}

// logger returns the client connection to logd.
func (e *endpoint) logger(ctx context.Context, args proxy.Message) (storage, error) {
	// Extract the connection configuration (if any)
	msg, _, err := args.ToMessage("Connection")
	if err != nil {
		return nil, err
	}
	// Build the config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, err
	}
	// Open the connection to the logger
	lg, err := e.cache.Get(ctx, cfg)
	if err != nil {
		return nil, err
	}
	return lg, nil
}

// withLogger calls the given handle function with logger.
func (e *endpoint) withLogger(ctx context.Context, f handleLoggerFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Fetch the logger
	l, err := e.logger(ctx, args)
	if err != nil {
		return nil, err
	}
	defer l.Close()
	// Call the handle function
	return f(ctx, l, e.id, args, lg)
}

// defaults returns the default backend-specific Connection values for this endpoint.
//
// The Connection values JSON is of the form:
//
//	{
//		"Address": string
//	}
func (e *endpoint) defaults() proxy.Message {
	return proxy.Message{
		"Address": e.cfg.Address.URI(),
	}
}

// Handle handles requests for operation 'op' with arguments 'args'.
func (e *endpoint) Handle(ctx context.Context, op string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	switch op {
	case "defaults":
		return defaults(ctx, args, e.defaults, lg)
	case "log":
		return e.withLogger(ctx, handleLog, args, lg)
	default:
		return nil, fmt.Errorf("unknown operation: %s", op)
	}
}

// newEndpoint returns a new endpoint using the given default config.
func newEndpoint(ctx context.Context, cfg *logd.ClientConfig, lg log.Interface) (proxy.Endpoint, error) {
	return &endpoint{
		id:    logdflag.DefaultIdentifier(),
		cfg:   cfg,
		cache: newCache(log.PrefixWith(lg, "[cache]")),
	}, nil
}
