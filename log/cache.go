// Cache defines a cache of logd client connections.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package log

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/logd"
	stdCache "bitbucket.org/pcas/proxy/internal/cache"
	"bitbucket.org/pcastools/log"
	"context"
)

// value is a value in the cache.
type value struct {
	storage
	release func() // The release function
}

// cache is a cache of storage accessed by their config.
type cache struct {
	cache *stdCache.Cache[*logd.ClientConfig, *logger.Buffer]
}

//////////////////////////////////////////////////////////////////////
// value functions
//////////////////////////////////////////////////////////////////////

// Close closes the value.
func (v *value) Close() error {
	v.release()
	return nil
}

//////////////////////////////////////////////////////////////////////
// cache functions
//////////////////////////////////////////////////////////////////////

// Close closes the cache.
func (c *cache) Close() error {
	return c.cache.Close()
}

// Get returns a storage with given config.
func (c *cache) Get(ctx context.Context, cfg *logd.ClientConfig) (storage, error) {
	e, err := c.cache.Get(ctx, cfg)
	if err != nil {
		return nil, err
	}
	return &value{
		storage: e.Value(),
		release: e.Release,
	}, nil
}

// newCache returns a new connection cache.
func newCache(lg log.Interface) *cache {
	return &cache{
		cache: stdCache.New(
			func(ctx context.Context, cfg *logd.ClientConfig) (*logger.Buffer, error) {
				// Open a connection to the logger
				c, err := logd.NewClient(ctx, cfg)
				if err != nil {
					return nil, err
				}
				// Wrap the connection in a buffer
				return logger.NewBuffer(c), nil
			},
			logd.AreEqual,
			lg,
		),
	}
}
