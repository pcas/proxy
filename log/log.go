// log defines the handlers for log requests

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package log

import (
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
)

// defaultsFunc returns the defaults for connections on this endpoint.
type defaultsFunc func() proxy.Message

//////////////////////////////////////////////////////////////////////
// handler functions
//////////////////////////////////////////////////////////////////////

// defaults handles defaults requests.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Connection": Connection
//	}
//
// The result describes the default connection values.
func defaults(_ context.Context, args proxy.Message, df defaultsFunc, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	return proxy.Message{"Connection": df()}, nil
}

// handleLog handles log requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Name": string,			// required
//		"Message": string		// required
//	}
//
// This will log the given message in the named log. The Result is empty.
func handleLog(ctx context.Context, l logger.LogMessager, id string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Name", "Message"); err != nil {
		return nil, err
	}
	// Fetch the name
	name, ok, err := args.ToString("Name")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Name'")
	}
	// Fetch the message
	message, ok, err := args.ToString("Message")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Message'")
	}
	// Log the message
	return nil, l.LogMessage(ctx, logger.Message{
		Identifier: id,
		LogName:    name,
		Message:    message,
	})
}
