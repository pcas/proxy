/*
Package log is an endpoint for log messages.

# Types

First we describe the JSON types specific to this endpoint.

# Connection

The Connection object describes the connection details to logd:

	{
		"Address": string
	}

Any values set in Connection are optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation. For a description of the meaning of these settings, see [bitbucket.org/pcas/logger/logd].

# Operations

Now we describe the operations permitted by this endpoint.

# defaults

The Arguments are empty. The Result is of the form:

	{
		"Connection": Connection
	}

The result describes the default connection values.

# log

The Arguments are of the form:

	{
		"Connection": Connection,
		"Name": string,			// required
		"Message": string		// required
	}

This will log the given message in the named log. The Result is empty.
*/
package log

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
