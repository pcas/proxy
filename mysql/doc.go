/*
Package mysql is an endpoint for the mysql key-value driver. See [bitbucket.org/pcas/keyvalue/mysql]. Mysql is a concrete implementation of the [bitbucket.org/pcas/proxy/keyvalue] abstract endpoint.

# Types

We describe the JSON types specific to this endpoint.

# Connection

The Connection object describes the connection details to mysql:

	{
		"Host": string,
		"Username": string,
		"Password": string,
		"PasswordSet": boolean,
		"ConnectTimeout": integer,
		"RequireSSL": boolean
	}

Any values set in Connection are optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation. For a description of the meaning of these settings, see [bitbucket.org/pcas/keyvalue/mysql].
*/
package mysql

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
