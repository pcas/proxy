// Proxy provides a proxy that allows computer algebra systems to communicate with the pcas infrastructure.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package proxy

import (
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/rand"
	"context"
	"io"
	"net"
	"sort"
)

// CreationFunc is the creation function for an endpoint.
type CreationFunc func(context.Context, *Config, log.Interface) (Endpoint, error)

// Endpoint is an endpoint.
type Endpoint interface {
	io.Closer
	// Handle handles requests for operation 'op' with arguments 'args'.
	Handle(ctx context.Context, op string, args Message, lg log.Interface) (Message, error)
}

// Description describes an endpoint
type Description struct {
	Name       string       // The name of the endpoint
	Operations []string     // The supported operations
	Create     CreationFunc // The creation function for the endpoint
	Flag       flag.Flag    // An optional flag
	FlagSet    flag.Set     // An optional flag.Set
}

// descriptions is a map from endpoint names to descriptions.
var descriptions = make(map[string]Description)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// endpointNames returns a slice containing the names of all registered endpoints, sorted lexicographically by Name.
func endpointNames() []string {
	// Copy the names into a slice
	names := make([]string, 0, len(descriptions))
	for _, d := range descriptions {
		names = append(names, d.Name)
	}
	// Sort the slice
	sort.Strings(names)
	return names
}

// isContextErr returns true iff the given error is a context error.
func isContextErr(err error) bool {
	return err != nil && (err == context.Canceled || err == context.DeadlineExceeded)
}

// closeListener closes the listener when doneC fires. The result is logged to lg.
func closeListener(l net.Listener, doneC <-chan struct{}, lg log.Interface) {
	<-doneC
	err := l.Close()
	if err != nil {
		lg.Printf("Listener closed with error: %v", err)
	} else {
		lg.Printf("Listener closed")
	}
}

// handleConnection handles a connection from a client.
func handleConnection(ctx context.Context, conn net.Conn, cfg *Config, lg log.Interface) {
	// Log this connection
	lg.Printf("Connection accepted from %s -> %s", conn.RemoteAddr(), conn.LocalAddr())
	// Defer closing the connection
	defer func() {
		if err := conn.Close(); err != nil {
			lg.Printf("Connection closed with error: %v", err)
		} else {
			lg.Printf("Connection closed")
		}
	}()
	// Create the new handler
	h, err := newHandler(conn, cfg, lg)
	if err != nil {
		lg.Printf("Error creating connection handler: %v", err)
		return
	}
	defer h.Close()
	// Handle the connection
	if err := h.Run(ctx); !isContextErr(err) {
		lg.Printf("Error handling connection: %v", err)
	}
}

//////////////////////////////////////////////////////////////////////
// Description functions
//////////////////////////////////////////////////////////////////////

// Register registers an endpoint handler.
func (d Description) Register() {
	// Sanity check
	if len(d.Name) == 0 {
		panic("the endpoint name must not be empty")
	} else if d.Create == nil {
		panic("the creation function must not be nil")
	}
	// Check for a duplicate endpoint
	if _, ok := descriptions[d.Name]; ok {
		panic("duplicate endpoint")
	}
	// Make a copy of the operations and sort them
	ops := make([]string, len(d.Operations))
	copy(ops, d.Operations)
	sort.Strings(ops)
	d.Operations = ops
	// Register the endpoint
	descriptions[d.Name] = d
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// FlagSets returns the slice of flagsets for each registered endpoint.
func FlagSets() []flag.Set {
	// Get the names
	names := endpointNames()
	// We collect any endpoint flags into our own flag set
	T := make([]flag.Flag, 0, len(names))
	// Make the slice of flagsets
	S := make([]flag.Set, 1, len(names)+1)
	for _, name := range names {
		d := descriptions[name]
		if d.FlagSet != nil {
			S = append(S, d.FlagSet)
		}
		if d.Flag != nil {
			T = append(T, d.Flag)
		}
	}
	// Prepend the proxy set, if necessary
	if len(T) != 0 {
		S[0] = flag.NewBasicSet("Proxy Options", T...)
	} else {
		S = S[1:]
	}
	// Return the sets
	return S
}

// Run listens for and handles connections, terminating when the context fires. Upon return the listener will be closed.
func Run(ctx context.Context, l net.Listener, options ...Option) error {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return err
	}
	// Make a note of the logger and the configuration
	lg, cfg := opts.Log, opts.Config()
	// When the context fires, close the listener
	go closeListener(l, ctx.Done(), lg)
	// Log that we're listening
	lg.Printf("Listening for connections on %s", l.Addr())
	// Listen for connections
	for {
		// Wait for a connection
		if conn, err := l.Accept(); err != nil {
			select {
			case <-ctx.Done():
				return ctx.Err()
			default:
			}
			lg.Printf("Error accepting connection: %v", err)
		} else {
			// Handle the connection
			clg := log.PrefixWith(lg, "[conn=0x%s]", rand.ID8())
			go handleConnection(ctx, conn, cfg, clg)
		}
	}
}
