// combinediterator provides a way to combine an iterator with a table.Close.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"context"
	"sync"
)

// combinedIterator combines an iterator with the connections that keep it alive.
type combinedIterator struct {
	itr      record.Iterator     // The wrapped iterator
	t        keyvalue.Table      // The underlying table
	db       keyvalue.Database   // The underlying database
	c        keyvalue.Connection // The underlying connection
	m        sync.Mutex          // Mutex protects the following
	isClosed bool                // Have we closed?
	closeErr error               // The error on close, if any
}

//////////////////////////////////////////////////////////////////////
// combinedIterator functions
//////////////////////////////////////////////////////////////////////

// newCombinedIterator returns a new combined iterator that keeps the underlying connections alive for the lifetime of the iterator.
func newCombinedIterator(c keyvalue.Connection, db keyvalue.Database, t keyvalue.Table, itr record.Iterator) record.Iterator {
	return &combinedIterator{
		itr: itr,
		t:   t,
		db:  db,
		c:   c,
	}
}

// closeWithLock closes the iterator. Assumes that the caller hold a lock on the mutex.
func (itr *combinedIterator) closeWithLock() error {
	// Is there anything to do?
	if !itr.isClosed {
		// Mark us as closed
		itr.isClosed = true
		// Close the iterator
		itr.closeErr = itr.itr.Close()
		// Close the connections
		itr.t.Close()  // Ignore any error
		itr.db.Close() // Ignore any error
		itr.c.Close()  // Ignore any error
	}
	// Return any error
	return itr.closeErr
}

// Close closes the iterator.
func (itr *combinedIterator) Close() error {
	itr.m.Lock()
	defer itr.m.Unlock()
	return itr.closeWithLock()
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *combinedIterator) Err() error {
	// Acquire a lock
	itr.m.Lock()
	defer itr.m.Unlock()
	// Check for errors
	err := itr.itr.Err()
	if err == nil && itr.isClosed {
		err = itr.closeErr
	}
	return err
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *combinedIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *combinedIterator) NextContext(ctx context.Context) (bool, error) {
	// Acquire a lock
	itr.m.Lock()
	defer itr.m.Unlock()
	// Advance the iterator
	ok, err := itr.itr.NextContext(ctx)
	// If an error occurred, or if we've reached the end of iteration, then
	// we close the iterator and associated connections
	if err != nil || !ok {
		itr.closeWithLock() // Ignore any errors
	}
	return ok, err
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (itr *combinedIterator) Scan(dest record.Record) error {
	itr.m.Lock()
	defer itr.m.Unlock()
	return itr.itr.Scan(dest)
}
