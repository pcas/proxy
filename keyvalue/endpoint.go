// endpoint provides a proxy.Endpoint for keyvalue connections.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/parse"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"context"
	"errors"
	"fmt"
	"io"
	"sync"
)

// CreationFunc is the creation function for a keyvalue endpoint.
type CreationFunc func(context.Context, *proxy.Config, log.Interface) (Endpoint, error)

// Endpoint is a keyvalue endpoint.
type Endpoint interface {
	io.Closer
	// Connection returns a connection using the given options.
	Connection(context.Context, proxy.Message) (keyvalue.Connection, error)
	// Defaults returns the default values for this endpoint.
	Defaults() proxy.Message
}

// Description describes an endpoint.
type Description struct {
	Name    string       // The name of the endpoint
	Create  CreationFunc // The creation function for the endpoint
	Flag    flag.Flag    // An optional flag
	FlagSet flag.Set     // An optional flag.Set
}

// endpoint wraps a keyvalue Endpoint to implement a proxy.Endpoint.
type endpoint struct {
	e    Endpoint                      // The endpoint
	lg   log.Interface                 // The logger
	m    sync.Mutex                    // Mutex protecting the following
	inst map[ulid.ULID]*insertStream   // A map of open insert streams
	itrs map[ulid.ULID]record.Iterator // A map of open iterators
}

// connHandleFunc is a handle function that takes a keyvalue.Connection.
type connHandleFunc func(context.Context, keyvalue.Connection, proxy.Message, log.Interface) (proxy.Message, error)

// dbHandleFunc is a handle function that takes a keyvalue.Database.
type dbHandleFunc func(context.Context, keyvalue.Database, proxy.Message, log.Interface) (proxy.Message, error)

// tableHandleFunc is a handle function that takes a keyvalue.Table.
type tableHandleFunc func(context.Context, keyvalue.Table, proxy.Message, log.Interface) (proxy.Message, error)

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// Close closes the endpoint.
func (e *endpoint) Close() error {
	// Acquire a lock
	e.m.Lock()
	defer e.m.Unlock()
	// Close any open insert streams
	if e.inst != nil {
		for u, itr := range e.inst {
			if err := itr.Close(); err != nil {
				e.Log().Printf("Insert stream %s closed with error: %v", u, err)
			} else {
				e.Log().Printf("Insert stream %s closed", u)
			}
		}
		e.inst = nil
	}
	// Close any open iterators
	if e.itrs != nil {
		for u, itr := range e.itrs {
			if err := itr.Close(); err != nil {
				e.Log().Printf("Iterator %s closed with error: %v", u, err)
			} else {
				e.Log().Printf("Iterator %s closed", u)
			}
		}
		e.itrs = nil
	}
	// Close the underlying endpoint
	return e.e.Close()
}

// connection returns the keyvalue connection.
func (e *endpoint) connection(ctx context.Context, args proxy.Message) (keyvalue.Connection, error) {
	// Extract the connection configuration (if any)
	cfg, ok, err := args.ToMessage("Connection")
	if err != nil {
		return nil, err
	} else if !ok {
		cfg = make(proxy.Message)
	}
	// Open the connection
	c, err := e.e.Connection(ctx, cfg)
	if err != nil {
		return nil, err
	}
	// Set the logger and return
	c.SetLogger(log.PrefixWith(e.Log(), "[connection]"))
	return c, nil
}

// withConnection calls the given handle function with keyvalue.Connection connection.
func (e *endpoint) withConnection(ctx context.Context, f connHandleFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Fetch the connection
	c, err := e.connection(ctx, args)
	if err != nil {
		return nil, err
	}
	defer c.Close()
	// Call the handle function
	return f(ctx, c, args, lg)
}

// withDatabase calls the given handle function with a keyvalue.Database connection.
func (e *endpoint) withDatabase(ctx context.Context, f dbHandleFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Recover the database name
	name, ok, err := args.ToString("Database")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Database'")
	}
	// Wrap the call
	return e.withConnection(ctx, func(ctx context.Context, c keyvalue.Connection, args proxy.Message, lg log.Interface) (proxy.Message, error) {
		// Open the database connection
		db, err := c.ConnectToDatabase(ctx, name)
		if err != nil {
			return nil, err
		}
		defer db.Close()
		// Call the handle function
		return f(ctx, db, args, lg)
	}, args, lg)
}

// withTable calls the given handle function with a keyvalue.Table connection.
func (e *endpoint) withTable(ctx context.Context, f tableHandleFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Recover the table name
	name, ok, err := args.ToString("Table")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Table'")
	}
	// Wrap the call
	return e.withDatabase(ctx, func(ctx context.Context, db keyvalue.Database, args proxy.Message, lg log.Interface) (proxy.Message, error) {
		// Open the table connection
		t, err := db.ConnectToTable(ctx, name)
		if err != nil {
			return nil, err
		}
		defer t.Close()
		// Call the handle function
		return f(ctx, t, args, lg)
	}, args, lg)
}

// closeInsertStream closes the insert stream with given ULID. On success, returns the number of records passed to the stream.
func (e *endpoint) closeInsertStream(ctx context.Context, u ulid.ULID) (int64, error) {
	s, err := func() (*insertStream, error) {
		// Acquire a lock on the insert stream map
		e.m.Lock()
		defer e.m.Unlock()
		// Recover the stream
		if e.inst == nil {
			return nil, fmt.Errorf("unknown ULID: %s", u)
		}
		s, ok := e.inst[u]
		if !ok {
			return nil, fmt.Errorf("unknown ULID: %s", u)
		}
		// Remove and return the stream
		delete(e.inst, u)
		return s, nil
	}()
	// Handle any errors and close the stream
	if err != nil {
		return 0, err
	} else if err = s.CloseWithContext(ctx); err != nil {
		return 0, err
	}
	return s.Count(), nil
}

// insertRecordInInsertStream inserts the given record in the insert stream with given ULID.
func (e *endpoint) insertRecordInInsertStream(ctx context.Context, r record.Record, u ulid.ULID) error {
	s, err := func() (*insertStream, error) {
		// Acquire a lock on the insert stream map
		e.m.Lock()
		defer e.m.Unlock()
		// Recover the stream
		if e.inst == nil {
			return nil, fmt.Errorf("unknown ULID: %s", u)
		}
		s, ok := e.inst[u]
		if !ok {
			return nil, fmt.Errorf("unknown ULID: %s", u)
		}
		// Return the stream
		return s, nil
	}()
	// Handle any errors and insert the record
	if err != nil {
		return err
	}
	return s.Insert(ctx, r)
}

// openInsertStream opens a new insert stream using the given data. The resulting stream is cached on e using the returned ULID, which may be used to access the stream.
func (e *endpoint) openInsertStream(ctx context.Context, args proxy.Message, bufSize int) (u ulid.ULID, err error) {
	// Recover the database name
	var dbName string
	var ok bool
	if dbName, ok, err = args.ToString("Database"); err != nil {
		return
	} else if !ok {
		err = errors.New("missing key 'Database'")
		return
	}
	// Recover the table name
	var tableName string
	if tableName, ok, err = args.ToString("Table"); err != nil {
		return
	} else if !ok {
		err = errors.New("missing key 'Table'")
		return
	}
	// Fetch the connection
	var c keyvalue.Connection
	if c, err = e.connection(ctx, args); err != nil {
		return
	}
	defer func() {
		if err != nil {
			c.Close()
		}
	}()
	// Open the database connection
	var db keyvalue.Database
	if db, err = c.ConnectToDatabase(ctx, dbName); err != nil {
		return
	}
	defer func() {
		if err != nil {
			db.Close()
		}
	}()
	// Open the table connection
	var t keyvalue.Table
	if t, err = db.ConnectToTable(ctx, tableName); err != nil {
		return
	}
	defer func() {
		if err != nil {
			t.Close()
		}
	}()
	// Acquire a lock on the insert stream map
	e.m.Lock()
	defer e.m.Unlock()
	if e.inst == nil {
		e.inst = make(map[ulid.ULID]*insertStream)
	}
	// Generate a unique ULID
	exists := true
	for exists {
		if u, err = ulid.New(); err != nil {
			return
		}
		_, exists = e.inst[u]
	}
	// Finally, add the stream to the map
	e.inst[u] = newInsertStream(c, db, t, bufSize)
	return
}

// closeIterator closes the iterator with given ULID.
func (e *endpoint) closeIterator(u ulid.ULID) error {
	itr, err := func() (record.Iterator, error) {
		// Acquire a lock on the iterator map
		e.m.Lock()
		defer e.m.Unlock()
		// Recover the iterator
		if e.itrs == nil {
			return nil, fmt.Errorf("unknown ULID: %s", u)
		}
		itr, ok := e.itrs[u]
		if !ok {
			return nil, fmt.Errorf("unknown ULID: %s", u)
		}
		// Remove and return the iterator
		delete(e.itrs, u)
		return itr, nil
	}()
	// Handle any errors and close the iterator
	if err != nil {
		return err
	}
	return itr.Close()
}

// advanceIterator advances the iterator with given ULID. The second return value will be true on successful iteration, false if the end of iteration has been reached, or an error occurred.
func (e *endpoint) advanceIterator(ctx context.Context, u ulid.ULID) (record.Record, bool, error) {
	itr, err := func() (record.Iterator, error) {
		// Acquire a lock on the iterator map
		e.m.Lock()
		defer e.m.Unlock()
		// Recover the iterator
		if e.itrs == nil {
			return nil, fmt.Errorf("unknown ULID: %s", u)
		}
		itr, ok := e.itrs[u]
		if !ok {
			return nil, fmt.Errorf("unknown ULID: %s", u)
		}
		// Return the iterator
		return itr, nil
	}()
	// Handle any errors
	if err != nil {
		return nil, false, err
	}
	// Advance the iterator
	ok, err := itr.NextContext(ctx)
	if err != nil || !ok {
		return nil, ok, err
	}
	// Scan the record
	r := record.Record{}
	if err = itr.Scan(r); err != nil {
		return nil, false, err
	}
	return r, true, nil
}

// performSelect performs a select using the given data. The resulting iterator is cached on e using the returned ULID, which may be used to access the iterator.
func (e *endpoint) performSelect(ctx context.Context, args proxy.Message, template record.Record, q *parse.Query, bufSize int) (u ulid.ULID, err error) {
	// Recover the database name
	var dbName string
	var ok bool
	if dbName, ok, err = args.ToString("Database"); err != nil {
		return
	} else if !ok {
		err = errors.New("missing key 'Database'")
		return
	}
	// Recover the table name
	var tableName string
	if tableName, ok, err = args.ToString("Table"); err != nil {
		return
	} else if !ok {
		err = errors.New("missing key 'Table'")
		return
	}
	// Fetch the connection
	var c keyvalue.Connection
	if c, err = e.connection(ctx, args); err != nil {
		return
	}
	defer func() {
		if err != nil {
			c.Close()
		}
	}()
	// Open the database connection
	var db keyvalue.Database
	if db, err = c.ConnectToDatabase(ctx, dbName); err != nil {
		return
	}
	defer func() {
		if err != nil {
			db.Close()
		}
	}()
	// Open the table connection
	var t keyvalue.Table
	if t, err = db.ConnectToTable(ctx, tableName); err != nil {
		return
	}
	defer func() {
		if err != nil {
			t.Close()
		}
	}()
	// Perform the select
	var itr record.Iterator
	if q.HasLimit {
		itr, err = t.SelectWhereLimit(ctx, template, q.Cond, q.Order, q.Limit)
	} else {
		itr, err = t.SelectWhere(ctx, template, q.Cond, q.Order)
	}
	if err != nil {
		return
	}
	// If necessary, buffer the iterator
	if bufSize > 0 {
		itr = record.NewBufferedIterator(itr, bufSize)
	}
	defer func() {
		if err != nil {
			itr.Close()
		}
	}()
	// Acquire a lock on the iterator map
	e.m.Lock()
	defer e.m.Unlock()
	if e.itrs == nil {
		e.itrs = make(map[ulid.ULID]record.Iterator)
	}
	// Generate a unique ULID
	exists := true
	for exists {
		if u, err = ulid.New(); err != nil {
			return
		}
		_, exists = e.itrs[u]
	}
	// Finally, add the iterator to the map
	e.itrs[u] = newCombinedIterator(c, db, t, itr)
	return
}

// Handle handles requests for operation 'op' with arguments 'args'.
func (e *endpoint) Handle(ctx context.Context, op string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	switch op {
	case "defaults":
		return defaults(ctx, args, e.e.Defaults, lg)
	case "close":
		return handleClose(ctx, args, e.closeIterator, lg)
	case "advance":
		return advance(ctx, args, e.advanceIterator, lg)
	case "select":
		return handleSelect(ctx, args, e.performSelect, lg)
	case "select_one":
		return e.withTable(ctx, selectOne, args, lg)
	case "insert":
		return e.withTable(ctx, insert, args, lg)
	case "begin_insert":
		return beginInsert(ctx, args, e.openInsertStream, lg)
	case "insert_record":
		return insertRecord(ctx, args, e.insertRecordInInsertStream, lg)
	case "end_insert":
		return endInsert(ctx, args, e.closeInsertStream, lg)
	case "update":
		return e.withTable(ctx, update, args, lg)
	case "delete":
		return e.withTable(ctx, handleDelete, args, lg)
	case "count":
		return e.withTable(ctx, count, args, lg)
	case "list_databases":
		return e.withConnection(ctx, listDatabases, args, lg)
	case "create_database":
		return e.withConnection(ctx, createDatabase, args, lg)
	case "delete_database":
		return e.withConnection(ctx, deleteDatabase, args, lg)
	case "create_table":
		return e.withDatabase(ctx, createTable, args, lg)
	case "list_tables":
		return e.withDatabase(ctx, listTables, args, lg)
	case "delete_table":
		return e.withDatabase(ctx, deleteTable, args, lg)
	case "rename_table":
		return e.withDatabase(ctx, renameTable, args, lg)
	case "describe_table":
		return e.withTable(ctx, describeTable, args, lg)
	case "add_index":
		return e.withTable(ctx, addIndex, args, lg)
	case "delete_index":
		return e.withTable(ctx, deleteIndex, args, lg)
	case "list_indices":
		return e.withTable(ctx, listIndices, args, lg)
	case "add_keys":
		return e.withTable(ctx, addKeys, args, lg)
	case "delete_keys":
		return e.withTable(ctx, deleteKeys, args, lg)
	default:
		return nil, fmt.Errorf("unknown operation: %s", op)
	}
}

// Log returns the logger for the endpoint.
func (e *endpoint) Log() log.Interface {
	return e.lg
}

// newEndpoint returns a new endpoint for the given data.
func newEndpoint(e Endpoint, lg log.Interface) proxy.Endpoint {
	// Return the endpoint
	return &endpoint{
		e:  e,
		lg: lg,
	}
}

//////////////////////////////////////////////////////////////////////
// Description functions
//////////////////////////////////////////////////////////////////////

// Register registers the connection function, defaults function, and flag set for the given endpoint.
func (d Description) Register() {
	// Sanity check
	if d.Create == nil {
		panic("the creation function must not be nil")
	}
	// Register the endpoint
	proxy.Description{
		Name: d.Name,
		Operations: []string{
			"defaults",
			"list_databases",
			"create_database",
			"delete_database",
			"list_tables",
			"describe_table",
			"create_table",
			"rename_table",
			"delete_table",
			"count",
			"insert",
			"begin_insert",
			"insert_record",
			"end_insert",
			"update",
			"delete",
			"select",
			"select_one",
			"advance",
			"close",
			"list_indices",
			"add_index",
			"delete_index",
			"add_keys",
			"delete_keys",
		},
		Create: func(ctx context.Context, cfg *proxy.Config, lg log.Interface) (proxy.Endpoint, error) {
			e, err := d.Create(ctx, cfg, lg)
			if err != nil {
				return nil, err
			}
			return newEndpoint(e, lg), nil
		},
		Flag:    d.Flag,
		FlagSet: d.FlagSet,
	}.Register()
}
