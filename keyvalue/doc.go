/*
Package keyvalue is an abstract endpoint for key-value connections. It is implemented by concrete endpoints such as [bitbucket.org/pcas/proxy/mongodb], [bitbucket.org/pcas/proxy/postgres], and [bitbucket.org/pcas/proxy/kvdb].

The operations on this abstract endpoint encode operations on objects in [bitbucket.org/pcas/keyvalue].

# Types

First we describe the JSON types specific to this endpoint.

# Connection

Each concrete endpoint has an implementation-specific Connection object:

	{
		implementation specific
	}

Any values set in Connection are typically optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation (see below).

# Record

A Record is defined by the JSON:

	{
		"key1": string|integer|boolean|float,
		...,
		"keyN": string|integer|boolean|float
	}

Here "key1", ..., "keyN" are arbitrary keys. The values are of type string, integer, boolean, or float. A Record is used to encode a [bitbucket.org/pcas/keyvalue/record.Record].

# Order

An Order is defined by the JSON:

	{
		"key": integer	// required
	}

Here "key" is the name of the key to order by, and value is either 1 (ascending) or -1 (descending). There must be exactly one element in an Order. An Order is used to encode a [bitbucket.org/pcas/keyvalue/sort.Order].

# OrderBy

An OrderBy is defined by the JSON:

	[Order, ..., Order]

An OrderBy is used to encode a [bitbucket.org/pcas/keyvalue/sort.OrderBy].

# Query

A Query can be defined by the JSON:

	string

where the string is an SQL-formatted query. The SQL should be formatted as follows:

	[[WHERE] <where condition>] [ORDER BY <sort order>] [LIMIT <limit>]

Alternatively, a Query can be defined by the JSON:

	{
		"Selector": Record,
		"Sort": OrderBy,
		"Limit": integer
	}

All keys are optional. A Query is used to encode a [bitbucket.org/pcas/keyvalue/parse.Query].

# Condition

A Condition can be defined by the JSON:

	string

where the string is an SQL-formatted condition. The SQL should be formatted as follows:

	[[WHERE] <where condition>]

Alternatively, a Condition can be defined by the JSON:

	{
		"Selector": Record
	}

All keys are optional. A Condition is used to encode a [bitbucket.org/pcas/keyvalue/condition.Condition].

# Operations

Now we describe the operations permitted by this endpoint.

# defaults

The Arguments are empty. The Result is of the form:

	{
		"Connection": Connection
	}

The result describes the implementation-specific default connection values.

# list_databases

The Arguments are of the form:

	{
		"Connection": Connection
	}

The result is a list of the names of the databases available on the Connection. The Result is of the form:

	{
		"Databases": [string, ..., string]
	}

# create_database

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string		// required
	}

This creates the database with the given name. The Result is empty.

# delete_database

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string		// required
	}

This will delete the database with the given name. If the database does not exist, this is a nop. The Result is empty.

# list_tables

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string		// required
	}

The result is a list of the names of the tables available on the named database. The Result is of the form:

	{
		"Tables": [string, ..., string]
	}

# describe_table

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
	}

The result is a best-guess description of the available keys and corresponding value types on the named table. The Result is of the form:

	{
		"Record": Record
	}

The keys of Record describe the keys of the table; the values are strings taken from the set:

	"int", "int8", "int16", "int32", "int64",
	"uint", "uint8", "uint16", "uint32", "uint64",
	"float64", "bool", "string", "[]byte"

Note that the accuracy of this description depends on the concrete endpoint. It might not be possible to return an exact description (for example, in a schema-less database), and in this case we return a good guess based on a sample of the data available.

# create_table

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Template": Record		// required
	}

This creates the named table. The Record is taken as a template to describe the keys of the table and their corresponding type (one of string, integer, or boolean). The Result is empty.

# rename_table

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"OldName": string,		// required
		"NewName": string		// required
	}

This renames the table OldName to NewName. The Result is empty.

# delete_table

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string			// required
	}

This deletes the named table. The Result is empty.

# count

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Where": Condition
	}

This counts the number of rows in the named table, satisfying the (optional) Condition. The Result is of the form:

	{
		"Number": integer
	}

where Number is set to the number of rows counted.

# insert

The Arguments are the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Record": Record,
		"Records": [Record, ..., Record]
	}

Exactly one of "Record" or "Records" is required. This will insert the Record or Records in the names table. The Result is of the form:

	{
		"Number": integer
	}

where Number is set to the number of records inserted.

# begin_insert

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"BufferSize": integer
	}

This will open an insert stream. The resulting stream will be assigned a ULID, which can then be used to refer to the stream via the "insert_record" and "end_insert" operations. It is essential that "end_insert" is called on the stream, otherwise resources may leak. The stream will optionally be backed by a buffer of indicated size. The Result is of the form:

	{
		"ULID": string
	}

# insert_record

The Arguments are of the form:

	{
		"ULID": string,		// required
		"Record": Record	// required
	}

This will insert the given Record into the stream with given ULID. The stream must have previously been opened with "begin_insert". The Result is empty.

# end_insert

The Arguments are of the form:

	{
		"ULID": string		// required
	}

This close the stream with given ULID, preventing future inserts, and ensuring that any buffered data is flushed. The stream must have previously been opened with "begin_insert". The Result is of the form:

	{
		"Number": integer
	}

where Number is set to the number of records inserted.

# update

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Where": Condition,
		"Replacement": Record	// required
	}

This will update all records in the named table that satisfy the condition, setting the keys present in replacement to their corresponding values. The Result is of the form:

	{
		"Number": integer
	}

The precise meaning of the value of Number depends on the concrete endpoint, however it is broadly understood to indicate the number of records updated. Some endpoints may always return the value 0.

# delete

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Where": Condition
	}

This deletes all records in the named table that satisfy the condition. The Result is of the form:

	{
		"Number": integer
	}

The value of Number is equal to the number of records deleted.

# select_one

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Where": Query,
		"Template": Record		// required
	}

This selects at most one records in the named table that satisfies the query, with the result in the format specified by template. The Result is of the form:

	{
		"Record": Record
	}

Record will be omitted from the Result if no records match the Where condition.

# select

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Where": Query,
		"Template": Record,		// required
		"BufferSize": integer
	}

This selects all records in the named table that satisfy the query, with results in the format specified by template. The records will optionally be backed by a buffer of indicated size. The resulting set of records are assigned a ULID, which can then be used to refer to the records via the "advance" and "close" operations. It is essential that "close" is called on the records, otherwise resources may leak. The Result is of the form:

	{
		"ULID": string
	}

# advance

The Arguments are of the form:

	{
		"ULID": string			// required
	}

This advances the iterator with given ULID obtained via the "select" operation. The Result is of the form:

	{
		"ULID": string,
		"Record": Record
	}

Record will be in the format given by the template record passed to "select". Record will be omitted from the Result if the end of iteration has been reached.

# close

The Arguments are of the form:

	{
		"ULID": string			// required
	}

This will close the iterator with given ULID obtained via the "select" operation, preventing any further iteration and releasing any resources used. The Result is empty.

# list_indices

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string			// required
	}

The result is a list of the keys in the named table that have an index. The Result is of the form:

	{
		"Indices": [string, ..., string]
	}

# add_index

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Key": string			// required
	}

Adds an index to the named key on the named table. The Result is empty.

# delete_index

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Key": string			// required
	}

Any index on the named key on the named table will be deleted. The Result is empty.

# add_keys

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,		// required
		"Table": string,		// required
		"Record": Record		// required
	}

Here Record describes the keys to add to the named table. The Record is taken as a template to describe the keys to add to the table and their corresponding type (one of string, integer, or boolean). The Result is empty.

# delete_keys

The Arguments are of the form:

	{
		"Connection": Connection,
		"Database": string,				// required
		"Table": string,				// required
		"Keys": [string, ..., string]	// required
	}

The given keys in the named table will be deleted. The Result is empty.
*/
package keyvalue

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
