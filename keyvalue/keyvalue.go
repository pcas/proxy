// keyvalue provides the endpoint for keyvalue database requests.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/parse"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"context"
	"encoding/json"
	"errors"
	"fmt"
)

// defaultStreamBuffer is the default size of the insert stream buffer.
const defaultStreamBuffer = 1024

// defaultIteratorBuffer is the default size of the iterator buffer.
const defaultIteratorBuffer = 1024

// recordIterator is a record.Iterator that converts [proxy.Messages] to [record.Records].
type recordIterator struct {
	name       string          // The name of the records being parsed
	ms         []proxy.Message // The slice of records to convert
	idx        int             // The index of the next entry to convert from ms
	hasStarted bool            // Has the user called Next or NextContext?
	hasNext    bool            // Is the record populated?
	r          record.Record   // The record
	itrErr     error           // The last error during iteration
	isClosed   bool            // Are we closed?
}

// closeIteratorFunc will close the iterator with given ULID.
type closeIteratorFunc func(ulid.ULID) error

// advanceIteratorFunc will advance the iterator with given ULID. The second return value will be true on successful iteration, false if the end of iteration has been reached, or an error occurred.
type advanceIteratorFunc func(context.Context, ulid.ULID) (record.Record, bool, error)

// selectFunc will create an iterator. Here the final argument is the buffer size (which may be 0, in which case the iterator is unbuffered). The returned ULID can be used to recovered the iterator.
type selectFunc func(context.Context, proxy.Message, record.Record, *parse.Query, int) (ulid.ULID, error)

// closeStreamFunc will close the stream with given ULID. On success, returns the number of entries passed to the stream.
type closeStreamFunc func(context.Context, ulid.ULID) (int64, error)

// insertInStreamFunc inserts the given record in the stream with given ULID.
type insertInStreamFunc func(context.Context, record.Record, ulid.ULID) error

// openStreamFunc will open a stream. Here the final argument is the buffer size (which may be 0, in which case the stream is unbuffered). The returned ULID can be used to recovered the stream.
type openStreamFunc func(context.Context, proxy.Message, int) (ulid.ULID, error)

// defaultsFunc returns the defaults for connections on this endpoint.
type defaultsFunc func() proxy.Message

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// parseRecord attempts to parse the given proxy.Message into a record.Record. The record will be validated before being returned.
//
// A Record is defined by the JSON:
//
//	{
//		"key1": string|integer|boolean|float,
//		...,
//		"keyN": string|integer|boolean|float
//	}
//
// Here "key1", ..., "keyN" are arbitrary keys. The values are of type string, integer, boolean, or float. A Record is used to encode a [record.Record].
func parseRecord(msg proxy.Message) (record.Record, error) {
	// Rebuild the record
	r := make(record.Record, len(msg))
	for k, v := range msg {
		switch x := v.(type) {
		case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64, float64, bool, string, []byte:
			r[k] = v
		case json.Number:
			if n, err := x.Int64(); err != nil {
				y, err := x.Float64()
				if err != nil {
					return nil, fmt.Errorf("error converting numerical value for '%s': %w", k, err)
				}
				r[k] = y
			} else {
				r[k] = n
			}
		default:
			return nil, fmt.Errorf("value for '%s' is of an unsupported record type: %T", k, v)
		}
	}
	// Validate the record
	if _, err := r.IsValid(); err != nil {
		return nil, err
	}
	return r, nil
}

// toRecord attempts to return the entry for the given key as a record.Record. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion. Any record will be validated before being returned.
func toRecord(msg proxy.Message, key string) (record.Record, bool, error) {
	rec, ok, err := msg.ToMessage(key)
	if err != nil || !ok {
		return nil, ok, err
	}
	r, err := parseRecord(rec)
	if err != nil {
		return nil, true, fmt.Errorf("invalid record '%s': %w", key, err)
	}
	return r, true, nil
}

// toRecordIterator attempts to return the entry for the given key as a record.Iterator. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion. Any records will be validated before being returned.
//
// A Record iterator is defined by the JSON:
//
//	[Record, ..., Record]
//
// of N Record elements.
func toRecordIterator(msg proxy.Message, key string) (record.Iterator, bool, error) {
	ms, ok, err := msg.ToMessageSlice(key)
	if err != nil || !ok {
		return nil, ok, err
	}
	return newRecordIterator(ms, key), true, nil
}

// toOrderBy attempts to return the entry for the given key as a sort.OrderBy. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
//
// An Order is defined by the JSON:
//
//	{
//		"key": integer	// reqwertyuired
//	}
//
// Here "key" is the name of the key to order by, and value is either 1 (ascending) or -1 (descending). There must be exactly one element in an Order. An Order is used to encode a [sort.Order].
//
// An OrderBy is defined by the JSON:
//
//	[Order, ..., Order]
//
// An OrderBy is used to encode a [sort.OrderBy].
func toOrderBy(msg proxy.Message, key string) (sort.OrderBy, bool, error) {
	// Recover the sort order
	T, ok, err := msg.ToMessageSlice(key)
	if err != nil || !ok {
		return nil, ok, err
	}
	// Convert the sort order
	S := make([]sort.Order, 0, len(T))
	for i, m := range T {
		// Recover the sort key
		ks := m.Keys()
		if len(ks) != 1 {
			return nil, true, fmt.Errorf("element %d of '%s' should have exactly one entry", i, key)
		}
		k := ks[0]
		// Recover the sort order
		dir, _, err := m.ToInt(k)
		if err != nil {
			return nil, true, fmt.Errorf("error parsing element %d of '%s': %w", i, key, err)
		}
		// Append the sort order
		switch dir {
		case 1:
			S = append(S, sort.Order{Key: k, Direction: sort.Ascending})
		case -1:
			S = append(S, sort.Order{Key: k, Direction: sort.Descending})
		default:
			return nil, true, fmt.Errorf("error parsing element %d of '%s': the sort order for '%s' must have value 1 or -1", i, key, k)
		}
	}
	return sort.By(S...), true, nil
}

// toQuery attempts to return the entry for the given key as a parse.Query. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
//
// A Query can be defined by the JSON:
//
//	string
//
// where the string is an SQL-formatted query. The SQL should be formatted as follows:
//
//	[[WHERE] <where condition>] [ORDER BY <sort order>] [LIMIT <limit>]
//
// Alternatively, a Query can be defined by the JSON:
//
//	{
//		"Selector": Record,
//		"Sort": OrderBy,
//		"Limit": integer
//	}
//
// All keys are optional. A Query is used to encode a [parse.Query].
func toQuery(msg proxy.Message, key string) (*parse.Query, bool, error) {
	// Is there anything to do?
	if msg == nil {
		return &parse.Query{}, false, nil
	}
	x, ok := msg[key]
	if !ok {
		return &parse.Query{}, false, nil
	}
	// Is this a string?
	if s, ok := x.(string); ok {
		// Parse the string
		q, err := parse.SQL(s)
		if err != nil {
			return nil, true, fmt.Errorf("error parsing '%s': %w", key, err)
		}
		return q, true, nil
	}
	// Otherwise it must be a Message
	y, ok := x.(map[string]interface{})
	if !ok {
		return nil, true, fmt.Errorf("error parsing '%s': must be either a string or a Message", key)
	}
	m := proxy.Message(y)
	// Sanity check on the keys
	if err := m.OnlyKeys("Selector", "Sort", "Limit"); err != nil {
		return nil, true, fmt.Errorf("error parsing '%s': %w", key, err)
	}
	// Extract the details
	r, _, err := toRecord(m, "Selector")
	if err != nil {
		return nil, true, err
	}
	order, _, err := toOrderBy(m, "Sort")
	if err != nil {
		return nil, true, err
	}
	limit, hasLimit, err := m.ToInt64("Limit")
	if err != nil {
		return nil, true, err
	}
	// Build the query
	return &parse.Query{
		Cond:     condition.FromRecord(r),
		Order:    order,
		HasLimit: hasLimit,
		Limit:    limit,
	}, true, nil
}

// toCondition attempts to return the entry for the given key as a condition.Condition. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
//
// A Condition can be defined by the JSON:
//
//	string
//
// where the string is an SQL-formatted condition. The SQL should be formatted as follows:
//
//	[[WHERE] <where condition>]
//
// Alternatively, a Condition can be defined by the JSON:
//
//	{
//		"Selector": Record
//	}
//
// All keys are optional. A Condition is used to encode a [condition.Condition].
func toCondition(msg proxy.Message, key string) (condition.Condition, bool, error) {
	// Is there anything to do?
	if msg == nil {
		return nil, false, nil
	}
	x, ok := msg[key]
	if !ok {
		return nil, false, nil
	}
	// Is this a string?
	if s, ok := x.(string); ok {
		// Parse the string
		q, err := parse.SQL(s)
		if err != nil {
			return nil, true, fmt.Errorf("error parsing '%s': %w", key, err)
		}
		// The query mustn't contain a sort order or limit
		if len(q.Order) != 0 {
			return nil, true, fmt.Errorf("error parsing '%s': condition must not contain an ORDER BY clause", key)
		} else if q.HasLimit {
			return nil, true, fmt.Errorf("error parsing '%s': condition must not contain a LIMIT clause", key)
		}
		// Return the condition
		return q.Cond, true, nil
	}
	// Otherwise it must be a Message
	y, ok := x.(map[string]interface{})
	if !ok {
		return nil, true, fmt.Errorf("error parsing '%s': must be either a string or a Message", key)
	}
	m := proxy.Message(y)
	// Sanity check on the keys
	if err := m.OnlyKeys("Selector"); err != nil {
		return nil, true, fmt.Errorf("error parsing '%s': %w", key, err)
	}
	// Extract the details
	r, _, err := toRecord(m, "Selector")
	if err != nil {
		return nil, true, err
	}
	// Return the condition
	return condition.FromRecord(r), true, nil
}

//////////////////////////////////////////////////////////////////////
// recordIterator functions
//////////////////////////////////////////////////////////////////////

// newRecordIterator returns a new iterator for the given slice.
func newRecordIterator(ms []proxy.Message, name string) record.Iterator {
	return &recordIterator{
		name: name,
		ms:   ms,
	}
}

// Close closes the iterator, preventing further iteration.
func (itr *recordIterator) Close() error {
	itr.isClosed = true
	return nil
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *recordIterator) Err() error {
	return itr.itrErr
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *recordIterator) Next() bool {
	ok, err := itr.NextContext(context.Background())
	return err == nil && ok
}

// NextContext advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next or NextContext must be called before the first call to Scan.
func (itr *recordIterator) NextContext(_ context.Context) (bool, error) {
	// Sanity check
	if itr.isClosed {
		return false, errors.New("iterator closed")
	}
	itr.hasStarted = true
	itr.hasNext = false
	// Is there anything to do?
	if itr.idx == len(itr.ms) {
		return false, nil
	}
	// Convert the entry
	r, err := parseRecord(itr.ms[itr.idx])
	if err != nil {
		itr.itrErr = fmt.Errorf("error parsing element %d of '%s': %w", itr.idx, itr.name, err)
		return false, itr.itrErr
	}
	// Success
	itr.hasNext = true
	itr.r = r
	itr.idx++
	return true, nil
}

// Scan copies the current record into "dest". Any previously set keys or values in "dest" will be deleted or overwritten.
func (itr *recordIterator) Scan(dest record.Record) error {
	// Sanity check
	if itr.isClosed {
		return errors.New("iterator closed")
	} else if !itr.hasStarted {
		return errors.New("calling Scan without previous call to Next or NextContext")
	} else if !itr.hasNext {
		return errors.New("end of iteration")
	}
	// Delete any existing keys in dest
	for k := range dest {
		delete(dest, k)
	}
	// Copy over the record to dest
	for k, v := range itr.r {
		dest[k] = v
	}
	return nil
}

//////////////////////////////////////////////////////////////////////
// handler functions
//////////////////////////////////////////////////////////////////////

// handleClose handles close requests.
//
// The Arguments are of the form:
//
//	{
//		"ULID": string			// required
//	}
//
// This will close the iterator with given ULID obtained via the "select" operation, preventing any further iteration and releasing any resources used. The Result is empty.
func handleClose(_ context.Context, args proxy.Message, closef closeIteratorFunc, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("ULID"); err != nil {
		return nil, err
	}
	u, ok, err := args.ToULID("ULID")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'ULID'")
	}
	if err := closef(u); err != nil {
		lg.Printf("Iterator %s closed with error: %v", u, err)
		return nil, err
	}
	lg.Printf("Iterator %s closed", u)
	return nil, nil
}

// advance handles advance requests.
//
// The Arguments are of the form:
//
//	{
//		"ULID": string			// required
//	}
//
// This advances the iterator with given ULID obtained via the "select" operation. The Result is of the form:
//
//	{
//		"ULID": string,
//		"Record": Record
//	}
//
// Record will be in the format given by the template record passed to "select". Record will be omitted from the Result if the end of iteration has been reached.
func advance(ctx context.Context, args proxy.Message, advancef advanceIteratorFunc, lg log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("ULID"); err != nil {
		return nil, err
	}
	// Recover the ULID
	u, ok, err := args.ToULID("ULID")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'ULID'")
	}
	// Advance the iterator
	r, ok, err := advancef(ctx, u)
	if err != nil {
		lg.Printf("Error advancing iterator %s: %v", u, err)
		return nil, err
	} else if !ok {
		// The end of iteration has been reached
		return proxy.Message{"ULID": u.String()}, nil
	}
	// Return the record
	return proxy.Message{
		"ULID":   u.String(),
		"Record": proxy.Message(r),
	}, nil
}

// selectOne handles select_one requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Where": Query,
//		"Template": Record		// required
//	}
//
// This selects at most one records in the named table that satisfies the query, with the result in the format specified by template. The Result is of the form:
//
//	{
//		"Record": Record
//	}
//
// Record will be omitted from the Result if no records match the Where condition.
func selectOne(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Database", "Table", "Where", "Template"); err != nil {
		return nil, err
	}
	// Get the query
	q, _, err := toQuery(args, "Where")
	if err != nil {
		return nil, err
	}
	// Get the template
	template, ok, err := toRecord(args, "Template")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Template'")
	}
	// Perform the select
	r, err := t.SelectOneWhere(ctx, template, q.Cond, q.Order)
	if err != nil {
		return nil, err
	}
	// Return success
	if r == nil {
		return proxy.Message{}, nil
	}
	return proxy.Message{"Record": proxy.Message(r)}, nil
}

// handleSelect handles select requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Where": Query,
//		"Template": Record,		// required
//		"BufferSize": integer
//	}
//
// This selects all records in the named table that satisfy the query, with results in the format specified by template. The records will optionally be backed by a buffer of indicated size. The resulting set of records are assigned a ULID, which can then be used to refer to the records via the "advance" and "close" operations. It is essential that "close" is called on the records, otherwise resources may leak. The Result is of the form:
//
//	{
//		"ULID": string
//	}
func handleSelect(ctx context.Context, args proxy.Message, performSelect selectFunc, lg log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Database", "Table", "Where", "Template", "BufferSize"); err != nil {
		return nil, err
	}
	// Get the query
	q, _, err := toQuery(args, "Where")
	if err != nil {
		return nil, err
	}
	// Get the template
	template, ok, err := toRecord(args, "Template")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Template'")
	}
	// Get the buffer size
	bufSize := defaultIteratorBuffer
	if n, ok, err := args.ToInt("BufferSize"); err != nil {
		return nil, err
	} else if ok {
		bufSize = n
	}
	// Sanity check on the buffer size
	if bufSize < 0 {
		return nil, errors.New("invalid buffer size")
	}
	// Perform the select
	u, err := performSelect(ctx, args, template, q, bufSize)
	if err != nil {
		return nil, err
	}
	// Log this iterator
	lg.Printf("Iterator created with ULID %s", u)
	// Return success
	return proxy.Message{"ULID": u.String()}, nil
}

// insert handles insert requests.
//
// The Arguments are the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Record": Record,
//		"Records": [Record, ..., Record]
//	}
//
// Exactly one of "Record" or "Records" is required. This will insert the Record or Records in the names table. The Result is of the form:
//
//	{
//		"Number": integer
//	}
//
// where Number is set to the number of records inserted.
func insert(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Database", "Table", "Record", "Records"); err != nil {
		return nil, err
	}
	// First we see if Record is set
	if r, ok, err := toRecord(args, "Record"); err != nil {
		return nil, err
	} else if ok {
		// Check that Records is not set
		if _, ok = args["Records"]; ok {
			return nil, errors.New("exacly one of 'Record' or 'Records' must be set")
		}
		// Insert the record
		if err = t.InsertRecords(ctx, r); err != nil {
			return nil, err
		}
		return proxy.Message{"Number": 1}, nil
	}
	// Since Record isn't set, Records must be set
	itr, ok, err := toRecordIterator(args, "Records")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("exacly one of 'Record' or 'Records' must be set")
	}
	// Wrap the iterator as a CountIterator
	citr := record.NewCountIterator(itr)
	defer citr.Close()
	// Insert the records
	if err = t.Insert(ctx, citr); err != nil {
		return nil, err
	}
	return proxy.Message{"Number": citr.Count()}, nil
}

// endInsert handles end_insert requests.
//
// The Arguments are of the form:
//
//	{
//		"ULID": string		// required
//	}
//
// This close the stream with given ULID, preventing future inserts, and ensuring that any buffered data is flushed. The stream must have previously been opened with "begin_insert". The Result is of the form:
//
//	{
//		"Number": integer
//	}
//
// where Number is set to the number of records inserted.
func endInsert(ctx context.Context, args proxy.Message, closef closeStreamFunc, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("ULID"); err != nil {
		return nil, err
	}
	u, ok, err := args.ToULID("ULID")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'ULID'")
	}
	n, err := closef(ctx, u)
	if err != nil {
		lg.Printf("Insert stream %s closed with error: %v", u, err)
		return nil, err
	}
	lg.Printf("Insert stream %s closed", u)
	return proxy.Message{"Number": n}, nil
}

// insertRecord handles insert_record requests.
//
// The Arguments are of the form:
//
//	{
//		"ULID": string,		// required
//		"Record": Record	// required
//	}
//
// This will insert the given Record into the stream with given ULID. The stream must have previously been opened with "begin_insert". The Result is empty.
func insertRecord(ctx context.Context, args proxy.Message, insertf insertInStreamFunc, _ log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("ULID", "Record"); err != nil {
		return nil, err
	}
	// Recover the ULID
	u, ok, err := args.ToULID("ULID")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'ULID'")
	}
	// Recover the record
	r, ok, err := toRecord(args, "Record")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Record'")
	}
	// Perform the insert
	return nil, insertf(ctx, r, u)
}

// beginInsert handles begin_insert requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"BufferSize": integer
//	}
//
// This will open an insert stream. The resulting stream will be assigned a ULID, which can then be used to refer to the stream via the "insert_record" and "end_insert" operations. It is essential that "end_insert" is called on the stream, otherwise resources may leak. The stream will optionally be backed by a buffer of indicated size. The Result is of the form:
//
//	{
//		"ULID": string
//	}
func beginInsert(ctx context.Context, args proxy.Message, openStream openStreamFunc, lg log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Database", "Table", "BufferSize"); err != nil {
		return nil, err
	}
	// Get the buffer size
	bufSize := defaultStreamBuffer
	if n, ok, err := args.ToInt("BufferSize"); err != nil {
		return nil, err
	} else if ok {
		bufSize = n
	}
	// Sanity check on the buffer size
	if bufSize < 0 {
		return nil, errors.New("invalid buffer size")
	}
	// Open the insert stream
	u, err := openStream(ctx, args, bufSize)
	if err != nil {
		return nil, err
	}
	// Log this stream
	lg.Printf("Insert stream created with ULID %s", u)
	// Return success
	return proxy.Message{"ULID": u.String()}, nil
}

// update handles update requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Where": Condition,
//		"Replacement": Record	// required
//	}
//
// This will update all records in the named table that satisfy the condition, setting the keys present in replacement to their corresponding values. The Result is of the form:
//
//	{
//		"Number": integer
//	}
//
// The precise meaning of the value of Number depends on the concrete endpoint, however it is broadly understood to indicate the number of records updated. Some endpoints may always return the value 0.
func update(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table", "Where", "Replacement"); err != nil {
		return nil, err
	}
	cond, _, err := toCondition(args, "Where")
	if err != nil {
		return nil, err
	}
	replacement, ok, err := toRecord(args, "Replacement")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Replacement'")
	}
	n, err := t.UpdateWhere(ctx, replacement, cond)
	if err != nil {
		return nil, err
	}
	return proxy.Message{"Number": n}, nil
}

// handleDelete handles delete requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Where": Condition
//	}
//
// This deletes all records in the named table that satisfy the condition. The Result is of the form:
//
//	{
//		"Number": integer
//	}
//
// The value of Number is equal to the number of records deleted.
func handleDelete(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table", "Where"); err != nil {
		return nil, err
	}
	cond, _, err := toCondition(args, "Where")
	if err != nil {
		return nil, err
	}
	n, err := t.DeleteWhere(ctx, cond)
	if err != nil {
		return nil, err
	}
	return proxy.Message{"Number": n}, nil
}

// count handles count requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Where": Condition
//	}
//
// This counts the number of rows in the named table, satisfying the (optional) Condition. The Result is of the form:
//
//	{
//		"Number": integer
//	}
//
// where Number is set to the number of rows counted.
func count(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table", "Where"); err != nil {
		return nil, err
	}
	cond, _, err := toCondition(args, "Where")
	if err != nil {
		return nil, err
	}
	n, err := t.CountWhere(ctx, cond)
	if err != nil {
		return nil, err
	}
	return proxy.Message{"Number": n}, nil
}

// listDatabases handles list_databases requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection
//	}
//
// The result is a list of the names of the databases available on the Connection. The Result is of the form:
//
//	{
//		"Databases": [string, ..., string]
//	}
func listDatabases(ctx context.Context, c keyvalue.Connection, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection"); err != nil {
		return nil, err
	}
	S, err := c.ListDatabases(ctx)
	if err != nil {
		return nil, err
	} else if S == nil {
		S = []string{}
	}
	return proxy.Message{"Databases": S}, nil
}

// createDatabase handles create_database requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//	}
//
// This will create the database with the given name. The Result is empty.
func createDatabase(ctx context.Context, c keyvalue.Connection, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database"); err != nil {
		return nil, err
	}
	// Recover the database name
	name, ok, err := args.ToString("Database")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Database'")
	}
	// Create the database
	return nil, c.CreateDatabase(ctx, name)
}

// deleteDatabase handles delete_database requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//	}
//
// This will delete the database with the given name. If the database does not exist, this is a nop. The Result is empty.
func deleteDatabase(ctx context.Context, c keyvalue.Connection, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database"); err != nil {
		return nil, err
	}
	// Recover the database name
	name, ok, err := args.ToString("Database")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Database'")
	}
	// Delete the database
	return nil, c.DeleteDatabase(ctx, name)
}

// createTable handles create_table requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Template": Record		// required
//	}
//
// This creates the named table. The Record is taken as a template to describe the keys of the table and their corresponding type (one of string, integer, or boolean). The Result is empty.
func createTable(ctx context.Context, db keyvalue.Database, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table", "Template"); err != nil {
		return nil, err
	}
	name, ok, err := args.ToString("Table")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Table'")
	}
	template, ok, err := toRecord(args, "Template")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Template'")
	}
	return nil, db.CreateTable(ctx, name, template)
}

// listTables handles list_tables requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string		// required
//	}
//
// The result is a list of the names of the tables available on the named database. The Result is of the form:
//
//	{
//		"Tables": [string, ..., string]
//	}
func listTables(ctx context.Context, db keyvalue.Database, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database"); err != nil {
		return nil, err
	}
	S, err := db.ListTables(ctx)
	if err != nil {
		return nil, err
	} else if S == nil {
		S = []string{}
	}
	return proxy.Message{"Tables": S}, nil
}

// deleteTable handles delete_table requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string			// required
//	}
//
// This deletes the named table. The Result is empty.
func deleteTable(ctx context.Context, db keyvalue.Database, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table"); err != nil {
		return nil, err
	}
	name, ok, err := args.ToString("Table")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Table'")
	}
	return nil, db.DeleteTable(ctx, name)
}

// renameTable handles rename_table requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"OldName": string,		// required
//		"NewName": string		// required
//	}
//
// This renames the table OldName to NewName. The Result is empty.
func renameTable(ctx context.Context, db keyvalue.Database, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "OldName", "NewName"); err != nil {
		return nil, err
	}
	oldName, ok, err := args.ToString("OldName")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'OldName'")
	}
	newName, ok, err := args.ToString("NewName")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'NewName'")
	}
	return nil, db.RenameTable(ctx, oldName, newName)
}

// describeTable handles describe_table requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//	}
//
// The result is a best-guess description of the available keys and corresponding value types on the named table. The Result is of the form:
//
//	{
//		"Record": Record
//	}
//
// The keys of Record describe the keys of the table; the values are strings taken from the set:
//
//	"int", "int8", "int16", "int32", "int64",
//	"uint", "uint8", "uint16", "uint32", "uint64",
//	"float64", "bool", "string", "[]byte"
//
// Note that the accuracy of this description depends on the concrete endpoint. It might not be possible to return an exact description (for example, in a schema-less database), and in this case we return a good guess based on a sample of the data available.
func describeTable(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table"); err != nil {
		return nil, err
	}
	r, err := t.Describe(ctx)
	if err != nil {
		return nil, err
	}
	desc := make(proxy.Message, len(r))
	for k, v := range r {
		switch v.(type) {
		case int:
			desc[k] = "int"
		case int8:
			desc[k] = "int8"
		case int16:
			desc[k] = "int16"
		case int32:
			desc[k] = "int32"
		case int64:
			desc[k] = "int64"
		case uint:
			desc[k] = "uint"
		case uint8:
			desc[k] = "uint8"
		case uint16:
			desc[k] = "uint16"
		case uint32:
			desc[k] = "uint32"
		case uint64:
			desc[k] = "uint64"
		case float64:
			desc[k] = "float64"
		case bool:
			desc[k] = "bool"
		case string:
			desc[k] = "string"
		case []byte:
			desc[k] = "[]byte"
		default:
			return nil, fmt.Errorf("unsupported type for key '%s': %T", k, v)
		}
	}
	return proxy.Message{"Record": desc}, nil
}

// addIndex handles add_index requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Key": string			// required
//	}
//
// Adds an index to the named key on the named table. The Result is empty.
func addIndex(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table", "Key"); err != nil {
		return nil, err
	}
	k, ok, err := args.ToString("Key")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Key'")
	}
	return nil, t.AddIndex(ctx, k)
}

// deleteIndex handles delete_index requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Key": string			// required
//	}
//
// Any index on the named key on the named table will be deleted. The Result is empty.
func deleteIndex(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table", "Key"); err != nil {
		return nil, err
	}
	k, ok, err := args.ToString("Key")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Key'")
	}
	return nil, t.DeleteIndex(ctx, k)
}

// listIndices handles list_indices requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string			// required
//	}
//
// The result is a list of the keys in the named table that have an index. The Result is of the form:
//
//	{
//		"Indices": [string, ..., string]
//	}
func listIndices(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table"); err != nil {
		return nil, err
	}
	idxs, err := t.ListIndices(ctx)
	if err != nil {
		return nil, err
	} else if idxs == nil {
		idxs = []string{}
	}
	return proxy.Message{"Indices": idxs}, nil
}

// addKeys handles add_keys requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,		// required
//		"Table": string,		// required
//		"Record": Record		// required
//	}
//
// Here Record describes the keys to add to the named table. The Record is taken as a template to describe the keys to add to the table and their corresponding type (one of string, integer, or boolean). The Result is empty.
func addKeys(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table", "Record"); err != nil {
		return nil, err
	}
	r, ok, err := toRecord(args, "Record")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Record'")
	}
	return nil, t.AddKeys(ctx, r)
}

// deleteKeys handles delete_keys requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Database": string,				// required
//		"Table": string,				// required
//		"Keys": [string, ..., string]	// required
//	}
//
// The given keys in the named table will be deleted. The Result is empty.
func deleteKeys(ctx context.Context, t keyvalue.Table, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Database", "Table", "Keys"); err != nil {
		return nil, err
	}
	keys, ok, err := args.ToStringSlice("Keys")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Keys'")
	}
	return nil, t.DeleteKeys(ctx, keys)
}

// defaults handles defaults requests.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Connection": Connection
//	}
//
// The result describes the implementation-specific default connection values.
func defaults(_ context.Context, args proxy.Message, df defaultsFunc, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	return proxy.Message{"Connection": df()}, nil
}
