// insertStream provides a stream allowing repeated inserts of records.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package keyvalue

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcastools/contextutil"
	"context"
	"errors"
	"sync"
)

// insertStream provides a stream allowing repeated inserts of records.
type insertStream struct {
	workerDoneC   <-chan struct{}      // Has the worker exited?
	workerErrC    <-chan error         // Any error reported by worker on exit
	m             sync.Mutex           // Mutex protects the following
	iteratorAddC  chan<- record.Record // Feed a record to the iterator
	iteratorErrC  chan<- error         // Pass an error down the iterator
	workerCancelC chan<- struct{}      // Ask the worker to exit
	n             int64                // The number of records passed
	isClosed      bool                 // Have we closed?
	closeErr      error                // The error on close, if any
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// startInsertWorker starts a background go-routine performing an insert into the given table t, using the given iterator itr. Returns three channels: the first, a cancel channel, should be closed to ask the worker to exit; the second, a done channel, will be closed when the worker exits; the third, an error channel, will be closed when the worker exits, and contain any error produced during the insert. The iterator and associated connections will be closed by the worker on exit.
func startInsertWorker(t keyvalue.Table, db keyvalue.Database, c keyvalue.Connection, itr record.Iterator) (chan<- struct{}, <-chan struct{}, <-chan error) {
	//	Create the communication channels
	cancelC := make(chan struct{})
	doneC := make(chan struct{})
	errC := make(chan error, 1)
	// Start the insert in a new go-routine
	go func(cancelC <-chan struct{}, doneC chan<- struct{}, errC chan<- error) {
		// Create a new context and bind it to the cancel channel
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		defer contextutil.NewChannelLink(cancelC, cancel).Stop()
		// Perform the insert
		if err := t.Insert(ctx, itr); err != nil {
			errC <- err
		}
		// Close the channels
		close(doneC)
		close(errC)
		// Close the iterator
		itr.Close() // Ignore any error
		// Close the connections
		t.Close()  // Ignore any error
		db.Close() // Ignore any error
		c.Close()  // Ignore any error
	}(cancelC, doneC, errC)
	// Return the channels
	return cancelC, doneC, errC
}

//////////////////////////////////////////////////////////////////////
// insertStream functions
//////////////////////////////////////////////////////////////////////

// newInsertStream returns a new insert stream, allowing records to be inserted into the table t. This keeps the underlying connections alive for the lifetime of the stream. The stream buffer size can be set with 'bufSize'.
func newInsertStream(c keyvalue.Connection, db keyvalue.Database, t keyvalue.Table, bufSize int) *insertStream {
	// Sanity check on the buffer size
	if bufSize < 0 {
		bufSize = 0
	}
	// Create the communication channels
	iteratorAddC := make(chan record.Record, bufSize)
	iteratorErrC := make(chan error)
	// Create the iterator
	itr := record.NewIterator(iteratorAddC, iteratorErrC, make(chan struct{}))
	// Start the background insert worker
	workerCancelC, workerDoneC, workerErrC := startInsertWorker(t, db, c, itr)
	// Return the stream
	return &insertStream{
		iteratorAddC:  iteratorAddC,
		iteratorErrC:  iteratorErrC,
		workerDoneC:   workerDoneC,
		workerCancelC: workerCancelC,
		workerErrC:    workerErrC,
	}
}

// Insert inserts the given record in the stream.
func (s *insertStream) Insert(ctx context.Context, r record.Record) error {
	// Acquire a lock
	s.m.Lock()
	defer s.m.Unlock()
	// Are we closed?
	if s.isClosed {
		return errors.New("stream closed")
	}
	// Attempt to perform the insert
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-s.workerDoneC:
		// The worker has exited from under us
		err := s.closeWithLock(ctx)
		if err == nil { // This shouldn't happen
			err = errors.New("stream error")
		}
		return err
	case s.iteratorAddC <- r:
		s.n++
	}
	return nil
}

// Count returns the number of records inserted into the stream.
func (s *insertStream) Count() int64 {
	s.m.Lock()
	defer s.m.Unlock()
	return s.n
}

// Close closes the stream, ensuring that any buffered records are inserted.
func (s *insertStream) Close() error {
	return s.CloseWithContext(context.Background())
}

// CloseWithContext closes the stream, ensuring that any buffered records are inserted.
func (s *insertStream) CloseWithContext(ctx context.Context) error {
	s.m.Lock()
	defer s.m.Unlock()
	return s.closeWithLock(ctx)
}

// closeWithLock closes the stream, ensuring that any buffered records are inserted. Assumes that the caller owns the lock on the stream.
func (s *insertStream) closeWithLock(ctx context.Context) (err error) {
	// Is there anything to do?
	if !s.isClosed {
		// Mark us as closed
		s.isClosed = true
		// Close the iterator
		close(s.iteratorAddC)
		close(s.iteratorErrC)
		// Wait for the worker to exit, or for the context to fire
		select {
		case <-s.workerDoneC:
		default:
			select {
			case <-ctx.Done():
				close(s.workerCancelC)
				<-s.workerDoneC
				err = ctx.Err()
			case <-s.workerDoneC:
			}
		}
		// Make a note of any worker errors
		s.closeErr = <-s.workerErrC
	}
	// Return any errors
	if err == nil {
		err = s.closeErr
	}
	return
}
