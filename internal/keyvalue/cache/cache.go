// Cache defines a cache of keyvalue.Connections.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package cache

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/proxy/internal/cache"
	"bitbucket.org/pcastools/log"
	"context"
)

// value is a value in the cache.
type value struct {
	keyvalue.Connection
	release func() // The release function
}

// Cache is a cache of keyvalue.Connections accessed by their config.
type Cache[K cache.Hasher] struct {
	cache *cache.Cache[K, keyvalue.Connection]
}

// CreationFunc creates a new value for the given key.
type CreationFunc[K cache.Hasher] func(context.Context, K) (keyvalue.Connection, error)

//////////////////////////////////////////////////////////////////////
// value functions
//////////////////////////////////////////////////////////////////////

// Close closes the value.
func (v *value) Close() error {
	v.release()
	return nil
}

//////////////////////////////////////////////////////////////////////
// Cache functions
//////////////////////////////////////////////////////////////////////

// Close closes the cache.
func (c *Cache[_]) Close() error {
	return c.cache.Close()
}

// Get returns a storage with given config.
func (c *Cache[K]) Get(ctx context.Context, cfg K) (keyvalue.Connection, error) {
	e, err := c.cache.Get(ctx, cfg)
	if err != nil {
		return nil, err
	}
	return &value{
		Connection: e.Value(),
		release:    e.Release,
	}, nil
}

// New returns a new keyvalue.Connection cache.
func New[K cache.Hasher](create CreationFunc[K], equals cache.EqualsFunc[K], lg log.Interface) *Cache[K] {
	return &Cache[K]{
		cache: cache.New(cache.CreationFunc[K, keyvalue.Connection](create), equals, lg),
	}
}
