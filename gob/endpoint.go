// endpoint defines the endpoint for gob requests

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package gob

import (
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/log"
	"context"
	"encoding/base64"
	"encoding/gob"
	"errors"
	"fmt"
	"sort"
)

// endpoint is the gob endpoint.
type endpoint int

// The registered encoders and decoders.
var (
	encoders = make(map[string]encodeFunc)
	decoders = make(map[string]decodeFunc)
)

// encodeFunc is a function that encodes the given object using the given gob.Encoder.
type encodeFunc func(proxy.Message, *gob.Encoder, log.Interface) error

// decodeFunc is a function that decodes the contents of the given gob.Decoder, returning the resulting object.
type decodeFunc func(*gob.Decoder, log.Interface) (proxy.Message, error)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init registers this endpoint.
func init() {
	// Register the endpoint
	proxy.Description{
		Name: "gob",
		Operations: []string{
			"universes",
			"encode",
			"decode",
		},
		Create: func(_ context.Context, _ *proxy.Config, _ log.Interface) (proxy.Endpoint, error) {
			return endpoint(0), nil
		},
	}.Register()
}

// register registers the given encode and decode functions for the given universe. This will panic if encode and decode functions are already registered for this universe.
func register(universe string, encode encodeFunc, decode decodeFunc) {
	// Sanity check
	if encode == nil {
		panic("the encoder cannot be nil")
	} else if decode == nil {
		panic("the decoder cannot be nil")
	}
	// Check that this universe is not already registered
	if _, ok := encoders[universe]; ok {
		panic("duplicate universe")
	}
	// Register the universe
	encoders[universe] = encode
	decoders[universe] = decode
}

//////////////////////////////////////////////////////////////////////
// handler functions
//////////////////////////////////////////////////////////////////////

// universes handles universes requests.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Universes": [string, ..., string]
//	}
//
// Here Universes is an array of all supported universes.
func universes(args proxy.Message) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	keys := make([]string, len(encoders))
	for k := range encoders {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return proxy.Message{
		"Universes": keys,
	}, nil
}

// handleEncode handles encodes requests.
//
// The Arguments are of the form:
//
//	{
//		"Universe": string,		// required
//		"Value": Object			// required
//	}
//
// Here the type Object of value depends on the universe. The Result is of the form:
//
//	{
//		"Universe": string,
//		"Encoding": string
//	}
func handleEncode(args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Universe", "Value"); err != nil {
		return nil, err
	}
	// Recover the universe
	universe, ok, err := args.ToString("Universe")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Universe'")
	}
	// Check that an encoder is registered for this universe
	encode, ok := encoders[universe]
	if !ok {
		return nil, fmt.Errorf("unknown universe: %s", universe)
	}
	// Recover the value
	val, ok, err := args.ToMessage("Value")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Object'")
	}
	// Fetch a bytes buffer
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Create the encoder
	baseEnc := base64.NewEncoder(base64.StdEncoding, b)
	enc := gob.NewEncoder(baseEnc)
	// Encode the value
	err = encode(val, enc, lg)
	if err != nil {
		return nil, err
	} else if err = baseEnc.Close(); err != nil {
		return nil, err
	}
	// Return the encoded value
	return proxy.Message{
		"Universe": universe,
		"Encoding": b.String(),
	}, nil
}

// handleDecode handles decode requests.
//
// The Arguments are of the form:
//
//	{
//		"Universe": string,		// required
//		"Encoding": string		// required
//	}
//
// The Result is of the form:
//
//	{
//		"Universe": string,
//		"Value": Object
//	}
//
// Here the type Object of value depends on the universe.
func handleDecode(args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Universe", "Encoding"); err != nil {
		return nil, err
	}
	// Recover the universe
	universe, ok, err := args.ToString("Universe")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Universe'")
	}
	// Check that a decoder is registered for this universe
	decode, ok := decoders[universe]
	if !ok {
		return nil, fmt.Errorf("unknown universe: %s", universe)
	}
	// Recover the encoded value
	s, ok, err := args.ToString("Encoding")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Encoding'")
	}
	// Wrap the object in a bytes buffer
	b := bytesbuffer.NewString(s)
	defer bytesbuffer.Reuse(b)
	// Create the decoder
	baseDec := base64.NewDecoder(base64.StdEncoding, b)
	dec := gob.NewDecoder(baseDec)
	// Decode the value
	res, err := decode(dec, lg)
	if err != nil {
		return nil, err
	}
	// Return the value
	return proxy.Message{
		"Universe": universe,
		"Value":    res,
	}, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// Close closes the endpoint.
func (endpoint) Close() error {
	return nil
}

// Handle handles requests for operation 'op' with arguments 'args'.
func (endpoint) Handle(_ context.Context, op string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	switch op {
	case "universes":
		return universes(args)
	case "encode":
		return handleEncode(args, lg)
	case "decode":
		return handleDecode(args, lg)
	default:
		return nil, fmt.Errorf("unknown operation: %s", op)
	}
}
