// Gob provides encoding and decoding of mathematical objects

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package gob

import (
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/log"
	"encoding/gob"
	"errors"
	"fmt"
	"math/big"
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init registers encoders and decoders for the universes.
func init() {
	register("[integers]", encodeIntSlice, decodeIntSlice)
	register("[rationals]", encodeRatSlice, decodeRatSlice)
}

// encodeIntSlice handles encoding for "[integers]".
//
// An Object is of the form:
//
//	{
//		"Array": [string, ..., string]	// required
//	}
//
// Here Array is an array of string representations of integers.
func encodeIntSlice(val proxy.Message, enc *gob.Encoder, lg log.Interface) error {
	if err := val.OnlyKeys("Array"); err != nil {
		return err
	}
	S, ok, err := val.ToStringSlice("Array")
	if err != nil {
		return err
	} else if !ok {
		return errors.New("missing key 'Array'")
	}
	T := make([]*big.Int, 0, len(S))
	for i, s := range S {
		n, ok := (&big.Int{}).SetString(s, 10)
		if !ok {
			return fmt.Errorf("unable to convert entry %d of 'Array'", i)
		}
		T = append(T, n)
	}
	return enc.Encode(T)
}

// decodeIntSlice handles decoding for "[integers]".
func decodeIntSlice(dec *gob.Decoder, lg log.Interface) (proxy.Message, error) {
	var T []*big.Int
	if err := dec.Decode(&T); err != nil {
		return nil, err
	}
	S := make([]string, 0, len(T))
	for _, n := range T {
		S = append(S, n.String())
	}
	return proxy.Message{
		"Array": S,
	}, nil
}

// encodeRatSlice handles encoding for "[rationals]".
//
// An Object is of the form:
//
//	{
//		"Array": [string, ..., string]	// required
//	}
//
// Here Array is an array of string representations of rationals.
func encodeRatSlice(val proxy.Message, enc *gob.Encoder, lg log.Interface) error {
	if err := val.OnlyKeys("Array"); err != nil {
		return err
	}
	S, ok, err := val.ToStringSlice("Array")
	if err != nil {
		return err
	} else if !ok {
		return errors.New("missing key 'Array'")
	}
	T := make([]*big.Rat, 0, len(S))
	for i, s := range S {
		q, ok := (&big.Rat{}).SetString(s)
		if !ok {
			return fmt.Errorf("unable to convert entry %d of 'Array'", i)
		}
		T = append(T, q)
	}
	return enc.Encode(T)
}

// decodeRatSlice handles decoding for "[rationals]".
func decodeRatSlice(dec *gob.Decoder, lg log.Interface) (proxy.Message, error) {
	var T []*big.Rat
	if err := dec.Decode(&T); err != nil {
		return nil, err
	}
	S := make([]string, 0, len(T))
	for _, q := range T {
		S = append(S, q.String())
	}
	return proxy.Message{
		"Array": S,
	}, nil
}
