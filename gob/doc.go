/*
Package gob is an endpoint for encoding and decoding of mathematical objects.

# Universes

Here we list the supported universes.

# [integers]

An Object is of the form:

	{
		"Array": [string, ..., string]	// required
	}

Here Array is an array of string representations of integers.

# [rationals]

An Object is of the form:

	{
		"Array": [string, ..., string]	// required
	}

Here Array is an array of string representations of rationals.

# Operations

Now we describe the operations permitted by this endpoint.

# universes

The Arguments are empty. The Result is of the form:

	{
		"Universes": [string, ..., string]
	}

Here Universes is an array of all supported universes.

# encode

The Arguments are of the form:

	{
		"Universe": string,		// required
		"Value": Object			// required
	}

Here the type Object of value depends on the universe. The Result is of the form:

	{
		"Universe": string,
		"Encoding": string
	}

# decode

The Arguments are of the form:

	{
		"Universe": string,		// required
		"Encoding": string		// required
	}

The Result is of the form:

	{
		"Universe": string,
		"Value": Object
	}

Here the type Object of value depends on the universe.
*/
package gob

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
