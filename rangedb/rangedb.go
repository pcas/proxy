// rangedb defines the handlers for rangedb requests

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangedb

import (
	"bitbucket.org/pcas/irange"
	rerrors "bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/log"
	"context"
	"encoding/json"
	"errors"
	"fmt"
)

// The default values for create requests.
const (
	defaultMaxRetries     = 5
	defaultMaxConcurrency = 1024
)

// defaultsFunc returns the defaults for connections on this endpoint.
type defaultsFunc func() proxy.Message

// fetchNextFunc fetches the next entry from the indicated storage. This entry must be released later, otherwise resources will leak.
type fetchNextFunc func(context.Context, proxy.Message) (irange.Entry, error)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// convertToInt64 attempts to convert x to an int64.
func convertToInt64(x interface{}) (int64, error) {
	if k, ok := x.(json.Number); ok {
		return k.Int64()
	}
	return convert.ToInt64(x)
}

// parseInterval attempts to convert the given value to an interval [a,b] and returns a and b.
//
// An Interval is defined by the JSON:
//
//	integer|[integer, integer]
//
// In the second case, the integers [a, b] must satisfy a <= b.
func parseInterval(x interface{}) (int64, int64, error) {
	// Is this a slice of int64s?
	if S, ok := x.([]int64); ok {
		if len(S) != 2 {
			return 0, 0, errors.New("value must be a slice of length two")
		}
		a, b := S[0], S[1]
		if a > b {
			return 0, 0, errors.New("value must be a slice sorted in increasing order")
		}
		return a, b, nil
	}
	// Is this a slice of ints?
	if S, ok := x.([]int); ok {
		if len(S) != 2 {
			return 0, 0, errors.New("value must be a slice of length two")
		}
		a, b := int64(S[0]), int64(S[1])
		if a > b {
			return 0, 0, errors.New("value must be a slice sorted in increasing order")
		}
		return a, b, nil
	}
	// Is this a slice of json.Numbers?
	if S, ok := x.([]json.Number); ok {
		if len(S) != 2 {
			return 0, 0, errors.New("value must be a slice of length two")
		}
		a, err := S[0].Int64()
		if err != nil {
			return 0, 0, fmt.Errorf("value must be a slice of integers: %w", err)
		}
		b, err := S[1].Int64()
		if err != nil {
			return 0, 0, fmt.Errorf("value must be a slice of integers: %w", err)
		}
		if a > b {
			return 0, 0, errors.New("value must be a slice sorted in increasing order")
		}
		return a, b, nil
	}
	// Is this a slice of interfaces?
	if S, ok := x.([]interface{}); ok {
		if len(S) != 2 {
			return 0, 0, errors.New("value must be a slice of length two")
		}
		a, err := convertToInt64(S[0])
		if err != nil {
			return 0, 0, fmt.Errorf("value must be a slice of integers: %w", err)
		}
		b, err := convertToInt64(S[1])
		if err != nil {
			return 0, 0, fmt.Errorf("value must be a slice of integers: %w", err)
		}
		if a > b {
			return 0, 0, errors.New("value must be a slice sorted in increasing order")
		}
		return a, b, nil
	}
	// Otherwise we assume this is a number and convert it to an int64
	n, err := convertToInt64(x)
	if err != nil {
		return 0, 0, err
	}
	return n, n, nil
}

// toRange attempts to return the entry for the given key as an [bitbucket.org/pcas/irange.Range]. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
//
// A Range can be defined by the JSON:
//
//	string
//
// Here the string is of the form:
//
//	"[A,B,...,C]"
//
// where each of A, B, ..., C are either integers or substrings of the form "E..F" where E and F are integers. (Note that '[' and ']' are part of the string defining the range, and are required.) Alternatively, a Range can be defined by the JSON:
//
//	[Interval, ..., Interval]
//
// A Range is used to encode an [bitbucket.org/pcas/irange.Range]. See [bitbucket.org/pcas/irange] for details.
func toRange(msg proxy.Message, key string) (irange.Range, bool, error) {
	// Is there anything to do?
	if msg == nil {
		return irange.Empty, false, nil
	}
	x, ok := msg[key]
	if !ok {
		return irange.Empty, false, nil
	}
	// Is this a string?
	if s, ok := x.(string); ok {
		// Parse the string
		r, err := irange.Parse(s)
		if err != nil {
			return irange.Empty, true, fmt.Errorf("error parsing '%s': %w", key, err)
		}
		return r, true, nil
	}
	// Is this a slice of int64s?
	r := irange.Range(irange.Empty)
	if S, ok := x.([]int64); ok {
		for _, s := range S {
			r = irange.Include(r, s)
		}
		return r, true, nil
	}
	// Is this a slice of ints?
	if S, ok := x.([]int); ok {
		for _, s := range S {
			r = irange.Include(r, int64(s))
		}
		return r, true, nil
	}
	// Is this a slice of json.Numbers?
	if S, ok := x.([]json.Number); ok {
		for i, s := range S {
			n, err := s.Int64()
			if err != nil {
				return irange.Empty, true, fmt.Errorf("error parsing '%s': unable to convert entry %d to an interval: %w", key, i, err)
			}
			r = irange.Include(r, n)
		}
		return r, true, nil
	}
	// Otherwise it must be a slice of interfaces
	S, ok := x.([]interface{})
	if !ok {
		return irange.Empty, true, fmt.Errorf("error parsing '%s': must be either a string or a slice", key)
	}
	// Build up the range
	for i, I := range S {
		a, b, err := parseInterval(I)
		if err != nil {
			return irange.Empty, true, fmt.Errorf("error parsing '%s': unable to convert entry %d to an interval: %w", key, i, err)
		}
		r = irange.Join(r, irange.ToInterval(a, b))
	}
	return r, true, nil
}

//////////////////////////////////////////////////////////////////////
// handler functions
//////////////////////////////////////////////////////////////////////

// defaults handles defaults requests.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Connection": Connection
//	}
//
// The result describes the default connection values.
func defaults(_ context.Context, args proxy.Message, df defaultsFunc, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	return proxy.Message{"Connection": df()}, nil
}

// next handles next requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection: Connection,
//		"Name": string,			// required
//		"AppName": string
//	}
//
// This will fetch a new entry from the range with given name. The Result is of the form:
//
//	{
//		"ID": ULID,
//		"Value": integer,
//		"Deadline": integer,
//		"Failures": integer
//	}
//
// The result will be empty if the end of iteration has been reached. Here Deadline is given by the nanoseconds elapsed since January 1, 1970 UTC (i.e. as a Unix time). For details of the meaning of these values see [bitbucket.org/pcas/irange].
func next(ctx context.Context, args proxy.Message, nextf fetchNextFunc, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name", "AppName"); err != nil {
		return nil, err
	}
	e, err := nextf(ctx, args)
	if err == rerrors.ErrEmpty {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	lg.Printf("Fetched task '%s'", e.ID())
	return proxy.Message{
		"ID":       e.ID().String(),
		"Value":    e.Value(),
		"Deadline": e.Deadline().UnixNano(),
		"Failures": e.Failures(),
	}, nil
}

// success handles success requests.
//
// The Arguments are of the form:
//
//	{
//		"ID": ULID	// required
//	}
//
// This will mark the entry with the given ID as having succeeded. The Result is empty.
func success(ctx context.Context, s irange.Storage, u irange.ID, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	err := s.Success(ctx, u)
	if err != nil {
		lg.Printf("Error marking task '%s' as succeeded: %v", u, err)
	} else {
		lg.Printf("Task '%s' marked as succeeded", u)
		err = args.OnlyKeys("ID")
	}
	return nil, err
}

// handleError handles error requests.
//
// The Arguments are of the form:
//
//	{
//		"ID": ULID	// required
//	}
//
// This will mark the entry with the given ID as having dropped into an error state. The Result is empty.
func handleError(ctx context.Context, s irange.Storage, u irange.ID, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	err := s.Error(ctx, u)
	if err != nil {
		lg.Printf("Error marking task '%s' as failed: %v", u, err)
	} else {
		lg.Printf("Task '%s' marked as failed", u)
		err = args.OnlyKeys("ID")
	}
	return nil, err
}

// requeue handles requeue requests.
//
// The Arguments are of the form:
//
//	{
//		"ID": ULID	// required
//	}
//
// This will requeue the entry with the given ID. The Result is empty.
func requeue(ctx context.Context, s irange.Storage, u irange.ID, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	err := s.Requeue(ctx, u)
	if err != nil {
		lg.Printf("Error requeuing task '%s': %v", u, err)
	} else {
		lg.Printf("Task '%s' requeued", u)
		err = args.OnlyKeys("ID")
	}
	return nil, err
}

// fatal handles fatal requests.
//
// The Arguments are of the form:
//
//	{
//		"ID": ULID	// required
//	}
//
// This will mark the entry with the given ID as having dropped into a fatal error state. The Result is empty.
func fatal(ctx context.Context, s irange.Storage, u irange.ID, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	err := s.Fatal(ctx, u)
	if err != nil {
		lg.Printf("Error marking task '%s' as failed with a fatal error: %v", u, err)
	} else {
		lg.Printf("Task '%s' marked as failed with a fatal error", u)
		err = args.OnlyKeys("ID")
	}
	return nil, err
}

// list handles list requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection
//	}
//
// This returns the names of all registered ranges. The Result is of the form:
//
//	{
//		"Names": [string, ..., string]
//	}
func list(ctx context.Context, s irange.Storage, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection"); err != nil {
		return nil, err
	}
	names, err := s.List(ctx)
	if err != nil {
		return nil, err
	} else if names == nil {
		names = []string{}
	}
	return proxy.Message{"Names": names}, nil
}

// create handles create requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection: Connection,
//		"Name": string,			// required
//		"Range": Range,			// required
//		"Lifetime": Duration,	// required
//		"MaxRetries": integer,
//		"MaxConcurrency": integer
//	}
//
// This will create a new range with given values. The duration Lifetime must be non-negative. The Result is empty.
func create(ctx context.Context, s irange.Storage, name string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name", "Range", "Lifetime", "MaxRetries", "MaxConcurrency"); err != nil {
		return nil, err
	}
	r, ok, err := toRange(args, "Range")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Range'")
	}
	lifetime, ok, err := args.ToDuration("Lifetime")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Lifetime'")
	} else if lifetime < 0 {
		return nil, errors.New("error parsing 'Lifetime': illegal negative duration")
	}
	maxRetries, ok, err := args.ToInt("MaxRetries")
	if err != nil {
		return nil, err
	} else if !ok {
		maxRetries = defaultMaxRetries
	}
	maxConcurrency, ok, err := args.ToInt("MaxConcurrency")
	if err != nil {
		return nil, err
	} else if !ok {
		maxConcurrency = defaultMaxConcurrency
	}
	return nil, s.Create(ctx, name, r, lifetime, maxRetries, maxConcurrency)
}

// handleDelete handles delete requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Name": string			// required
//	}
//
// This deletes the range with the given name. The Result is empty.
func handleDelete(ctx context.Context, s irange.Storage, name string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name"); err != nil {
		return nil, err
	}
	return nil, s.Delete(ctx, name)
}

// status handles status requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Name": string			// required
//	}
//
// This returns information about the status of the range with the given name. The Result is of the form:
//
//	{
//		"Pending": Range,
//		"Active": Range,
//		"Succeeded": Range,
//		"Failed": Range
//	}
//
// This describes an [bitbucket.org/pcas/irange.Status] object. For details of the meaning of these values see [bitbucket.org/pcas/irange].
func status(ctx context.Context, s irange.Storage, name string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name"); err != nil {
		return nil, err
	}
	d, err := s.Status(ctx, name)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"Pending":   d.Pending(),
		"Active":    d.Active(),
		"Succeeded": d.Succeeded(),
		"Failed":    d.Failed(),
	}, nil
}

// info handles info requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Name": string,			// required
//		"Entry": integer		// required
//	}
//
// This returns information about the given entry in the range with the given name. The Result is of the form:
//
//	{
//		"Value": integer,
//		"State": string,
//		"AppName": string,
//		"Hostname": string,
//		"Start": integer,
//		"Deadline": integer,
//		"Failures": integer
//	}
//
// This describes an [bitbucket.org/pcas/irange.Info] object. Here State is one of "uninitialised", "pending", "active", "succeeded", or "failed"; Start and Deadline are given by the nanoseconds elapsed since January 1, 1970 UTC (i.e. as a Unix time). For details of the meaning of these values see [bitbucket.org/pcas/irange].
func info(ctx context.Context, s irange.Storage, name string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name", "Entry"); err != nil {
		return nil, err
	}
	n, ok, err := args.ToInt64("Entry")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Entry'")
	}
	d, err := s.Info(ctx, name, n)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"Value":    d.Value(),
		"State":    d.State().String(),
		"AppName":  d.AppName(),
		"Hostname": d.Hostname(),
		"Start":    d.Start().UnixNano(),
		"Deadline": d.Deadline().UnixNano(),
		"Failures": d.Failures(),
	}, nil
}
