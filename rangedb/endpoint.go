// endpoint defines the endpoint for rangedb requests

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangedb

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/irange/rangedb"
	"bitbucket.org/pcas/irange/rangedb/rangedbflag"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"sync"
	"time"
)

// defaultRequeueTimeout is the default timeout when issuing requeue requests.
const defaultRequeueTimeout = 3 * time.Second

// storage is an irange.Storage with a logger and close method.
type storage interface {
	irange.Storage
	io.Closer
	log.Logable
}

// endpoint is the rangedb endpoint.
type endpoint struct {
	cfg       *rangedb.ClientConfig // The default client config
	cache     *cache                // The cache of open connections
	lg        log.Interface         // The logger
	m         sync.Mutex            // Mutex protecting the following
	taskcache map[irange.ID]storage // A map of task IDs to storage
}

// handleStorageFunc is a handle function that takes a storage.
type handleStorageFunc func(context.Context, irange.Storage, proxy.Message, log.Interface) (proxy.Message, error)

// handleStorageAndNameFunc is a handle function that takes a storage and name.
type handleStorageAndNameFunc func(context.Context, irange.Storage, string, proxy.Message, log.Interface) (proxy.Message, error)

// handleStorageAndIDFunc is a handle function that takes a storage and ID.
type handleStorageAndIDFunc func(context.Context, irange.Storage, irange.ID, proxy.Message, log.Interface) (proxy.Message, error)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

func init() {
	// Create the flag set
	flags := rangedbflag.NewSet(nil)
	// Register the endpoint
	proxy.Description{
		Name: "rangedb",
		Operations: []string{
			"defaults",
			"next",
			"success",
			"error",
			"requeue",
			"fatal",
			"list",
			"create",
			"delete",
			"status",
			"info",
		},
		Create: func(ctx context.Context, opts *proxy.Config, lg log.Interface) (proxy.Endpoint, error) {
			// Create the config
			cfg := flags.ClientConfig()
			cfg.SSLDisabled = opts.SSLDisabled
			cfg.SSLCert = opts.SSLCert
			// Return the new endpoint
			return newEndpoint(ctx, cfg, lg)
		},
		FlagSet: flags,
	}.Register()
}

// createConfig modifies (a copy of) the client configuration, with values updated with those in msg.
func createConfig(cfg *rangedb.ClientConfig, msg proxy.Message) (*rangedb.ClientConfig, error) {
	// Sanity check on the keys
	if err := msg.OnlyKeys("Address"); err != nil {
		return nil, err
	}
	// Pass to a copy
	cfg = cfg.Copy()
	// Add any options set by the user
	if s, ok, err := msg.ToString("Address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	// Validate the configuration before returning
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

// requeueTask requeues the task with given ID on the given storage.
func requeueTask(s irange.Storage, id irange.ID) error {
	ctx, cancel := context.WithTimeout(context.Background(), defaultRequeueTimeout)
	defer cancel()
	return s.Requeue(ctx, id)
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// Close closes the endpoint.
func (e *endpoint) Close() error {
	// Acquire a lock on the task cache
	e.m.Lock()
	defer e.m.Unlock()
	// Requeue any open tasks
	if e.taskcache != nil {
		for u, s := range e.taskcache {
			if err := requeueTask(s, u); err != nil {
				e.Log().Printf("Error requeuing task '%s': %v", u, err)
			} else {
				e.Log().Printf("Task '%s' requeued", u)
			}
			s.Close() // Ignore any error
		}
		e.taskcache = nil
	}
	// Close the cache
	return e.cache.Close()
}

// defaults returns the default backend-specific Connection values for this endpoint.
//
// The Connection values JSON is of the form:
//
//	{
//		"Address": string
//	}
func (e *endpoint) defaults() proxy.Message {
	return proxy.Message{
		"Address": e.cfg.Address.URI(),
	}
}

// storage returns the connection to the storage.
func (e *endpoint) storage(ctx context.Context, args proxy.Message) (storage, error) {
	// Extract the connection configuration (if any)
	msg, _, err := args.ToMessage("Connection")
	if err != nil {
		return nil, err
	}
	// Build the config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, err
	}
	// Open the connection to the storage
	s, err := e.cache.Get(ctx, cfg)
	if err != nil {
		return nil, err
	}
	// Set the logger and return
	s.SetLogger(log.PrefixWith(e.Log(), "[connection]"))
	return s, nil
}

// fetchNext calls Next on the storage indicated by args, returning the fetched entry. This entry must be released later, otherwise resources will leak.
func (e *endpoint) fetchNext(ctx context.Context, args proxy.Message) (irange.Entry, error) {
	// Fetch the name
	name, ok, err := args.ToString("Name")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Name'")
	}
	// Fetch the app name
	appName, _, err := args.ToString("AppName")
	if err != nil {
		return nil, err
	}
	// Fetch the hostname
	hostname, _ := os.Hostname() // Ignore any error
	// Build a connection to the storage
	s, err := e.storage(ctx, args)
	if err != nil {
		return nil, err
	}
	// Fetch the next entry
	entry, err := s.Next(ctx, name, &irange.Metadata{
		AppName:  appName,
		Hostname: hostname,
	})
	if err != nil {
		return nil, err
	}
	// We need to cache this connection for the lifetime of the task
	e.m.Lock()
	defer e.m.Unlock()
	if e.taskcache == nil {
		e.taskcache = make(map[irange.ID]storage)
	}
	e.taskcache[entry.ID()] = s
	// Return the entry
	return entry, nil
}

// withStorageAndID calls the given handle function with storage and ID. The storage and ID will be released after call.
func (e *endpoint) withStorageAndID(ctx context.Context, f handleStorageAndIDFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Fetch the ID
	uu, ok, err := args.ToULID("ID")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'ID'")
	}
	u := irange.ID(uu)
	// Recover the storage for this ID
	var s storage
	ok = false
	e.m.Lock()
	if e.taskcache != nil {
		s, ok = e.taskcache[u]
	}
	e.m.Unlock()
	if !ok {
		return nil, fmt.Errorf("unknown task ID: %s", u)
	}
	// Defer closing the storage
	defer s.Close() // Ignore any error
	// Defer removing the storage
	defer func() {
		e.m.Lock()
		defer e.m.Unlock()
		if e.taskcache != nil {
			delete(e.taskcache, u)
		}
	}()
	// Call the user's function
	return f(ctx, s, u, args, lg)
}

// withStorage calls the given handle function with storage.
func (e *endpoint) withStorage(ctx context.Context, f handleStorageFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Fetch the storage
	s, err := e.storage(ctx, args)
	if err != nil {
		return nil, err
	}
	defer s.Close()
	// Call the handle function
	return f(ctx, s, args, lg)
}

// withStorageAndName calls the given handle function with storage and name.
func (e *endpoint) withStorageAndName(ctx context.Context, f handleStorageAndNameFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Fetch the name
	name, ok, err := args.ToString("Name")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Name'")
	}
	// Wrap the call
	return e.withStorage(ctx, func(ctx context.Context, s irange.Storage, args proxy.Message, lg log.Interface) (proxy.Message, error) {
		return f(ctx, s, name, args, lg)
	}, args, lg)
}

// Handle handles requests for operation 'op' with arguments 'args'.
func (e *endpoint) Handle(ctx context.Context, op string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	switch op {
	case "defaults":
		return defaults(ctx, args, e.defaults, lg)
	case "next":
		return next(ctx, args, e.fetchNext, lg)
	case "success":
		return e.withStorageAndID(ctx, success, args, lg)
	case "error":
		return e.withStorageAndID(ctx, handleError, args, lg)
	case "requeue":
		return e.withStorageAndID(ctx, requeue, args, lg)
	case "fatal":
		return e.withStorageAndID(ctx, fatal, args, lg)
	case "list":
		return e.withStorage(ctx, list, args, lg)
	case "create":
		return e.withStorageAndName(ctx, create, args, lg)
	case "delete":
		return e.withStorageAndName(ctx, handleDelete, args, lg)
	case "status":
		return e.withStorageAndName(ctx, status, args, lg)
	case "info":
		return e.withStorageAndName(ctx, info, args, lg)
	default:
		return nil, fmt.Errorf("unknown operation: %s", op)
	}
}

// Log returns the logger.
func (e *endpoint) Log() log.Interface {
	return e.lg
}

// newEndpoint returns a new endpoint using the given default config.
func newEndpoint(ctx context.Context, cfg *rangedb.ClientConfig, lg log.Interface) (proxy.Endpoint, error) {
	return &endpoint{
		cfg:   cfg,
		cache: newCache(log.PrefixWith(lg, "[cache]")),
		lg:    lg,
	}, nil
}
