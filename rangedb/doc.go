/*
Package rangedb is an endpoint for rangedb. See [bitbucket.org/pcas/irange].

# Types

First we describe the JSON types specific to this endpoint.

# Connection

The Connection object describes the connection details to rangedb:

	{
		"Address": string
	}

Any values set in Connection are optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation. For a description of the meaning of these settings, see [bitbucket.org/pcas/irange/rangedb].

# Interval

An Interval is defined by the JSON:

	integer|[integer, integer]

In the second case, the integers [a, b] must satisfy a <= b. An Interval is used to encode an [bitbucket.org/pcas/irange.Interval].

# Range

A Range can be defined by the JSON:

	string

Here the string is of the form:

	"[A,B,...,C]"

where each of A, B, ..., C are either integers or substrings of the form "E..F" where E and F are integers. (Note that '[' and ']' are part of the string defining the range, and are required.) Alternatively, a Range can be defined by the JSON:

	[Interval, ..., Interval]

A Range is used to encode an [bitbucket.org/pcas/irange.Range].

# Operations

Now we describe the operations permitted by this endpoint.

# defaults

The Arguments are empty. The Result is of the form:

	{
		"Connection": Connection
	}

The result describes the default connection values.

# next

The Arguments are of the form:

	{
		"Connection: Connection,
		"Name": string,			// required
		"AppName": string
	}

This will fetch a new entry from the range with given name. The Result is of the form:

	{
		"ID": ULID,
		"Value": integer,
		"Deadline": integer,
		"Failures": integer
	}

The result will be empty if the end of iteration has been reached. Here Deadline is given by the nanoseconds elapsed since January 1, 1970 UTC (i.e. as a Unix time). For details of the meaning of these values see [bitbucket.org/pcas/irange].

# success

The Arguments are of the form:

	{
		"ID": ULID	// required
	}

This will mark the entry with the given ID as having succeeded. The Result is empty.

# error

The Arguments are of the form:

	{
		"ID": ULID	// required
	}

This will mark the entry with the given ID as having dropped into an error state. The Result is empty.

# requeue

The Arguments are of the form:

	{
		"ID": ULID	// required
	}

This will requeue the entry with the given ID. The Result is empty.

# fatal

The Arguments are of the form:

	{
		"ID": ULID	// required
	}

This will mark the entry with the given ID as having dropped into a fatal error state. The Result is empty.

# list

The Arguments are of the form:

	{
		"Connection": Connection
	}

This returns the names of all registered ranges. The Result is of the form:

	{
		"Names": [string, ..., string]
	}

# create

The Arguments are of the form:

	{
		"Connection: Connection,
		"Name": string,			// required
		"Range": Range,			// required
		"Lifetime": Duration,	// required
		"MaxRetries": integer,
		"MaxConcurrency": integer
	}

This will create a new range with given values. The duration Lifetime must be non-negative. The Result is empty.

# delete

The Arguments are of the form:

	{
		"Connection": Connection,
		"Name": string			// required
	}

This deletes the range with the given name. The Result is empty.

# status

The Arguments are of the form:

	{
		"Connection": Connection,
		"Name": string			// required
	}

This returns information about the status of the range with the given name. The Result is of the form:

	{
		"Pending": Range,
		"Active": Range,
		"Succeeded": Range,
		"Failed": Range
	}

This describes an [bitbucket.org/pcas/irange.Status] object.

# info

The Arguments are of the form:

	{
		"Connection": Connection,
		"Name": string,			// required
		"Entry": integer		// required
	}

This returns information about the given entry in the range with the given name. The Result is of the form:

	{
		"Value": integer,
		"State": string,
		"AppName": string,
		"Hostname": string,
		"Start": integer,
		"Deadline": integer,
		"Failures": integer
	}

This describes an [bitbucket.org/pcas/irange.Info] object. Here State is one of "uninitialised", "pending", "active", "succeeded", or "failed"; Start and Deadline are given by the nanoseconds elapsed since January 1, 1970 UTC (i.e. as a Unix time). For details of the meaning of these values see [bitbucket.org/pcas/irange].
*/
package rangedb

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
