// Cache defines a cache of queue client connections.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queue

import (
	"context"

	stdCache "bitbucket.org/pcas/proxy/internal/cache"
	"bitbucket.org/pcas/queue"
	"bitbucket.org/pcas/queue/queued"
	"bitbucket.org/pcastools/log"
)

// value is a value in the cache.
type value struct {
	queue.Connection
	release func() // The release function
}

// cache is a cache of queues accessed by their config and queue name.
type cache struct {
	cache *stdCache.Cache[*queued.ClientConfig, queue.Connection]
}

//////////////////////////////////////////////////////////////////////
// value functions
//////////////////////////////////////////////////////////////////////

// Close closes the value.
func (v *value) Close() error {
	v.release()
	return nil
}

//////////////////////////////////////////////////////////////////////
// cache functions
//////////////////////////////////////////////////////////////////////

// Close closes the cache.
func (c *cache) Close() error {
	return c.cache.Close()
}

// Get returns a queue system with the given config.
func (c *cache) Get(ctx context.Context, cfg *queued.ClientConfig) (*value, error) {
	e, err := c.cache.Get(ctx, cfg)
	if err != nil {
		return nil, err
	}
	return &value{
		Connection: e.Value(),
		release:    e.Release,
	}, nil
}

// newConnection returns a new queue connection based on queued with the given config.
func newConnection(ctx context.Context, cfg *queued.ClientConfig) (queue.Connection, error) {
	// Create the driver
	d, err := queued.NewClient(ctx, cfg)
	if err != nil {
		return nil, err
	}
	return queue.NewConnection(d), nil
}

// newCache returns a new cache of queue systems.
func newCache(lg log.Interface) *cache {
	return &cache{
		cache: stdCache.New(newConnection, queued.AreEqual, lg),
	}
}
