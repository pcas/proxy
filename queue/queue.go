// queue defines the handlers for queue requests

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queue

import (
	"context"
	"errors"
	"time"

	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcas/queue"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
)

// defaultsFunc returns the defaults for connections on this endpoint.
type defaultsFunc func() proxy.Message

//////////////////////////////////////////////////////////////////////
// handler functions
//////////////////////////////////////////////////////////////////////

// defaults handles defaults requests.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Connection": Connection
//	}
//
// The result describes the default connection values.
func defaults(_ context.Context, args proxy.Message, df defaultsFunc, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	return proxy.Message{"Connection": df()}, nil
}

// handleGet handles get requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection: Connection,
//		"Name": string,			// required
//		"AppName": string
//	}
//
// This will fetch a new message from the queue with the given name. The Result is of the form:
//
//	{
//		"Message": string,
//	}
func (e *endpoint) handleGet(ctx context.Context, q queue.Queue, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name", "AppName"); err != nil {
		return nil, err
	}
	msg, err := q.Get(ctx)
	if err != nil {
		lg.Printf("Error getting message from queue %s: %v", q.Name(), err)
		return nil, err
	}
	lg.Printf("Fetched message from queue %s", q.Name())
	// Create a ULID
	u, err := ulid.New()
	for err != nil {
		time.Sleep(100 * time.Nanosecond)
		u, err = ulid.New()
	}
	// Cache the message
	e.m.Lock()
	defer e.m.Unlock()
	e.messages[u] = msg
	return proxy.Message{
		"ID":      u.String(),
		"Message": string(msg.Content()),
	}, nil
}

// handleAcknowledge handles acknowledge requests.
// The Arguments are of the form:
//
//	{
//		"Connection: Connection,
//		"ID": ULID,				// required
//		"AppName": string
//	}
//
// This will acknowledge the message msg, deleting it from the cache of known messages. The argument u should be the key of msg in the cache. The Result is empty.
func (e *endpoint) handleAcknowledge(ctx context.Context, msg queue.Message, u ulid.ULID, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "ID", "AppName"); err != nil {
		return nil, err
	}
	// Acknowledge the message
	if err := msg.Acknowledge(ctx); err != nil {
		lg.Printf("Error acknowledging message: %v", err)
		return nil, err
	}
	// Remove the message from the cache
	e.m.Lock()
	delete(e.messages, u)
	e.m.Unlock()
	lg.Printf("Acknowledged message with ID %v", u)
	return nil, nil
}

// handleRequeue handles requeue requests.
// The Arguments are of the form:
//
//	{
//		"Connection: Connection,
//		"ID": ULID,				// required
//		"AppName": string
//	}
//
// This will requeue the message msg, deleting it from the cache of known messages. The argument u should be the key of msg in the cache. The Result is empty.
func (e *endpoint) handleRequeue(ctx context.Context, msg queue.Message, u ulid.ULID, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "ID", "AppName"); err != nil {
		return nil, err
	}
	// Requeue the message
	if err := msg.Requeue(ctx); err != nil {
		lg.Printf("Error requeuing message: %v", err)
		return nil, err
	}
	// Remove the message from the cache
	e.m.Lock()
	delete(e.messages, u)
	e.m.Unlock()
	lg.Printf("Requeued message with ID %v", u)
	return nil, nil
}

// handlePut handles put requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection: Connection,
//		"Name": string,			// required
//		"AppName": string
//		"Message": string		// required
//	}
//
// This will put the given message onto the queue with the given name. The Result is empty.
func handlePut(ctx context.Context, q queue.Queue, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name", "AppName", "Message"); err != nil {
		return nil, err
	}
	// Recover the message
	msg, ok, err := args.ToString("Message")
	if err != nil {
		lg.Printf("Error parsing Message field: %v", err)
		return nil, err
	} else if !ok {
		lg.Printf("Error: missing Message field")
		return nil, errors.New("missing Message field")
	}
	// Put the message on the queue
	if err := q.Put(ctx, []byte(msg)); err != nil {
		lg.Printf("Error in Put: %v", err)
		return nil, err
	}
	lg.Printf("Put succeeded")
	return nil, nil
}

// handleLength handles length requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection: Connection,
//		"Name": string,			// required
//		"AppName": string
//	}
//
// This will return the number of messages in the queue with the given name. The Result is of the form:
//
//	{
//		"Length": integer,
//	}
func handleLength(ctx context.Context, q queue.Queue, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name", "AppName"); err != nil {
		return nil, err
	}
	// Ask for the length
	n, err := q.Len(ctx)
	if err != nil {
		lg.Printf("Error getting length of queue %s: %v", q.Name(), err)
		return nil, err
	}
	return proxy.Message{
		"Length": n,
	}, nil
}

// handleDelete handles delete requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Name": string			// required
//		"AppName": string
//	}
//
// This deletes the queue with the given name. The Result is empty.
func handleDelete(ctx context.Context, c queue.Connection, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Name", "AppName"); err != nil {
		return nil, err
	}
	// Extract the name
	name, ok, err := args.ToString("Name")
	if err != nil {
		lg.Printf("Error parsing Name field: %v", err)
		return nil, err
	} else if !ok {
		lg.Printf("Error: missing Name field")
		return nil, errors.New("missing Name field")
	}
	// Delete the queue
	if err := c.Delete(ctx, name); err != nil {
		lg.Printf("Error performing delete: %v", err)
		return nil, err
	}
	return nil, nil
}
