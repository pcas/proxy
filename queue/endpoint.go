// endpoint defines the endpoint for queue requests

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package queue

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcas/queue"
	"bitbucket.org/pcas/queue/queued"
	"bitbucket.org/pcas/queue/queued/queueflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
)

// endpoint is the queue endpoint.
type endpoint struct {
	cfg      *queued.ClientConfig        // The default client config
	cache    *cache                      // The cache of open connections
	lg       log.Interface               // The logger
	m        sync.Mutex                  // Mutex protecting the following
	messages map[ulid.ULID]queue.Message // The cache of known messages, indexed by message ID
	isClosed bool                        // Are we closed?
	closeErr error                       // The error on close, if any
}

// handleQueueFunc represents a function for processing an operation on a queue.
type handleQueueFunc func(context.Context, queue.Queue, proxy.Message, log.Interface) (proxy.Message, error)

// handleMessageFunc represents a function for processing an operation on a message.
type handleMessageFunc func(context.Context, queue.Message, ulid.ULID, proxy.Message, log.Interface) (proxy.Message, error)

// handleConnectionFunc represents a function for processing an operation on a queue connection.
type handleConnectionFunc func(context.Context, queue.Connection, proxy.Message, log.Interface) (proxy.Message, error)

// init registers the endpoint
func init() {
	// Create the flag set
	flags := queueflag.NewSet(nil)
	// Register the endpoint
	proxy.Description{
		Name: "queue",
		Operations: []string{
			"defaults",
			"get",
			"acknowledge",
			"requeue",
			"put",
			"length",
			"delete",
		},
		Create: func(ctx context.Context, opts *proxy.Config, lg log.Interface) (proxy.Endpoint, error) {
			// Create the config
			cfg := flags.ClientConfig()
			cfg.SSLDisabled = opts.SSLDisabled
			cfg.SSLCert = opts.SSLCert
			// Return the new endpoint
			return newEndpoint(ctx, cfg, lg)
		},
		FlagSet: flags,
	}.Register()
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// createConfig modifies (a copy of) the client configuration, with values updated with those in msg.
func createConfig(cfg *queued.ClientConfig, msg proxy.Message) (*queued.ClientConfig, error) {
	// Sanity check on the keys
	if err := msg.OnlyKeys("Address"); err != nil {
		return nil, err
	}
	// Pass to a copy
	cfg = cfg.Copy()
	// Add any options set by the user
	if s, ok, err := msg.ToString("Address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	// Validate the configuration before returning
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// Close closes the endpoint.
func (e *endpoint) Close() error {
	e.m.Lock()
	defer e.m.Unlock()
	if !e.isClosed {
		// Requeue all the messages
		for _, msg := range e.messages {
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			if err := msg.Requeue(ctx); err != nil {
				e.closeErr = err
			}
		}
		// Zero out the message cache
		e.messages = nil
		// Close the cache
		if err := e.cache.Close(); err != nil {
			e.closeErr = err
		}
	}
	return e.closeErr
}

// defaults returns the default backend-specific Connection values for this endpoint.
//
// The Connection values JSON is of the form:
//
//	{
//		"Address": string
//	}
func (e *endpoint) defaults() proxy.Message {
	return proxy.Message{
		"Address": e.cfg.Address.URI(),
	}
}

// withQueue calls the given handle function with the queue specified by args.
func (e *endpoint) withQueue(ctx context.Context, f handleQueueFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Extract the connection configuration (if any)
	msg, _, err := args.ToMessage("Connection")
	if err != nil {
		lg.Printf("Error parsing Connection field: %v", err)
		return nil, err
	}
	// Build the config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		lg.Printf("Error creating client config: %v", err)
		return nil, err
	}
	// Recover the connection
	c, err := e.cache.Get(ctx, cfg)
	if err != nil {
		lg.Printf("Error recovering connection: %v", err)
		return nil, err
	}
	// Extract the queue name
	name, ok, err := args.ToString("Name")
	if err != nil {
		lg.Printf("Error parsing Name field: %v", err)
		return nil, err
	} else if !ok {
		lg.Printf("Error: missing queue name")
		return nil, errors.New("missing queue name")
	}
	// Recover the queue
	q, err := c.Queue(ctx, name)
	if err != nil {
		lg.Printf("Error recovering queue: %v", err)
		return nil, err
	}
	// Call the user's function
	return f(ctx, q, args, lg)
}

// withMessageAndID calls the given handle function with the message and message ID specified by args.
func (e *endpoint) withMessageAndID(ctx context.Context, f handleMessageFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Extract the message ID
	id, ok, err := args.ToString("ID")
	if err != nil {
		lg.Printf("Error parsing ID field: %v", err)
		return nil, err
	} else if !ok {
		lg.Printf("Error: missing message ID")
		return nil, errors.New("missing message ID")
	}
	// Recover the ULID
	u, err := ulid.FromString(id)
	if err != nil {
		lg.Printf("Error parsing message ID: %v", err)
		return nil, err
	}
	// Recover the message
	e.m.Lock()
	msg, ok := e.messages[u]
	e.m.Unlock()
	if !ok {
		lg.Printf("Error: unknown message ID")
		return nil, errors.New("unknown message ID")
	}
	// Call the user's function
	return f(ctx, msg, u, args, lg)
}

// withSystem calls the given handle function with the queue system specified by args.
func (e *endpoint) withSystem(ctx context.Context, f handleConnectionFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Extract the connection configuration (if any)
	msg, _, err := args.ToMessage("Connection")
	if err != nil {
		lg.Printf("Error parsing Connection field: %v", err)
		return nil, err
	}
	// Build the config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		lg.Printf("Error creating client config: %v", err)
		return nil, err
	}
	// Recover the queue system
	s, err := e.cache.Get(ctx, cfg)
	if err != nil {
		lg.Printf("Error recovering queue system: %v", err)
		return nil, err
	}
	// Call the user's function
	return f(ctx, s, args, lg)
}

// Handle handles requests for operation 'op' with arguments 'args'.
func (e *endpoint) Handle(ctx context.Context, op string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	switch op {
	case "defaults":
		return defaults(ctx, args, e.defaults, lg)
	case "get":
		return e.withQueue(ctx, e.handleGet, args, lg)
	case "acknowledge":
		return e.withMessageAndID(ctx, e.handleAcknowledge, args, lg)
	case "requeue":
		return e.withMessageAndID(ctx, e.handleRequeue, args, lg)
	case "put":
		return e.withQueue(ctx, handlePut, args, lg)
	case "length":
		return e.withQueue(ctx, handleLength, args, lg)
	case "delete":
		return e.withSystem(ctx, handleDelete, args, lg)
	default:
		return nil, fmt.Errorf("unknown operation: %s", op)
	}
}

// Log returns the logger.
func (e *endpoint) Log() log.Interface {
	return e.lg
}

// newEndpoint returns a new endpoint using the given default config.
func newEndpoint(ctx context.Context, cfg *queued.ClientConfig, lg log.Interface) (proxy.Endpoint, error) {
	return &endpoint{
		cfg:      cfg,
		cache:    newCache(log.PrefixWith(lg, "[cache]")),
		lg:       lg,
		messages: make(map[ulid.ULID]queue.Message),
	}, nil
}
