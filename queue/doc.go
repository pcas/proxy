/*
Package queue is an endpoint for the pcas queue system. See [bitbucket.org/pcas/queue].

# Types

First we describe the JSON types specific to this endpoint.

# Connection

The Connection object describes the connection details:

	{
		"Address": string
	}

Any values set in Connection are optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation. For a description of the meaning of these settings, see [bitbucket.org/pcas/queue/queued].

# Operations

Now we describe the operations permitted by this endpoint.

# defaults

The Arguments are empty. The Result is of the form:

	{
		"Connection": Connection
	}

The result describes the default connection values.

# get

The Arguments are of the form:

	{
		"Connection: Connection,
		"Name": string,			// required
		"AppName": string
	}

This will fetch a new message from the queue with the given name. The Result is of the form:

	{
		"ID": ULID,
		"Message": string,
	}

# acknowledge

The Arguments are of the form:

	{
		"Connection: Connection,
		"ID": ULID,				// required
		"AppName": string
	}

This will acknowledge the message with the given ID. The Result is empty.

# requeue

The Arguments are of the form:

	{
		"Connection: Connection,
		"ID": ULID,				// required
		"AppName": string
	}

This will requeue the message with the given ID. The Result is empty.

# put

The Arguments are of the form:

	{
		"Connection: Connection,
		"Name": string,			// required
		"AppName": string
		"Message": string		// required
	}

This will put the given message onto the queue with the given name. The Result is empty.

# length

The Arguments are of the form:

	{
		"Connection: Connection,
		"Name": string,			// required
		"AppName": string
	}

This will return the number of messages in the queue with the given name. The Result is of the form:

	{
		"Length": integer,
	}

# delete

The Arguments are of the form:

	{
		"Connection": Connection,
		"Name": string			// required
		"AppName": string
	}

This deletes the queue with the given name. The Result is empty.

*/
package queue

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
