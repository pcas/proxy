/*
Package sqlite is an endpoint for the SQLite key-value driver. See [bitbucket.org/pcas/keyvalue/sqlite]. Sqlite is a concrete implementation of the [bitbucket.org/pcas/proxy/keyvalue] abstract endpoint.

# Types

We describe the JSON types specific to this endpoint.

# Connection
The Connection object describes the connection details to SQLite:

	{
		"Path": string
	}

Any values set in Connection are optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation.
*/
package sqlite

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
