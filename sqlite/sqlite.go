// Sqlite provides an endpoint handler for keyvalue database requests backed by SQLite.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package sqlite

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/sqlite"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcas/proxy/internal/keyvalue/cache"
	proxykeyvalue "bitbucket.org/pcas/proxy/keyvalue"
	"bitbucket.org/pcastools/hash"
	"bitbucket.org/pcastools/log"
	"context"
	"io/ioutil"
	"os"
)

// path describes the path to an SQLite database.
type path string

// endpoint is a proxy/keyvalue endpoint.
type endpoint struct {
	defaultPath string             // The default path
	cache       *cache.Cache[path] // The cache of open connections
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init registers this endpoint.
func init() {
	proxykeyvalue.Description{
		Name: "sqlite",
		Create: func(_ context.Context, _ *proxy.Config, lg log.Interface) (proxykeyvalue.Endpoint, error) {
			return newEndpoint(lg), nil
		},
	}.Register()
}

// createTempPath create a temporary file and returns the path.
func createTempPath() (string, error) {
	fh, err := ioutil.TempFile("", "*.db")
	if err != nil {
		return "", err
	}
	p := fh.Name()
	if err = fh.Close(); err != nil {
		return "", err
	}
	return p, nil
}

// createPath returns the path given in msg.
func createPath(msg proxy.Message) (string, error) {
	if err := msg.OnlyKeys("Path"); err != nil {
		return "", err
	}
	s, _, err := msg.ToString("Path")
	if err != nil {
		return "", err
	}
	return s, nil
}

//////////////////////////////////////////////////////////////////////
// path functions
//////////////////////////////////////////////////////////////////////

// Hash returns a hash for the path.
func (p path) Hash() uint32 {
	return hash.String(string(p))
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// newEndpoint returns a new proxy/keyvalue endpoint.
func newEndpoint(lg log.Interface) proxykeyvalue.Endpoint {
	lg = log.PrefixWith(lg, "[cache]")
	return &endpoint{
		cache: cache.New(
			func(ctx context.Context, p path) (keyvalue.Connection, error) {
				return sqlite.Open(ctx, string(p))
			},
			func(p1 path, p2 path) bool {
				return p1 == p2
			},
			lg,
		),
	}
}

// Close closes the endpoint.
func (e *endpoint) Close() error {
	// Close the cache
	err := e.cache.Close()
	// Delete the temp db file
	if len(e.defaultPath) != 0 {
		os.Remove(e.defaultPath) // Ignore any errors
	}
	return err
}

// Connection returns a connection using the given connection values.
func (e *endpoint) Connection(ctx context.Context, msg proxy.Message) (keyvalue.Connection, error) {
	// Create the path
	p, err := createPath(msg)
	if err != nil {
		return nil, err
	} else if len(p) == 0 {
		// No path was given, so we use (or create) a temp db file
		if len(e.defaultPath) == 0 {
			q, err := createTempPath()
			if err != nil {
				return nil, err
			}
			e.defaultPath = q
		}
		p = e.defaultPath
	}
	// Recover (or open) a connection from the cache
	return e.cache.Get(ctx, path(p))
}

// Defaults returns the default backend-specific connection values for this endpoint.
//
// The connection values JSON is of the form:
//
//	{
//		"Path": string
//	}
func (*endpoint) Defaults() proxy.Message {
	return proxy.Message{
		"Path": "",
	}
}
