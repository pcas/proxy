// Postgres provides an endpoint handler for keyvalue database requests backed by postgres.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package postgres

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/postgres"
	"bitbucket.org/pcas/keyvalue/postgres/postgresflag"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcas/proxy/internal/keyvalue/cache"
	proxykeyvalue "bitbucket.org/pcas/proxy/keyvalue"
	"bitbucket.org/pcastools/log"
	"context"
)

// endpoint is a proxy/keyvalue endpoint.
type endpoint struct {
	cfg   *postgres.ClientConfig               // The default client config
	cache *cache.Cache[*postgres.ClientConfig] // The cache of open connections
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init registers this endpoint.
func init() {
	flags := postgresflag.NewSet(nil)
	proxykeyvalue.Description{
		Name: "postgres",
		Create: func(_ context.Context, _ *proxy.Config, lg log.Interface) (proxykeyvalue.Endpoint, error) {
			return newEndpoint(flags.ClientConfig(), lg), nil
		},
		FlagSet: flags,
	}.Register()
}

// createConfig modifies (a copy of) the client configuration, with values updated with those in msg.
func createConfig(cfg *postgres.ClientConfig, msg proxy.Message) (*postgres.ClientConfig, error) {
	// Sanity check on the keys
	if err := msg.OnlyKeys("AppName", "Hosts", "Username", "Password",
		"PasswordSet", "ConnectTimeout", "RequireSSL", "FastCopy"); err != nil {
		return nil, err
	}
	// Pass to a copy
	cfg = cfg.Copy()
	// Add any options set by the user
	if s, ok, err := msg.ToString("AppName"); ok {
		if err != nil {
			return nil, err
		}
		cfg.AppName = s
	}
	if S, ok, err := msg.ToStringSlice("Hosts"); ok {
		if err != nil {
			return nil, err
		}
		cfg.Hosts = S
	}
	if s, ok, err := msg.ToString("Username"); ok {
		if err != nil {
			return nil, err
		}
		cfg.Username = s
	}
	if s, ok, err := msg.ToString("Password"); ok {
		if err != nil {
			return nil, err
		}
		cfg.Password = s
	}
	if b, ok, err := msg.ToBool("PasswordSet"); ok {
		if err != nil {
			return nil, err
		}
		cfg.PasswordSet = b
	}
	if n, ok, err := msg.ToInt("ConnectTimeout"); ok {
		if err != nil {
			return nil, err
		}
		cfg.ConnectTimeout = n
	}
	if b, ok, err := msg.ToBool("RequireSSL"); ok {
		if err != nil {
			return nil, err
		}
		cfg.RequireSSL = b
	}
	if b, ok, err := msg.ToBool("FastCopy"); ok {
		if err != nil {
			return nil, err
		}
		cfg.FastCopy = b
	}
	// Validate the configuration before returning
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// newEndpoint returns a new proxy/keyvalue endpoint.
func newEndpoint(cfg *postgres.ClientConfig, lg log.Interface) proxykeyvalue.Endpoint {
	lg = log.PrefixWith(lg, "[cache]")
	return &endpoint{
		cfg:   cfg,
		cache: cache.New(postgres.Open, postgres.AreEqual, lg),
	}
}

// Close closes the endpoint.
func (e *endpoint) Close() error {
	return e.cache.Close()
}

// Connection returns a connection using the given connection values.
func (e *endpoint) Connection(ctx context.Context, msg proxy.Message) (keyvalue.Connection, error) {
	// Create the client config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, err
	}
	// Recover (or open) a connection from the cache
	return e.cache.Get(ctx, cfg)
}

// Defaults returns the default backend-specific connection values for this endpoint.
//
// The connection values JSON is of the form:
//
//	{
//		"AppName": string,
//		"Hosts": [string, ..., string],
//		"Username": string,
//		"Password": string,
//		"PasswordSet": boolean,
//		"ConnectTimeout": integer,
//		"RequireSSL": boolean,
//		"FastCopy": boolean
//	}
func (e *endpoint) Defaults() proxy.Message {
	hosts := e.cfg.Hosts
	if hosts == nil {
		hosts = []string{}
	}
	return proxy.Message{
		"AppName":        e.cfg.AppName,
		"Hosts":          hosts,
		"Username":       e.cfg.Username,
		"Password":       e.cfg.Password,
		"PasswordSet":    e.cfg.PasswordSet,
		"ConnectTimeout": e.cfg.ConnectTimeout,
		"RequireSSL":     e.cfg.RequireSSL,
		"FastCopy":       e.cfg.FastCopy,
	}
}
