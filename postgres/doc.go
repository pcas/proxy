/*
Package postgres is an endpoint for the postgres key-value driver. See [bitbucket.org/pcas/keyvalue/postgres]. Postgres is a concrete implementation of the [bitbucket.org/pcas/proxy/keyvalue] abstract endpoint.

# Types

We describe the JSON types specific to this endpoint.

# Connection

The Connection object describes the connection details to postgres:

	{
		"AppName": string,
		"Hosts": [string, ..., string],
		"Username": string,
		"Password": string,
		"PasswordSet": boolean,
		"ConnectTimeout": integer,
		"RequireSSL": boolean,
		"FastCopy": boolean
	}

Any values set in Connection are optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation. For a description of the meaning of these settings, see [bitbucket.org/pcas/keyvalue/postgres].
*/
package postgres

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
