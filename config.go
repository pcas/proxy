// Config defines configuration options that can be set on proxies and endpoints.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package proxy

import (
	"bitbucket.org/pcastools/log"
	"errors"
)

// Config describes the configuration options that a proxy can set on an endpoint.
type Config struct {
	SSLCert     []byte // The SSL certificate
	SSLDisabled bool   // Is SSL disabled?
}

// Option sets options on a kvdb server.
type Option interface {
	apply(*proxyOptions) error
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*proxyOptions) error
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *proxyOptions) error {
	return h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*proxyOptions) error) *funcOption {
	return &funcOption{
		f: f,
	}
}

// proxyOptions describes the options that can be set on a proxy.
type proxyOptions struct {
	Log         log.Interface // The logger
	SSLCert     []byte        // The SSL certificate
	SSLDisabled bool          // Is SSL disabled?
}

/////////////////////////////////////////////////////////////////////////
// proxyOptions functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) (*proxyOptions, error) {
	// Create the default options
	opts := &proxyOptions{
		Log: log.Discard,
	}
	// Set the options
	for _, h := range options {
		if err := h.apply(opts); err != nil {
			return nil, err
		}
	}
	// Looks good
	return opts, nil
}

// Config returns the corresponding configuration for these options.
func (opts *proxyOptions) Config() *Config {
	// Create the config
	c := &Config{
		SSLDisabled: opts.SSLDisabled,
	}
	// Make a copy of the SSLCert
	if opts.SSLCert != nil {
		crt := make([]byte, len(opts.SSLCert))
		copy(crt, opts.SSLCert)
		c.SSLCert = crt
	}
	// Return the config
	return c
}

// Log sets the logger.
func Log(lg log.Interface) Option {
	return newFuncOption(func(opts *proxyOptions) error {
		if lg != nil {
			opts.Log = lg
		}
		return nil
	})
}

// SSLCert adds the given SSL public certificate.
func SSLCert(crt []byte) Option {
	// Copy the certificate
	crtCopy := make([]byte, len(crt))
	copy(crtCopy, crt)
	// Return the option
	return newFuncOption(func(opts *proxyOptions) error {
		if len(crtCopy) == 0 {
			return errors.New("missing SSL certificate")
		}
		opts.SSLCert = crtCopy
		return nil
	})
}

// SSLDisabled disables SSL.
func SSLDisabled() Option {
	return newFuncOption(func(opts *proxyOptions) error {
		opts.SSLDisabled = true
		return nil
	})
}

/////////////////////////////////////////////////////////////////////////
// Config functions
/////////////////////////////////////////////////////////////////////////

// Copy returns a copy of the configuration.
func (c *Config) Copy() *Config {
	cc := *c
	// Make a copy of the SSLCert
	if c.SSLCert != nil {
		crt := make([]byte, len(c.SSLCert))
		copy(crt, c.SSLCert)
		cc.SSLCert = crt
	}
	return &cc
}
