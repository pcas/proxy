/*
Package proxy provides a proxy that allows clients, such as Magma, to communicate with the pcas infrastructure. Communication in JSON format.

# Overview

 1. Upon connection the client should write a Handshake.
 2. The client may then write Request objects to the proxy, one at a time.
 3. Upon receiving and processing a Request, the proxy will write a Response.
 4. The client should read the Response before writing the next Request.

# Handshake

The Handshake describes configuration options, and should be written by the client immediately upon connecting to the proxy. The Handshake is of the form:

	{
		"SendLengthPrefix":	bool
	}

If SendLengthPrefix is true then the proxy writes JSON objects to the port as strings of the form

	s\nt

where s is a string representation of an integer N, t is a string containing the JSON, and t has length N bytes. If bool is false then the proxy writes JSON objects to the port as strings of the form

	t

as above, without the length prefix.

# Request

A Request describes a request from the client to the proxy:

	{
		"ID": ULID,
		"Timeout": Duration,
		"Endpoint": string,
		"Operation": string,
		"Arguments": Arguments
	}

All keys are optional.
  - ID is a client-provided request ULID. This client can use this, if desired, to associate requests with responses.
  - Timeout is a timeout duration for handling this request. If Timeout is not specified, or if the Duration is <= 0, then the request will not timeout.
  - Endpoint names the endpoint for this requests.
  - Operation names the operation on this given endpoint. It is an error for Operation to be set, but for Endpoint to be omitted.
  - Arguments are operation-specific arguments for this request. It is an error for Arguments to be set, but for either Operation or Endpoint to be omitted.

Upon handling a Request, proxy will write a Response.

# Response

A Response describes a response from the proxy to a Request from the client:

	{
		"ID": ULID,
		"Endpoint": string,
		"Operation": string,
		"Result": Result,
		"Error": string
	}

Here:
  - ID is the client-provided request ULID set on the Request. If the request ID was omitted from the Request, this will be omitted from the response.
  - Endpoint is the endpoint named in the request. If the endpoint was omitted from the Request, or is equal to the empty string, this will be omitted from the response.
  - Operation is the operation named in the request. If the operation was omitted from the Request, or is equal to the empty string, this will be omitted from the response.
  - Result is the operation-specific result.
  - Error is a string describing any error performing this operation.

Error is set only if the request failed; if Error is not set then the request succeeded. At most one of Result or Error will be set.

# Duration

A Duration is defined by the JSON:

	integer|string

In the case where Duration is given by an integer, it gives the number of nanoseconds in the duration. In the case where Duration is given by a string, this will be converted to a [time.Duration] via [time.ParseDuration].

# ULID

A ULID is defined by the JSON:

	string

which must consist of exactly 26 characters taken from the alphabet:

	0123456789ABCDEFGHJKMNPQRSTVWXYZ

A ULID is used to encode a [bitbucket.org/pcastools/ulid.ULID].

# Available endpoints

The available endpoints can be obtained by submitting a Request with empty Endpoint (and hence empty Operation and Arguments). The Response will have Result of the form:

	{
		"Endpoints": [string, ..., string]
	}

where Endpoints is an array of all available endpoints.

# Available operations

The available operations can be obtained for a given endpoint can be obtained by submitting a Request with Endpoint set (and non-empty) and empty Operation (and hence empty Arguments). The Response will have Result of the form:

	{
		"Operations": [string, ..., string]
	}

where Operations is an array of all available operations for this endpoint.
*/
package proxy

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
