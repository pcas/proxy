// Mongodb provides an endpoint handler for keyvalue database requests backed by mongodb.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package mongodb

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/mongodb"
	"bitbucket.org/pcas/keyvalue/mongodb/mongodbflag"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcas/proxy/internal/keyvalue/cache"
	proxykeyvalue "bitbucket.org/pcas/proxy/keyvalue"
	"bitbucket.org/pcastools/log"
	"context"
)

// endpoint is a proxy/keyvalue endpoint.
type endpoint struct {
	cfg   *mongodb.ClientConfig               // The default client config
	cache *cache.Cache[*mongodb.ClientConfig] // The cache of open connections
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init registers this endpoint.
func init() {
	flags := mongodbflag.NewSet(nil)
	proxykeyvalue.Description{
		Name: "mongodb",
		Create: func(_ context.Context, _ *proxy.Config, lg log.Interface) (proxykeyvalue.Endpoint, error) {
			return newEndpoint(flags.ClientConfig(), lg), nil
		},
		FlagSet: flags,
	}.Register()
}

// createConfig modifies (a copy of) the client configuration, with values updated with those in msg.
func createConfig(cfg *mongodb.ClientConfig, msg proxy.Message) (*mongodb.ClientConfig, error) {
	// Sanity check on the keys
	if err := msg.OnlyKeys("AppName", "Hosts", "Username", "Password",
		"PasswordSet", "ReadConcern", "WriteConcern", "WriteN",
		"WriteJournal"); err != nil {
		return nil, err
	}
	// Pass to a copy
	cfg = cfg.Copy()
	// Add any options set by the user
	if s, ok, err := msg.ToString("AppName"); ok {
		if err != nil {
			return nil, err
		}
		cfg.AppName = s
	}
	if S, ok, err := msg.ToStringSlice("Hosts"); ok {
		if err != nil {
			return nil, err
		}
		cfg.Hosts = S
	}
	if s, ok, err := msg.ToString("Username"); ok {
		if err != nil {
			return nil, err
		}
		cfg.Username = s
	}
	if s, ok, err := msg.ToString("Password"); ok {
		if err != nil {
			return nil, err
		}
		cfg.Password = s
	}
	if b, ok, err := msg.ToBool("PasswordSet"); ok {
		if err != nil {
			return nil, err
		}
		cfg.PasswordSet = b
	}
	if s, ok, err := msg.ToString("ReadConcern"); ok {
		if err != nil {
			return nil, err
		}
		cfg.ReadConcern = s
	}
	if s, ok, err := msg.ToString("WriteConcern"); ok {
		if err != nil {
			return nil, err
		} else if s == "writeN" {
			s = ""
		}
		cfg.WriteConcern = s
	}
	if n, ok, err := msg.ToInt("WriteN"); ok {
		if err != nil {
			return nil, err
		}
		cfg.WriteN = n
	}
	if b, ok, err := msg.ToBool("WriteJournal"); ok {
		if err != nil {
			return nil, err
		}
		cfg.WriteJournal = b
	}
	// Validate the configuration before returning
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// newEndpoint returns a new proxy/keyvalue endpoint.
func newEndpoint(cfg *mongodb.ClientConfig, lg log.Interface) proxykeyvalue.Endpoint {
	lg = log.PrefixWith(lg, "[cache]")
	return &endpoint{
		cfg:   cfg,
		cache: cache.New(mongodb.Open, mongodb.AreEqual, lg),
	}
}

// Close closes the endpoint.
func (e *endpoint) Close() error {
	return e.cache.Close()
}

// Connection returns a connection using the given connection values.
func (e *endpoint) Connection(ctx context.Context, msg proxy.Message) (keyvalue.Connection, error) {
	// Create the client config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, err
	}
	// Recover (or open) a connection from the cache
	return e.cache.Get(ctx, cfg)
}

// Defaults returns the default backend-specific connection values for this endpoint.
//
// The connection values JSON is of the form:
//
//	{
//		"AppName": string,
//		"Hosts": [string, ..., string],
//		"Username": string,
//		"Password": string,
//		"PasswordSet": boolean,
//		"ReadConcern": string,
//		"WriteConcern": string,
//		"WriteN": integer,
//		"WriteJournal: boolean
//	}
func (e *endpoint) Defaults() proxy.Message {
	hosts := e.cfg.Hosts
	if hosts == nil {
		hosts = []string{}
	}
	return proxy.Message{
		"AppName":      e.cfg.AppName,
		"Hosts":        hosts,
		"Username":     e.cfg.Username,
		"Password":     e.cfg.Password,
		"PasswordSet":  e.cfg.PasswordSet,
		"ReadConcern":  e.cfg.ReadConcern,
		"WriteConcern": e.cfg.WriteConcern,
		"WriteN":       e.cfg.WriteN,
		"WriteJournal": e.cfg.WriteJournal,
	}
}
