// endpoint defines the endpoint for metrics

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metrics

import (
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/bufmetrics"
	"bitbucket.org/pcas/metrics/metricsdb"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/convert"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"sync"
)

// endpoint is the rangedb endpoint.
type endpoint struct {
	cfg   *metricsdb.ClientConfig // The default client config
	cache *cache                  // The cache of open connections
	lg    log.Interface           // The logger
	m     sync.Mutex              // Mutex controlling the following
	tags  metrics.Tags            // The default tags
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

func init() {
	// Create the flag set
	flags := metricsdbflag.NewSet(nil)
	// Register the endpoint
	proxy.Description{
		Name: "metrics",
		Operations: []string{
			"defaults",
			"clear_default_tags",
			"add_default_tags",
			"submit",
		},
		Create: func(ctx context.Context, opts *proxy.Config, lg log.Interface) (proxy.Endpoint, error) {
			// Create the config
			cfg := flags.ClientConfig()
			cfg.SSLDisabled = opts.SSLDisabled
			cfg.SSLCert = opts.SSLCert
			// Return the new endpoint
			return newEndpoint(ctx, cfg, lg)
		},
		FlagSet: flags,
	}.Register()
}

// createConfig modifies (a copy of) the client configuration, with values updated with those in msg.
func createConfig(cfg *metricsdb.ClientConfig, msg proxy.Message) (*metricsdb.ClientConfig, error) {
	// Sanity check on the keys
	if err := msg.OnlyKeys("Address"); err != nil {
		return nil, err
	}
	// Pass to a copy
	cfg = cfg.Copy()
	// Add any options set by the user
	if s, ok, err := msg.ToString("Address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	// Validate the configuration before returning
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

// toTags attempts to return the entry for the given key as a [bitbucket.org/pcas/metrics.Tags]. The given set of tags will be modified and returned. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
//
// A Tags is defined by the JSON:
//
//	{
//		"key1": string,
//		...,
//		"keyN": string
//	}
//
// Here key1, ..., keyN are arbitrary keys for the tags. A Tags is used to encode a [bitbucket.org/pcas/metrics.Tags]. See [bitbucket.org/pcas/metrics] for details.
func toTags(msg proxy.Message, key string, tags metrics.Tags) (metrics.Tags, bool, error) {
	// Fetch the JSON-encoded tags
	t, ok, err := msg.ToMessage(key)
	if err != nil || !ok {
		return tags, ok, err
	}
	// Ensure that tags is non-nil
	if tags == nil {
		tags = make(metrics.Tags, len(t))
	}
	// Start adding the key-value tag pairs to the provided tags
	for k, v := range t {
		s, err := convert.ToString(v)
		if err != nil {
			return tags, true, fmt.Errorf("unable to convert value for '%s.%s' to string: %w", key, k, err)
		}
		tags[k] = s
	}
	// Validate the tags
	if _, err := tags.IsValid(); err != nil {
		return nil, true, fmt.Errorf("invalid tags '%s': %w", key, err)
	}
	return tags, true, nil
}

// valueKey extracts the value key from the Field JSON. Assumes that a value key is set.
func valueKey(msg proxy.Message) string {
	for k := range msg {
		switch k {
		case "Boolean", "Duration", "Float", "Integer", "String":
			return k
		}
	}
	return "Unknown"
}

// parseField attempts to convert the given proxy.Message to a Field. That is, to a key-value pair suitable for including in a metrics.Fields.
//
// A Field is defined by the JSON:
//
//	{
//		"Key": string,	// required
//		"Boolean": boolean,
//		"Duration": Duration,
//		"Float": float,
//		"Integer": integer,
//		"String": string
//	}
//
// Exactly one of "Boolean", "Duration", "Float", "Integer", or "String" is required. A Field is used to encode an entry in a [bitbucket.org/pcas/metrics.Fields]. See [bitbucket.org/pcas/metrics] for details.
func parseField(msg proxy.Message) (string, interface{}, error) {
	// Sanity check
	if err := msg.OnlyKeys("Key", "Boolean", "Duration", "Float", "Integer", "String"); err != nil {
		return "", nil, err
	}
	// Fetch the key
	k, ok, err := msg.ToString("Key")
	if err != nil {
		return "", nil, err
	} else if !ok {
		return "", nil, errors.New("missing key 'Key'")
	}
	// Further sanity check
	if len(msg) != 2 {
		return "", nil, errors.New("exactly one of the keys 'Boolean', 'Duration', 'Float', 'Integer', or 'String' is required")
	}
	// Extract the value
	var v interface{}
	switch valueKey(msg) {
	case "Boolean":
		v, _, err = msg.ToBool("Boolean")
	case "Duration":
		v, _, err = msg.ToDuration("Duration")
	case "Float":
		v, _, err = msg.ToFloat64("Float")
	case "Integer":
		v, _, err = msg.ToInt64("Integer")
	case "String":
		v, _, err = msg.ToString("String")
	default:
		err = errors.New("unsupported type")
	}
	return k, v, err
}

// toFields attempts to return the entry for the given key as a metrics.Fields. The second return value will be true if and only if the entry is set, regardless of any errors in type conversion.
//
// The entry is defined by the JSON:
//
//	[Field, ..., Field]
func toFields(msg proxy.Message, key string) (metrics.Fields, bool, error) {
	// Recover the slice of entries that define the fields
	ms, ok, err := msg.ToMessageSlice(key)
	if err != nil || !ok {
		return nil, ok, err
	}
	// Rebuild the fields
	fields := make(metrics.Fields, len(ms))
	for i, m := range ms {
		k, v, err := parseField(m)
		if err != nil {
			return nil, true, fmt.Errorf("error converting element %d of '%s' to a Field: %w", i, key, err)
		}
		fields[k] = v
	}
	// Validate the fields
	if _, err := fields.IsValid(); err != nil {
		return nil, true, fmt.Errorf("invalid fields '%s': %w", key, err)
	}
	return fields, true, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// Close closes the endpoint.
func (e *endpoint) Close() error {
	return e.cache.Close()
}

// defaults returns the default backend-specific Connection values for this endpoint.
//
// The Connection values JSON is of the form:
//
//	{
//		"Address": string
//	}
func (e *endpoint) defaults() proxy.Message {
	return proxy.Message{
		"Address": e.cfg.Address.URI(),
	}
}

// defaultTags returns the defaults tags.
func (e *endpoint) defaultTags() metrics.Tags {
	e.m.Lock()
	defer e.m.Unlock()
	if e.tags == nil {
		return metrics.Tags{}
	}
	return e.tags.Copy()
}

// storage returns the client connection to metricsdb.
func (e *endpoint) storage(ctx context.Context, args proxy.Message) (bufmetrics.Buffer, error) {
	// Extract the connection configuration (if any)
	msg, _, err := args.ToMessage("Connection")
	if err != nil {
		return nil, err
	}
	// Build the config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, err
	}
	// Open the connection to the storage
	m, err := e.cache.Get(ctx, cfg)
	if err != nil {
		return nil, err
	}
	// Set the logger and return
	m.SetLogger(log.PrefixWith(e.Log(), "[connection]"))
	return m, nil
}

// handleDefaults handles defaults requests.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Connection": Connection
//	}
//
// The result describes the default connection values.
func (e *endpoint) handleDefaults(args proxy.Message) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	return proxy.Message{"Connection": e.defaults()}, nil
}

// clearDefaultTags handles clear_default_tags requests.
//
// The Arguments are empty. This will clear any default tags previously set with "add_default_tags". The Result is empty.
func (e *endpoint) clearDefaultTags(args proxy.Message) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	e.m.Lock()
	defer e.m.Unlock()
	e.tags = nil
	return nil, nil
}

// addDefaultTags handles add_default_tags requests.
//
// The Arguments are of the form:
//
//	{
//		"Tags": Tags
//	}
//
// This will add the given tags to the set of default tags, replacing any existing tags with duplicate keys. The Result is empty.
func (e *endpoint) addDefaultTags(args proxy.Message) (proxy.Message, error) {
	if err := args.OnlyKeys("Tags"); err != nil {
		return nil, err
	}
	tags, _, err := toTags(args, "Tags", nil)
	if err != nil {
		return nil, err
	} else if len(tags) == 0 {
		return nil, nil
	}
	e.m.Lock()
	defer e.m.Unlock()
	if e.tags == nil {
		e.tags = tags
	} else {
		for k, v := range tags {
			e.tags[k] = v
		}
	}
	return nil, nil
}

// submit handles submit requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Name": string,					// required
//		"Tags": Tags,
//		"Fields": [Field,...,Field],	// required
//	}
//
// This will submit the named point to the metrics. The default tags will be augmented with any provided tags. The Result is empty.
func (e *endpoint) submit(ctx context.Context, args proxy.Message) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Name", "Tags", "Fields"); err != nil {
		return nil, err
	}
	// Fetch the name
	name, ok, err := args.ToString("Name")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Name'")
	}
	// Fetch the tags
	tags, _, err := toTags(args, "Tags", e.defaultTags())
	if err != nil {
		return nil, err
	}
	// Fetch the fields
	fields, ok, err := toFields(args, "Fields")
	if !ok {
		return nil, errors.New("missing key 'Fields'")
	} else if err != nil {
		return nil, err
	}
	// Create the point
	pt, err := metrics.NewPoint(name, fields, tags)
	if err != nil {
		return nil, err
	}
	// Fetch the metrics.Interface
	m, err := e.storage(ctx, args)
	if err != nil {
		return nil, err
	}
	defer m.Close()
	// Submit the point
	return nil, m.Submit(ctx, pt)
}

// Handle handles requests for operation 'op' with arguments 'args'.
func (e *endpoint) Handle(ctx context.Context, op string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	switch op {
	case "defaults":
		return e.handleDefaults(args)
	case "clear_default_tags":
		return e.clearDefaultTags(args)
	case "add_default_tags":
		return e.addDefaultTags(args)
	case "submit":
		return e.submit(ctx, args)
	default:
		return nil, fmt.Errorf("unknown operation: %s", op)
	}
}

// Log returns the logger.
func (e *endpoint) Log() log.Interface {
	return e.lg
}

// newEndpoint returns a new endpoint using the given default config.
func newEndpoint(ctx context.Context, cfg *metricsdb.ClientConfig, lg log.Interface) (proxy.Endpoint, error) {
	return &endpoint{
		cfg:   cfg,
		cache: newCache(log.PrefixWith(lg, "[cache]")),
		lg:    lg,
	}, nil
}
