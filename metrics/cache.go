// Cache defines a cache of metricsdb client connections.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package metrics

import (
	"bitbucket.org/pcas/metrics/bufmetrics"
	"bitbucket.org/pcas/metrics/metricsdb"
	stdCache "bitbucket.org/pcas/proxy/internal/cache"
	"bitbucket.org/pcastools/log"
	"context"
	"time"
)

// Metrics buffer configuration
const (
	bufSize       = 1024             // The buffer size
	flushInterval = 10 * time.Second // The duration between flushes
	flustTimeout  = 8 * time.Second  // The timeout before abandoning a flush
)

// value is a value in the cache.
type value struct {
	bufmetrics.Buffer
	release func() // The release function
}

// cache is a cache of storage accessed by their config.
type cache struct {
	cache *stdCache.Cache[*metricsdb.ClientConfig, bufmetrics.Buffer]
}

//////////////////////////////////////////////////////////////////////
// value functions
//////////////////////////////////////////////////////////////////////

// Close closes the value.
func (v *value) Close() error {
	v.release()
	return nil
}

//////////////////////////////////////////////////////////////////////
// cache functions
//////////////////////////////////////////////////////////////////////

// Close closes the cache.
func (c *cache) Close() error {
	return c.cache.Close()
}

// Get returns a storage with given config.
func (c *cache) Get(ctx context.Context, cfg *metricsdb.ClientConfig) (bufmetrics.Buffer, error) {
	e, err := c.cache.Get(ctx, cfg)
	if err != nil {
		return nil, err
	}
	return &value{
		Buffer:  e.Value(),
		release: e.Release,
	}, nil
}

// newCache returns a new connection cache.
func newCache(lg log.Interface) *cache {
	return &cache{
		cache: stdCache.New(
			func(ctx context.Context, cfg *metricsdb.ClientConfig) (bufmetrics.Buffer, error) {
				// Open a connection to the metrics
				m, err := metricsdb.NewClient(ctx, cfg)
				if err != nil {
					return nil, err
				}
				// Wrap the connection in a buffer
				b, err := bufmetrics.New(m, bufSize, flushInterval, flustTimeout)
				if err != nil {
					m.Close()
					return nil, err
				}
				return b, nil
			},
			metricsdb.AreEqual,
			lg,
		),
	}
}
