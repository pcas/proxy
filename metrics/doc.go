/*
Package metrics is an endpoint for collecting metrics data. See [bitbucket.org/pcas/metrics].

# Types

First we describe the JSON types specific to this endpoint.

# Connection

The Connection object describes the connection details to metricsdb:

	{
		"Address": string
	}

Any values set in Connection are optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation. For a description of the meaning of these settings, see [bitbucket.org/pcas/metrics/metricsdb].

# Tags

The Tags object is defined by the JSON:

	{
		"key1": string,
		...,
		"keyN": string
	}

Here key1, ..., keyN are arbitrary keys for the tags. A Tags is used to encode a [bitbucket.org/pcas/metrics.Tags]. See [bitbucket.org/pcas/metrics] for details.

# Field

A Field is defined by the JSON:

	{
		"Key": string,	// required
		"Boolean": boolean,
		"Duration": Duration,
		"Float": float,
		"Integer": integer,
		"String": string
	}

Exactly one of "Boolean", "Duration", "Float", "Integer", or "String" is required. A Field is used to encode an entry in a [bitbucket.org/pcas/metrics.Fields]. See [bitbucket.org/pcas/metrics] for details.

# Operations

Now we describe the operations permitted by this endpoint.

# defaults

The Arguments are empty. The Result is of the form:

	{
		"Connection": Connection
	}

The result describes the default connection values.

# clear_default_tags

The Arguments are empty. This will clear any default tags previously set with "add_default_tags". The Result is empty.

# add_default_tags

The Arguments are of the form:

	{
		"Tags": Tags
	}

This will add the given tags to the set of default tags, replacing any existing tags with duplicate keys. The Result is empty.

# submit

The Arguments are of the form:

	{
		"Connection": Connection,
		"Name": string,					// required
		"Tags": Tags,
		"Fields": [Field,...,Field],	// required
	}

This will submit the named point to the metrics. The default tags will be augmented with any provided tags. The Result is empty.
*/
package metrics

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
