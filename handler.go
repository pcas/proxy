// Server.go provides connection handling for proxy

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package proxy

import (
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net"
	"sort"
	"strconv"
	"time"
)

// encoder is an interface satisfied by an object with an Encode method
type encoder interface {
	Encode(interface{}) error
}

// decoder is an interface satisfied by an object with a Decode method
type decoder interface {
	Decode(interface{}) error
}

// lengthPrefixEncoder wraps a json.Encoder so that the size of any JSON precedes the JSON itself.
type lengthPrefixEncoder struct {
	b   bytes.Buffer  // A buffer to hold encoded JSON
	enc *json.Encoder // An Encoder backed by b
	w   io.Writer     // The writer to which length-prefixed JSON is to be written
}

// handleFunc is the Handle method on an Endpoint.
type handleFunc func(context.Context, string, Message, log.Interface) (Message, error)

// handler handles passing requests to endpoints and writing out the results.
type handler struct {
	enc       encoder             // The encoder
	dec       decoder             // The decoder
	cfg       *Config             // The configuration options
	endpoints map[string]Endpoint // The open endpoints
	lg        log.Interface       // The logger
}

// request describes a client's request to the server.
type request struct {
	ID        ulid.ULID     // Client-provided request ID, if any
	Operation string        // The operation, if any
	Endpoint  string        // The endpoint, if any
	Timeout   time.Duration // The timeout (a value <= 0 is taken to mean no timeout)
	Arguments Message       // The arguments, if any
}

// response describes the server's response to a client.
type response struct {
	ID        ulid.ULID `json:",omitempty"` // Client-provided request ID, if any
	Endpoint  string    `json:",omitempty"` // The endpoint, if any
	Operation string    `json:",omitempty"` // The operation, if any
	Result    Message   `json:",omitempty"` // The result, if any
	Error     string    `json:",omitempty"` // The error, if any
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// listEndpoints returns the available endpoints.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Endpoints": [string, ..., string]
//	}
//
// where Endpoints is an array of all available endpoints.
func listEndpoints(args Message) (Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	names := make([]string, 0, len(descriptions))
	for name := range descriptions {
		names = append(names, name)
	}
	sort.Strings(names)
	return Message{
		"Endpoints": names,
	}, nil
}

// listOperations returns the available operations for the named endpoint.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Operations": [string, ..., string]
//	}
//
// where Operations is an array of all available operations for this endpoint.
func listOperations(args Message, name string) (Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	d, ok := descriptions[name]
	if !ok {
		return nil, fmt.Errorf("unknown endpoint: %s", name)
	}
	ops := d.Operations
	if ops == nil {
		ops = []string{}
	}
	return Message{
		"Operations": ops,
	}, nil
}

// execCreationFunc executes the given creation function, recovering from any panics.
func execCreationFunc(ctx context.Context, f CreationFunc, cfg *Config, lg log.Interface) (e Endpoint, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("panic in creation: %v", r)
		}
	}()
	e, err = f(ctx, cfg.Copy(), lg)
	return
}

// execHandleFunc executes the given handle function, recovering from any panics.
func execHandleFunc(ctx context.Context, f handleFunc, op string, args Message, lg log.Interface) (msg Message, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("panic in handler: %v", r)
		}
	}()
	msg, err = f(ctx, op, args, lg)
	return
}

//////////////////////////////////////////////////////////////////////
// request functions
//////////////////////////////////////////////////////////////////////

// toRequest attempts to convert the message to a request object. Note: On error the returned request will be completed with whatever data could be parsed.
func toRequest(m Message) (req request, err error) {
	// Extract the request ID
	if u, ok, myErr := m.ToULID("ID"); myErr != nil {
		if err == nil {
			err = myErr
		}
	} else if ok {
		req.ID = u
	}
	// Extract the timeout
	if d, ok, myErr := m.ToDuration("Timeout"); myErr != nil {
		if err == nil {
			err = myErr
		}
	} else if ok {
		req.Timeout = d
	}
	// Extract the endpoint
	if e, ok, myErr := m.ToString("Endpoint"); myErr != nil {
		if err == nil {
			err = myErr
		}
	} else if ok {
		req.Endpoint = e
	}
	// Extract the operation
	if op, ok, myErr := m.ToString("Operation"); myErr != nil {
		if err == nil {
			err = myErr
		}
	} else if ok {
		req.Operation = op
	}
	// Extract the Arguments
	if args, ok, myErr := m.ToMessage("Arguments"); myErr != nil {
		if err == nil {
			err = myErr
		}
	} else if ok {
		req.Arguments = args
	}
	// Validate the keys in the message
	if err == nil {
		err = m.OnlyKeys("ID", "Timeout", "Endpoint", "Operation", "Arguments")
	}
	// Finally validate the request
	// Validate the request
	if err == nil {
		err = req.Validate()
	}
	return
}

// Validate validates the request.
func (r request) Validate() error {
	// If the endpoint is empty, then the operation must be empty too
	if len(r.Endpoint) == 0 {
		if len(r.Operation) != 0 {
			return errors.New("request has 'Operation' but empty value for 'Endpoint'")
		}
		return nil
	}
	// Fetch the description for this endpoint
	d, ok := descriptions[r.Endpoint]
	if !ok {
		return fmt.Errorf("unknown endpoint: %s", r.Endpoint)
	}
	// If an operation is given, check that it's listed in the description
	if len(r.Operation) != 0 {
		idx := sort.SearchStrings(d.Operations, r.Operation)
		if idx == len(d.Operations) || d.Operations[idx] != r.Operation {
			return fmt.Errorf("unknown operation: %s/%s", r.Endpoint, r.Operation)
		}
	}
	// Looks good
	return nil
}

//////////////////////////////////////////////////////////////////////
// lengthPrefixEncoder functions
//////////////////////////////////////////////////////////////////////

// Encode writes the length of s to the writer underlying e, followed by s, where s is the JSON encoding of v.
func (e *lengthPrefixEncoder) Encode(v interface{}) error {
	// JSON encode v into the buffer
	if err := e.enc.Encode(v); err != nil {
		return err
	}
	// Write the length of the buffer
	if _, err := io.WriteString(e.w, strconv.Itoa(e.b.Len())); err != nil {
		return err
	}
	// Write a new line
	if _, err := io.WriteString(e.w, "\n"); err != nil {
		return err
	}
	// Write the contents of the buffer
	if _, err := e.b.WriteTo(e.w); err != nil {
		return err
	}
	return nil
}

// newLengthPrefixEncoder returns a new encoder that writes length-prefixed JSON to w.
func newLengthPrefixEncoder(w io.Writer) encoder {
	e := &lengthPrefixEncoder{
		w: w,
	}
	e.enc = json.NewEncoder(&e.b)
	return e
}

//////////////////////////////////////////////////////////////////////
// handler functions
//////////////////////////////////////////////////////////////////////

// Close closes any open endpoints on this handler.
func (h *handler) Close() error {
	// Is there anything to do?
	if h.endpoints == nil {
		return nil
	}
	// Work through the endpoints
	var err error
	for name, e := range h.endpoints {
		if closeErr := e.Close(); closeErr != nil {
			h.Log().Printf("Error closing endpoint '%s': %v", name, closeErr)
			if err == nil {
				err = closeErr
			}
		}
	}
	h.endpoints = nil
	// Return any errors
	return err
}

// Log returns the logger for this handler.
func (h *handler) Log() log.Interface {
	if h.lg == nil {
		return log.Discard
	}
	return h.lg
}

// getEndpoint returns the endpoint on this handler with given name, opening it if necessary.
func (h *handler) getEndpoint(ctx context.Context, name string) (Endpoint, error) {
	// Have we already opened this endpoint?
	if h.endpoints != nil {
		if e, ok := h.endpoints[name]; ok {
			return e, nil
		}
	}
	// No luck -- fetch the description for this endpoint
	d, ok := descriptions[name]
	if !ok {
		return nil, fmt.Errorf("unknown endpoint: %s", name)
	}
	// Open the endpoint
	e, err := execCreationFunc(ctx, d.Create, h.cfg, log.PrefixWith(h.Log(), "[%s]", name))
	if err != nil {
		if isContextErr(err) {
			return nil, err
		}
		return nil, fmt.Errorf("error opening endpoint '%s': %w", name, err)
	}
	// Cache the endpoint and return
	if h.endpoints == nil {
		h.endpoints = make(map[string]Endpoint)
	}
	h.endpoints[name] = e
	return e, nil
}

// handleOperation handles the given request, returning the response.
func (h *handler) handleOperation(ctx context.Context, req request, lg log.Interface) (Message, error) {
	// Handle the case where no endpoint or operation is specified
	if len(req.Endpoint) == 0 {
		return listEndpoints(req.Arguments)
	} else if len(req.Operation) == 0 {
		return listOperations(req.Arguments, req.Endpoint)
	}
	// Create the context for this operation
	var cancel context.CancelFunc
	if req.Timeout > 0 {
		ctx, cancel = context.WithTimeout(ctx, req.Timeout)
	} else {
		ctx, cancel = context.WithCancel(ctx)
	}
	defer cancel()
	// Recover the endpoint
	e, err := h.getEndpoint(ctx, req.Endpoint)
	if err != nil {
		return nil, err
	}
	// Call the handler
	return execHandleFunc(ctx, e.Handle, req.Operation, req.Arguments, lg)
}

// handleRequest handles the given request m, returning the response.
func (h *handler) handleRequest(ctx context.Context, m Message) response {
	// Convert the message to a request object. Note: on error req will be
	// completed with whatever data could be parsed.
	req, err := toRequest(m)
	// Create a logger and log this request
	lg := h.Log()
	if !req.ID.IsNil() {
		lg = log.PrefixWith(lg, "[ID=%s]", req.ID)
	}
	lg.Printf("Handling '%s/%s'", req.Endpoint, req.Operation)
	// Populate the header of the response
	resp := response{
		ID:        req.ID,
		Endpoint:  req.Endpoint,
		Operation: req.Operation,
	}
	// Handle any errors
	if err != nil {
		lg.Printf("Error handling request: %v", err)
		resp.Error = err.Error()
		return resp
	}
	// Do the work
	now := time.Now()
	result, err := h.handleOperation(ctx, req, lg)
	// Handle any errors
	if err != nil {
		lg.Printf("Error handling request: %v", err)
		resp.Error = err.Error()
		return resp
	}
	lg.Printf("Operation succeeded in %s", time.Since(now))
	// Add the result to the response
	resp.Result = result
	return resp
}

// Run runs the handler.
func (h *handler) Run(ctx context.Context) error {
	for {
		// Decode the request
		var req Message
		if err := h.dec.Decode(&req); err != nil {
			return fmt.Errorf("error decoding request: %w", err)
		}
		// Handle the request
		resp := h.handleRequest(ctx, req)
		// Encode the response
		if err := h.enc.Encode(resp); err != nil {
			return fmt.Errorf("error encoding response: %w", err)
		}
	}
}

// newHandler returns a new handler wrapping the given connection.
func newHandler(conn net.Conn, cfg *Config, lg log.Interface) (*handler, error) {
	// Create the JSON decoder wrapping conn
	dec := json.NewDecoder(conn)
	// We send and receive int64s, and we do not want these interpreted as
	// floats (otherwise anything over a 53 bit integer will get corrupted)
	dec.UseNumber()
	// The first thing we should read is the handshake block
	var handshake struct {
		SendLengthPrefix bool // Whether we should prefix JSON objects with their length when sending
	}
	if err := dec.Decode(&handshake); err != nil {
		return nil, fmt.Errorf("error reading handshake block: %w", err)
	}
	// Create the appropriate type of encoder
	var enc encoder
	if handshake.SendLengthPrefix {
		enc = newLengthPrefixEncoder(conn)
	} else {
		enc = json.NewEncoder(conn)
	}
	// Return the handler
	return &handler{
		enc: enc,
		dec: dec,
		cfg: cfg,
		lg:  lg,
	}, nil
}
