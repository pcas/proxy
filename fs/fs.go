// fs provides access to the fs remote filesystem

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fs

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/stringsbuilder"
	"bufio"
	"context"
	"crypto"
	"errors"
	"fmt"
	"github.com/nanmu42/limitio"
	"io"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
)

// defaultDirPerm is the permission new directories will be created with.
const defaultDirPerm = os.FileMode(0755)

// defaultFilePerm is the permission new files will be created with.
const defaultFilePerm = os.FileMode(0644)

// maxCatSize is the maximum size (in bytes) allowed when cat-ing a file.
const maxCatSize = 128 * 1048576 // 128MB

// defaultsFunc returns the defaults for connections on this endpoint.
type defaultsFunc func() proxy.Message

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// resolveSymlinks attempts to resolve any symlinks in the path p. This will work even if the base of p does not exist. Returns the resolved path.
func resolveSymlinks(p string) (string, error) {
	// Try to resolve p
	q, err := filepath.EvalSymlinks(p)
	if err == nil {
		return q, nil
	} else if !os.IsNotExist(err) {
		return "", err
	}
	// If we're here then p doesn't exist. Strip off the base and retry.
	q, err = filepath.EvalSymlinks(filepath.Dir(p))
	if err != nil {
		return "", err
	}
	// Append the base and return the path
	return filepath.Join(q, filepath.Base(p)), nil
}

// createOrAssertEmptyDir either creates an empty directory at path p, or asserts that any existing file at path p is an empty directory.
func createOrAssertEmptyDir(p string) error {
	// Open the path
	f, err := os.Open(p)
	if err != nil {
		// If the path doesn't exist, try to create the directory
		if os.IsNotExist(err) {
			return os.Mkdir(p, defaultDirPerm)
		}
		return err
	}
	defer f.Close() // Ignore any errors
	// Check that it's a directory
	if st, err := f.Stat(); err != nil {
		return err
	} else if !st.IsDir() {
		return fserrors.NotDirectory
	}
	// Is it empty?
	_, err = f.Readdir(1)
	if err != nil {
		// An io.EOF means the directory is empty, which is what we want
		if err == io.EOF {
			return nil
		}
		return err
	}
	// If we're here then the directory isn't empty
	return fserrors.DirNotEmpty
}

//////////////////////////////////////////////////////////////////////
// download functions
//////////////////////////////////////////////////////////////////////

// downloadPath downloads the contents of src on s to dst on the local file system. If src is a file then dst must not exist. If src is a directory then either dst must not exist, in which case it will be created, or dst must be an empty directory. In each case the contents of src are downloaded recursively to dst. Any paths within src whose final element begins with a '.' will be ignored.
func downloadPath(ctx context.Context, src string, s fs.Interface, dst string) error {
	// Resolve any symlinks in the destination path
	dst, err := resolveSymlinks(dst)
	if err != nil {
		return err
	}
	// Is this a directory? Otherwise it's a file.
	if md, err := s.DownloadMetadata(ctx, src); err != nil {
		return err
	} else if md.IsDir {
		return downloadDir(ctx, src, s, dst)
	}
	return downloadFile(ctx, src, s, dst)
}

// downloadPathRecursive downloads the contents of src on s to dst on the local file system. If src is a file then dst must not exist. If src is a directory then either dst must not exist, in which case it will be created, or dst must be an empty directory. In each case the contents of src are downloaded recursively to dst. Any src paths whose final element begins with a '.' will be ignored.
func downloadPathRecursive(ctx context.Context, src string, s fs.Interface, dst string) error {
	// Does the source begin with a '.'? In which case we ignore it.
	if strings.HasPrefix(path.Base(src), ".") {
		return nil
	}
	// Is this a directory? Otherwise it's a file.
	if md, err := s.DownloadMetadata(ctx, src); err != nil {
		return err
	} else if md.IsDir {
		return downloadDir(ctx, src, s, dst)
	}
	return downloadFile(ctx, src, s, dst)
}

// downloadFile downloads the contents of src on s to dst on the local file system.
func downloadFile(ctx context.Context, src string, s fs.Interface, dst string) (err error) {
	// Check that the source file exists and is a file
	var ok bool
	if ok, err = fs.IsFile(ctx, s, src); err != nil {
		return err
	} else if !ok {
		err = fserrors.NotFile
		return
	}
	// Open the destination file for writing
	var f io.WriteCloser
	f, err = os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_EXCL, defaultFilePerm)
	if err != nil {
		if os.IsExist(err) {
			err = fserrors.Exist
		}
		return
	}
	defer func() {
		if closeErr := f.Close(); err == nil {
			err = closeErr
		}
	}()
	// Wrap the file in a buffered writer
	w := bufio.NewWriter(f)
	defer func() {
		if flushErr := w.Flush(); err == nil {
			err = flushErr
		}
	}()
	// Perform the copy
	_, err = fs.CopyFrom(ctx, w, s, src)
	return
}

// downloadDir downloads the contents of the directory src on s to dst on the local file system.
func downloadDir(ctx context.Context, src string, s fs.Interface, dst string) (err error) {
	// Open the source directory for iteration
	var itr fs.DirIterator
	if itr, err = s.Dir(ctx, src); err != nil {
		return
	}
	// Defer closing the iterator and checking for errors
	defer func() {
		if closeErr := itr.Close(); err == nil {
			err = closeErr
		}
		if err == nil {
			err = itr.Err()
		}
	}()
	// Check that the destination exists (creating it if necessary) and is an
	// empty directory
	if err = createOrAssertEmptyDir(dst); err != nil {
		return
	}
	// Start iterating
	var md fs.Metadata
	var ok bool
	ok, err = itr.NextContext(ctx)
	for err == nil && ok {
		// Read in the metadata
		if err = itr.Scan(&md); err != nil {
			return
		}
		// Create the new dst path
		newDst := filepath.Join(dst, path.Base(md.Path))
		// Perform the download
		if err = downloadPathRecursive(ctx, md.Path, s, newDst); err != nil {
			return
		}
		// Move on
		ok, err = itr.NextContext(ctx)
	}
	return
}

//////////////////////////////////////////////////////////////////////
// upload functions
//////////////////////////////////////////////////////////////////////

// uploadPath uploads the contents of src on the local file system to dst on s. If src is a file then dst must not exist. If src is a directory then either dst must not exist, in which case it will be created, or dst must be an empty directory. In each case the contents of src are uploaded recursively to dst. In the case that src is a directory then symlinks within src will be ignored. Similarly, any paths within src whose final element begins with a '.' will be ignored.
func uploadPath(ctx context.Context, src string, dst string, s fs.Interface) (err error) {
	// Resolve any symlinks in the source path
	if src, err = filepath.EvalSymlinks(src); err != nil {
		return
	}
	// Open the source
	var fh *os.File
	if fh, err = os.Open(src); err != nil {
		return
	}
	defer func() {
		if closeErr := fh.Close(); err == nil {
			err = closeErr
		}
	}()
	// Recover the file info
	var info os.FileInfo
	if info, err = fh.Stat(); err != nil {
		return
	}
	// Is this a directory?
	if info.IsDir() {
		return uploadDir(ctx, fh, dst, s)
	}
	// Is this a regular file?
	if info.Mode().IsRegular() {
		return uploadFile(ctx, fh, dst, s)
	}
	// We don't know what to do with this file
	err = errors.New("source path is not a directory, regular file, or symlink")
	return
}

// uploadPathRecursive uploads the contents of src on the local file system to dst on s. If src is a file then dst must not exist. If src is a directory then either dst must not exist, in which case it will be created, or dst must be an empty directory. In each case the contents of src are uploaded recursively to dst. Note that only regular files will be uploaded; e.g. symlinks will be ignored. Any src paths whose final element begins with a '.' will be ignored.
func uploadPathRecursive(ctx context.Context, src string, dst string, s fs.Interface) (err error) {
	// Does the source begin with a '.'? In which case we ignore it.
	if strings.HasPrefix(filepath.Base(src), ".") {
		return
	}
	// Open the source
	var fh *os.File
	if fh, err = os.Open(src); err != nil {
		return
	}
	defer func() {
		if closeErr := fh.Close(); err == nil {
			err = closeErr
		}
	}()
	// Recover the file info
	var info os.FileInfo
	if info, err = fh.Stat(); err != nil {
		return
	}
	// Is this a directory?
	if info.IsDir() {
		return uploadDir(ctx, fh, dst, s)
	}
	// Is this a regular file?
	if info.Mode().IsRegular() {
		return uploadFile(ctx, fh, dst, s)
	}
	// We skip this file
	return
}

// uploadFile uploads the contents of r to dst on s.
func uploadFile(ctx context.Context, r io.Reader, dst string, s fs.Interface) error {
	_, err := fs.CopyTo(ctx, s, dst, bufio.NewReader(r))
	return err
}

// uploadDir uploads the contents of the directory dir to dst on s.
func uploadDir(ctx context.Context, dir *os.File, dst string, s fs.Interface) error {
	// Does the destination exist? If so, the destination must be an empty
	// directory. Otherwise, create the directory.
	if ok, err := fs.Exists(ctx, s, dst); err != nil {
		return err
	} else if ok {
		if ok, err = fs.IsDirEmpty(ctx, s, dst); err != nil {
			return err
		} else if !ok {
			return fserrors.DirNotEmpty
		}
	} else if err = s.Mkdir(ctx, dst); err != nil {
		return err
	}
	// Start working through the directory
	var err error
	for err == nil {
		// Fetch the next block of names
		var names []string
		names, err = dir.Readdirnames(128)
		// Work through them
		for _, name := range names {
			// Create the new src and dst paths
			newSrc := filepath.Join(dir.Name(), name)
			newDst := path.Join(dst, name)
			// Perform the upload
			if uploadErr := uploadPathRecursive(ctx, newSrc, newDst, s); uploadErr != nil {
				return uploadErr
			}
		}
	}
	// Clear io.EOF before returning
	if err == io.EOF {
		err = nil
	}
	return err
}

//////////////////////////////////////////////////////////////////////
// handler functions
//////////////////////////////////////////////////////////////////////

// defaults handles defaults requests.
//
// The Arguments are empty. The Result is of the form:
//
//	{
//		"Connection": Connection
//	}
//
// The result describes the default connection values.
func defaults(_ context.Context, args proxy.Message, df defaultsFunc, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys(); err != nil {
		return nil, err
	}
	return proxy.Message{"Connection": df()}, nil
}

// metadata handles metadata requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string			// required
//	}
//
// This returns metadata for the given path. The Result is of the form:
//
//	{
//		"Path": string,
//		"Size": integer,
//		"ModificationTime": integer,
//		"IsDir": boolean
//	}
//
// This describes an [bitbucket.org/pcas/fs.Metadata] object. Here ModificationTime is given by the number of nanoseconds elapsed since January 1, 1970 UTC (i.e. as a Unix time).
func metadata(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path"); err != nil {
		return nil, err
	}
	mt, err := s.DownloadMetadata(ctx, p)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"Path":             mt.Path,
		"Size":             mt.Size,
		"ModificationTime": mt.ModTime.UnixNano(),
		"IsDir":            mt.IsDir,
	}, nil
}

// exists handles exists requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string			// required
//	}
//
// Determines whether the given path exists. See [bitbucket.org/pcas/fs.Exists] for details. The Result is of the form:
//
//	{
//		"Exists": boolean
//	}
func exists(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path"); err != nil {
		return nil, err
	}
	ok, err := fs.Exists(ctx, s, p)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"Exists": ok,
	}, nil
}

// isFile handles is_file requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string			// required
//	}
//
// Determines whether the given path exists and is a file. See [bitbucket.org/pcas/fs.IsFile] for details. The Result is of the form:
//
//	{
//		"IsFile": boolean
//	}
func isFile(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path"); err != nil {
		return nil, err
	}
	ok, err := fs.IsFile(ctx, s, p)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"IsFile": ok,
	}, nil
}

// isDir handles is_dir requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string			// required
//	}
//
// Determines whether the given path exists and is a directory. See [bitbucket.org/pcas/fs.IsDir] for details. The Result is of the form:
//
//	{
//		"IsDir": boolean
//	}
func isDir(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path"); err != nil {
		return nil, err
	}
	ok, err := fs.IsDir(ctx, s, p)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"IsDir": ok,
	}, nil
}

// isEmptyDir handles is_empty_dir requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string			// required
//	}
//
// Determines whether the given path exists and is an empty directory. See [bitbucket.org/pcas/fs.IsDirEmpty] for details. The Result is of the form:
//
//	{
//		"IsEmptyDir": boolean
//	}
func isEmptyDir(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path"); err != nil {
		return nil, err
	}
	ok, err := fs.IsDirEmpty(ctx, s, p)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"IsEmptyDir": ok,
	}, nil
}

// mkdir handles mkdir requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string,			// required
//		"Recursive": boolean
//	}
//
// This will create the given directory. If Recursive is true then intermediate subdirectories will be creates as necessary. See [bitbucket.org/pcas/fs.Interface] and [bitbucket.org/pcas/fs.MkdirRecursive] for details. The Result is empty.
func mkdir(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path", "Recursive"); err != nil {
		return nil, err
	}
	recursive, _, err := args.ToBool("Recursive")
	if err != nil {
		return nil, err
	}
	if recursive {
		return nil, fs.MkdirRecursive(ctx, s, p)
	}
	return nil, s.Mkdir(ctx, p)
}

// listDir handles list_dir requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string			// required
//	}
//
// This will list the contents of the given directory. The Result is of the form:
//
//	{
//		"Listing": [string, ..., string]
//	}
//
// Here Listing contains the base names of the contents of the directory.
func listDir(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path"); err != nil {
		return nil, err
	}
	itr, err := s.Dir(ctx, p)
	if err != nil {
		return nil, err
	}
	defer itr.Close()
	var md fs.Metadata
	names := make([]string, 0)
	ok, err := itr.NextContext(ctx)
	for err == nil && ok {
		if err = itr.Scan(&md); err != nil {
			return nil, err
		}
		names = append(names, path.Base(md.Path))
		ok, err = itr.NextContext(ctx)
	}
	if err != nil {
		return nil, err
	}
	sort.Strings(names)
	return proxy.Message{
		"Listing": names,
	}, nil
}

// handleHash handles hash requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string,			// required
//		"HashName": string		// required
//	}
//
// Computes the hex-encoded hash of the file with indicated path. Here HashName is the name of the hash to use. The following (case insensitive) hash names are supported (the corresponding hash is indicated after each name: see [crypto] for details):
//
//	"MD5"			// crypto.MD5
//	"SHA1"			// crypto.SHA1
//	"SHA224"		// crypto.SHA224
//	"SHA256"		// crypto.SHA256
//	"SHA384"		// crypto.SHA384
//	"SHA512"		// crypto.SHA512
//	"SHA512/224"	// crypto.SHA512_224
//	"SHA512/256"	// crypto.SHA512_256
//	"SHA3-224"		// crypto.SHA3_224
//	"SHA3-256"		// crypto.SHA3_256
//	"SHA3-384"		// crypto.SHA3_384
//	"SHA3-512"		// crypto.SHA3_512
//	"BLAKE2s-256"	// crypto.BLAKE2s_256
//	"BLAKE2b-256"	// crypto.BLAKE2b_256
//	"BLAKE2b-384"	// crypto.BLAKE2b_384
//	"BLAKE2b-512"	// crypto.BLAKE2b_512
//
// The Result is of the form:
//
//	{
//		"Hash": string
//	}
func handleHash(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path", "HashName"); err != nil {
		return nil, err
	}
	hashName, ok, err := args.ToString("HashName")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'HashName'")
	}
	var h crypto.Hash
	switch strings.ToUpper(hashName) {
	case "MD5":
		h = crypto.MD5
	case "SHA1":
		h = crypto.SHA1
	case "SHA224":
		h = crypto.SHA224
	case "SHA256":
		h = crypto.SHA256
	case "SHA384":
		h = crypto.SHA384
	case "SHA512":
		h = crypto.SHA512
	case "SHA512/224":
		h = crypto.SHA512_224
	case "SHA512/256":
		h = crypto.SHA512_256
	case "SHA3-224":
		h = crypto.SHA3_224
	case "SHA3-256":
		h = crypto.SHA3_256
	case "SHA3-384":
		h = crypto.SHA3_384
	case "SHA3-512":
		h = crypto.SHA3_512
	case "BLAKE2S-256":
		h = crypto.BLAKE2s_256
	case "BLAKE2B-256":
		h = crypto.BLAKE2b_256
	case "BLAKE2B-384":
		h = crypto.BLAKE2b_384
	case "BLAKE2B-512":
		h = crypto.BLAKE2b_512
	default:
		return nil, fmt.Errorf("unknown hash name: %s", hashName)
	}
	val, err := fs.Hash(ctx, s, p, h)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"Hash": fmt.Sprintf("%x", val),
	}, nil
}

// handleCat handles cat requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string,			// required
//		"MaxSize": integer
//	}
//
// MaxSize is the maximum size (in bytes) to read from the file. There is an upper limit of 134217728 bytes (128MB) which cannot be exceeded. The Result is of the form:
//
//	{
//		"Content": string
//	}
//
// Here Content is the content of the file with indicated path.
func handleCat(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Path", "MaxSize"); err != nil {
		return nil, err
	}
	// Recover the maximum number of bytes to read
	maxSize, ok, err := args.ToInt("MaxSize")
	if err != nil {
		return nil, err
	} else if !ok {
		maxSize = maxCatSize
	} else if maxSize <= 0 {
		return nil, errors.New("the maximum number of bytes to read must be positive")
	} else if maxSize > maxCatSize {
		return nil, fmt.Errorf("the maximum number of bytes to read cannot exceed %d", maxCatSize)
	}
	// Create a string builder to hold the data
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	// Read in the data
	_, err = fs.CopyFrom(ctx, limitio.NewWriter(b, maxSize, false), s, p)
	if err != nil {
		return nil, err
	}
	return proxy.Message{
		"Content": b.String(),
	}, nil
}

// replace handles replace requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string,				// required
//		"Data": string				// required
//	}
//
// Uploads the given data to a file at the given path. If a file already exists with this path then the content will be replaced with Data. The Result is empty.
func replace(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Path", "Data"); err != nil {
		return nil, err
	}
	// Read in the data
	data, ok, err := args.ToString("Data")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Data'")
	}
	// Write the data to a bytes buffer
	b := bytesbuffer.NewString(data)
	defer bytesbuffer.Reuse(b)
	// Delete any existing file
	fs.RemoveFile(ctx, s, p) // Ignore any error
	// Write the data to the file
	_, err = fs.CopyTo(ctx, s, p, b)
	return nil, err
}

// handleDelete handles delete requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string,			// required
//		"Recursive": boolean
//	}
//
// This will delete the given file or (empty) directory. If Recursive is true then, in the case where the path points to a directory, the contents of that directory will be deleted too. See [bitbucket.org/pcas/fs.Inserface] and [bitbucket.org/pcas/fs.RemoveAll] for details. The Result is empty.
func handleDelete(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Path", "Recursive"); err != nil {
		return nil, err
	}
	recursive, _, err := args.ToBool("Recursive")
	if err != nil {
		return nil, err
	}
	if recursive {
		return nil, fs.RemoveAll(ctx, s, p)
	}
	return nil, s.Remove(ctx, p)
}

// handleCopy handles copy requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Source": string,		// required
//		"Destination: string,	// required
//		"Recursive": boolean
//	}
//
// This will copy the given file at the path Source to the path Destination. If Recursive is true then, in the case where the source path points to a directory, the contents of that directory will recursively copied. See [bitbucket.org/pcas/fs.Copy] and [bitbucket.org/pcas/fs.CopyAll] for details. The Result is empty.
func handleCopy(ctx context.Context, s fs.Interface, src string, dst string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Source", "Destination", "Recursive"); err != nil {
		return nil, err
	}
	recursive, _, err := args.ToBool("Recursive")
	if err != nil {
		return nil, err
	}
	if recursive {
		return nil, fs.CopyAll(ctx, s, dst, src)
	}
	_, err = fs.Copy(ctx, s, dst, src)
	return nil, err
}

// download handles download requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Source: string,		// required
//		"Destination": string	// requires
//	}
//
// Here Source is the source path on the connection; Destination is the destination path on the local file system. If the source path points to a file then the destination path must not point to an already existing file or directory. If the source path points to a directory then either the destination path must not exist, in which case it will be created, or the destination path must point to an empty directory. In each case the contents of the source path are downloaded recursively to the destination path. Any paths within the source directory whose final element begins with a '.' will be ignored. The Result is empty.
func download(ctx context.Context, s fs.Interface, src string, dst string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Source", "Destination"); err != nil {
		return nil, err
	}
	return nil, downloadPath(ctx, src, s, dst)
}

// upload handles upload requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Source: string,		// required
//		"Destination": string	// requires
//	}
//
// Here Source is the source path on the local file system; Destination is the destination path on the connection. If the source path points to a file then the destination path must not point to an already existing file or directory. If the source path points to a directory then either the destination path must not exist, in which case it will be created, or the destination path must point to an empty directory. In each case the contents of the source path are uploaded recursively to the destination path. In the case that the source path points to a directory then symlinks within the source directory will be ignored. Similarly, any paths within the source directory whose final element begins with a '.' will be ignored. The Result is empty.
func upload(ctx context.Context, s fs.Interface, src string, dst string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	if err := args.OnlyKeys("Connection", "Source", "Destination"); err != nil {
		return nil, err
	}
	return nil, uploadPath(ctx, src, dst, s)
}
