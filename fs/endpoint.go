// endpoint defines the endpoint for fs requests

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fs

import (
	"bitbucket.org/pcas/fs"
	fserrors "bitbucket.org/pcas/fs/errors"
	"bitbucket.org/pcas/fs/fsd"
	"bitbucket.org/pcas/fs/fsd/fsdflag"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"io"
	"path"
)

// storage is an fs.Interface with a logger and close method.
type storage interface {
	fs.Interface
	io.Closer
	log.Logable
}

// workingdir records the current working directory for a client connection.
type workingdir struct {
	cfg *fsd.ClientConfig
	p   string
}

// endpoint is the fsd endpoint.
type endpoint struct {
	cfg   *fsd.ClientConfig     // The default client config
	cache *cache                // The cache of open connections
	cwd   map[uint32]workingdir // The map of working directories
	lg    log.Interface         // The logger
}

// sourceAndDestOpts describe how the source and destination paths should be treated by withStorageSourceAndDest.
type sourceAndDestOpts struct {
	LocalSource bool // Is the source on the local file system?
	LocalDest   bool // Is the destination on the local file system?
}

// handleStorageAndPathFunc is a handle function that takes a storage and path. The path will be an absolute path.
type handleStorageAndPathFunc func(context.Context, fs.Interface, string, proxy.Message, log.Interface) (proxy.Message, error)

// handleStorageSourceAndDestFunc is a handle function that takes a storage, source path (the first string argument), and destination path (the second string argument).
type handleStorageSourceAndDestFunc func(context.Context, fs.Interface, string, string, proxy.Message, log.Interface) (proxy.Message, error)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

func init() {
	// Create the flag set
	flags := fsdflag.NewSet(nil)
	// Register the endpoint
	proxy.Description{
		Name: "fs",
		Operations: []string{
			"defaults",
			"get_working_dir",
			"set_working_dir",
			"metadata",
			"exists",
			"is_file",
			"is_dir",
			"is_empty_dir",
			"mkdir",
			"list_dir",
			"hash",
			"delete",
			"cat",
			"replace",
			"copy",
			"upload",
			"download",
		},
		Create: func(ctx context.Context, opts *proxy.Config, lg log.Interface) (proxy.Endpoint, error) {
			// Create the config
			cfg := flags.ClientConfig()
			cfg.SSLDisabled = opts.SSLDisabled
			cfg.SSLCert = opts.SSLCert
			// Return the new endpoint
			return newEndpoint(ctx, cfg, lg)
		},
		FlagSet: flags,
	}.Register()
}

// createConfig modifies (a copy of) the client configuration, with values updated with those in msg.
func createConfig(cfg *fsd.ClientConfig, msg proxy.Message) (*fsd.ClientConfig, error) {
	// Sanity check on the keys
	if err := msg.OnlyKeys("Address"); err != nil {
		return nil, err
	}
	// Pass to a copy
	cfg = cfg.Copy()
	// Add any options set by the user
	if s, ok, err := msg.ToString("Address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	// Validate the configuration before returning
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// Close closes the endpoint.
func (e *endpoint) Close() error {
	return e.cache.Close()
}

// defaults returns the default backend-specific Connection values for this endpoint.
//
// The Connection values JSON is of the form:
//
//	{
//		"Address": string
//	}
func (e *endpoint) defaults() proxy.Message {
	return proxy.Message{
		"Address": e.cfg.Address.URI(),
	}
}

// handleGetWorkingDir handles get_working_dir requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//	}
//
// This returns the current working directory. The Result is of the form:
//
//	{
//		"Path": string
//	}
func (e *endpoint) handleGetWorkingDir(_ context.Context, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection"); err != nil {
		return nil, err
	}
	// Extract the connection configuration (if any)
	msg, _, err := args.ToMessage("Connection")
	if err != nil {
		return nil, err
	}
	// Build the config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, err
	}
	// Return the working directory
	return proxy.Message{
		"Path": e.getWorkingDirectory(cfg),
	}, nil
}

// handleSetWorkingDir handles set_working_dir requests.
//
// The Arguments are of the form:
//
//	{
//		"Connection": Connection,
//		"Path": string			// required
//	}
//
// This sets the current working directory. The Result is empty.
func (e *endpoint) handleSetWorkingDir(ctx context.Context, s fs.Interface, p string, args proxy.Message, _ log.Interface) (proxy.Message, error) {
	// Sanity check
	if err := args.OnlyKeys("Connection", "Path"); err != nil {
		return nil, err
	}
	// Check that the path points to a directory
	ok, err := fs.IsDir(ctx, s, p)
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, fserrors.NotDirectory
	}
	// Extract the connection configuration (if any)
	msg, _, err := args.ToMessage("Connection")
	if err != nil {
		return nil, err // This should never happen
	}
	// Build the config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, err // This should never happen
	}
	// Set the working directory
	e.setWorkingDirectory(p, cfg)
	return nil, nil
}

// getWorkingDirectory returns the current working directory for the indicated client connection.
func (e *endpoint) getWorkingDirectory(cfg *fsd.ClientConfig) string {
	// If necessary create the working directory map
	if e.cwd == nil {
		e.cwd = make(map[uint32]workingdir)
	}
	// Get the hash
	h := cfg.Hash()
	// Is the working directory in the cache? Allow for the possibility of a
	// hash collision.
	wd, ok := e.cwd[h]
	for ok && !fsd.AreEqual(cfg, wd.cfg) {
		h++
		wd, ok = e.cwd[h]
	}
	if !ok {
		// No luck -- create the default working directory and add it
		wd = workingdir{
			cfg: cfg,
			p:   "/",
		}
		e.cwd[h] = wd
	}
	// Return the working directory
	return wd.p
}

// setWorkingDirectory sets the current working directory for the indicated client connection.
func (e *endpoint) setWorkingDirectory(p string, cfg *fsd.ClientConfig) {
	// If necessary create the working directory map
	if e.cwd == nil {
		e.cwd = make(map[uint32]workingdir)
	}
	// Get the hash
	h := cfg.Hash()
	// Is the working directory in the cache? Allow for the possibility of a
	// hash collision.
	wd, ok := e.cwd[h]
	for ok && !fsd.AreEqual(cfg, wd.cfg) {
		h++
		wd, ok = e.cwd[h]
	}
	// Set the working directory
	e.cwd[h] = workingdir{
		cfg: cfg,
		p:   p,
	}
}

// storage returns the connection to the storage, along with the current working directory.
func (e *endpoint) storage(ctx context.Context, args proxy.Message) (storage, string, error) {
	// Extract the connection configuration (if any)
	msg, _, err := args.ToMessage("Connection")
	if err != nil {
		return nil, "", err
	}
	// Build the config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, "", err
	}
	// Open the connection to the storage
	s, err := e.cache.Get(ctx, cfg)
	if err != nil {
		return nil, "", err
	}
	// Set the logger and return
	s.SetLogger(log.PrefixWith(e.Log(), "[connection]"))
	return s, e.getWorkingDirectory(cfg), nil
}

// withStorageAndPath calls the given handle function with storage and path on the storage.
func (e *endpoint) withStorageAndPath(ctx context.Context, f handleStorageAndPathFunc, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	// Fetch the path
	p, ok, err := args.ToString("Path")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Path'")
	}
	// Fetch the storage
	s, wd, err := e.storage(ctx, args)
	if err != nil {
		return nil, err
	}
	defer s.Close()
	// Ensure that the path is absolute
	if !path.IsAbs(p) {
		p = path.Join(wd, p)
	}
	// Call the handle function
	return f(ctx, s, p, args, lg)
}

// withStorageSourceAndDest calls the given handle function with storage, source path, and destination path.
func (e *endpoint) withStorageSourceAndDest(ctx context.Context, f handleStorageSourceAndDestFunc, args proxy.Message, opts sourceAndDestOpts, lg log.Interface) (proxy.Message, error) {
	// Fetch the paths
	src, ok, err := args.ToString("Source")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Source'")
	}
	dst, ok, err := args.ToString("Destination")
	if err != nil {
		return nil, err
	} else if !ok {
		return nil, errors.New("missing key 'Destination'")
	}
	// Fetch the storage
	s, cwd, err := e.storage(ctx, args)
	if err != nil {
		return nil, err
	}
	defer s.Close()
	// Ensure that the remote paths are absolute
	if !opts.LocalSource && !path.IsAbs(src) {
		src = path.Join(cwd, src)
	}
	if !opts.LocalDest && !path.IsAbs(dst) {
		dst = path.Join(cwd, dst)
	}
	// Call the handle function
	return f(ctx, s, src, dst, args, lg)
}

// Handle handles requests for operation 'op' with arguments 'args'.
func (e *endpoint) Handle(ctx context.Context, op string, args proxy.Message, lg log.Interface) (proxy.Message, error) {
	switch op {
	case "defaults":
		return defaults(ctx, args, e.defaults, lg)
	case "get_working_dir":
		return e.handleGetWorkingDir(ctx, args, lg)
	case "set_working_dir":
		return e.withStorageAndPath(ctx, e.handleSetWorkingDir, args, lg)
	case "metadata":
		return e.withStorageAndPath(ctx, metadata, args, lg)
	case "exists":
		return e.withStorageAndPath(ctx, exists, args, lg)
	case "is_file":
		return e.withStorageAndPath(ctx, isFile, args, lg)
	case "is_dir":
		return e.withStorageAndPath(ctx, isDir, args, lg)
	case "is_empty_dir":
		return e.withStorageAndPath(ctx, isEmptyDir, args, lg)
	case "mkdir":
		return e.withStorageAndPath(ctx, mkdir, args, lg)
	case "list_dir":
		return e.withStorageAndPath(ctx, listDir, args, lg)
	case "hash":
		return e.withStorageAndPath(ctx, handleHash, args, lg)
	case "cat":
		return e.withStorageAndPath(ctx, handleCat, args, lg)
	case "replace":
		return e.withStorageAndPath(ctx, replace, args, lg)
	case "delete":
		return e.withStorageAndPath(ctx, handleDelete, args, lg)
	case "copy":
		return e.withStorageSourceAndDest(ctx, handleCopy, args, sourceAndDestOpts{
			LocalSource: false,
			LocalDest:   false,
		}, lg)
	case "upload":
		return e.withStorageSourceAndDest(ctx, upload, args, sourceAndDestOpts{
			LocalSource: true,
			LocalDest:   false,
		}, lg)
	case "download":
		return e.withStorageSourceAndDest(ctx, download, args, sourceAndDestOpts{
			LocalSource: false,
			LocalDest:   true,
		}, lg)
	default:
		return nil, fmt.Errorf("unknown operation: %s", op)
	}
}

// Log returns the logger.
func (e *endpoint) Log() log.Interface {
	return e.lg
}

// newEndpoint returns a new endpoint using the given default config.
func newEndpoint(ctx context.Context, cfg *fsd.ClientConfig, lg log.Interface) (proxy.Endpoint, error) {
	return &endpoint{
		cfg:   cfg,
		cache: newCache(log.PrefixWith(lg, "[cache]")),
		lg:    lg,
	}, nil
}
