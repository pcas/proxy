/*
Package fs is an endpoint for fsd. See [bitbucket.org/pcas/fs].

# Types

First we describe the JSON types specific to this endpoint.

# Connection

The Connection object describes the connection details to fsd:

	{
		"Address": string
	}

Any values set in Connection are optional; they overwrite the default Connection values for the endpoint. The default values can be obtained via the "defaults" operation. For a description of the meaning of these settings, see [bitbucket.org/pcas/fs/fsd].

# Operations

Now we describe the operations permitted by this endpoint.

# defaults

The Arguments are empty. The Result is of the form:

	{
		"Connection": Connection
	}

The result describes the default connection values.

# get_working_dir

The Arguments are of the form:

	{
		"Connection": Connection,
	}

This returns the current working directory. The Result is of the form:

	{
		"Path": string
	}

# set_working_dir

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string			// required
	}

This sets the current working directory. The Result is empty.

# metadata

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string			// required
	}

This returns metadata for the given path. The Result is of the form:

	{
		"Path": string,
		"Size": integer,
		"ModificationTime": integer,
		"IsDir": boolean
	}

This describes an [bitbucket.org/pcas/fs.Metadata] object. Here ModificationTime is given by the number of nanoseconds elapsed since January 1, 1970 UTC (i.e. as a Unix time).

# exists

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string			// required
	}

Determines whether the given path exists. See [bitbucket.org/pcas/fs.Exists] for details. The Result is of the form:

	{
		"Exists": boolean
	}

# is_file

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string			// required
	}

Determines whether the given path exists and is a file. See [bitbucket.org/pcas/fs.IsFile] for details. The Result is of the form:

	{
		"IsFile": boolean
	}

# is_dir

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string			// required
	}

Determines whether the given path exists and is a directory. See [bitbucket.org/pcas/fs.IsDir] for details. The Result is of the form:

	{
		"IsDir": boolean
	}

# is_empty_dir

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string			// required
	}

Determines whether the given path exists and is an empty directory. See [bitbucket.org/pcas/fs.IsDirEmpty] for details. The Result is of the form:

	{
		"IsEmptyDir": boolean
	}

# mkdir

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string,			// required
		"Recursive": boolean
	}

This will create the given directory. If Recursive is true then intermediate subdirectories will be creates as necessary. See [bitbucket.org/pcas/fs.Interface] and [bitbucket.org/pcas/fs.MkdirRecursive] for details. The Result is empty.

# list_dir

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string			// required
	}

This will list the contents of the given directory. The Result is of the form:

	{
		"Listing": [string, ..., string]
	}

Here Listing contains the base names of the contents of the directory.

# hash

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string,			// required
		"HashName": string		// required
	}

Computes the hex-encoded hash of the file with indicated path. Here HashName is the name of the hash to use. The following (case insensitive) hash names are supported (the corresponding hash is indicated after each name: see [crypto] for details):

	"MD5"			// crypto.MD5
	"SHA1"			// crypto.SHA1
	"SHA224"		// crypto.SHA224
	"SHA256"		// crypto.SHA256
	"SHA384"		// crypto.SHA384
	"SHA512"		// crypto.SHA512
	"SHA512/224"	// crypto.SHA512_224
	"SHA512/256"	// crypto.SHA512_256
	"SHA3-224"		// crypto.SHA3_224
	"SHA3-256"		// crypto.SHA3_256
	"SHA3-384"		// crypto.SHA3_384
	"SHA3-512"		// crypto.SHA3_512
	"BLAKE2s-256"	// crypto.BLAKE2s_256
	"BLAKE2b-256"	// crypto.BLAKE2b_256
	"BLAKE2b-384"	// crypto.BLAKE2b_384
	"BLAKE2b-512"	// crypto.BLAKE2b_512

The Result is of the form:

	{
		"Hash": string
	}

# cat

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string,			// required
		"MaxSize": integer
	}

MaxSize is the maximum size (in bytes) to read from the file. There is an upper limit of 134217728 bytes (128MB) which cannot be exceeded. The Result is of the form:

	{
		"Content": string
	}

Here Content is the content of the file with indicated path.

# replace

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string,				// required
		"Data": string				// required
	}

Uploads the given data to a file at the given path. If a file already exists with this path then the content will be replaced with Data. The Result is empty.

# delete

The Arguments are of the form:

	{
		"Connection": Connection,
		"Path": string,			// required
		"Recursive": boolean
	}

This will delete the given file or (empty) directory. If Recursive is true then, in the case where the path points to a directory, the contents of that directory will be deleted too. See [bitbucket.org/pcas/fs.Interface] and [bitbucket.org/pcas/fs.RemoveAll] for details. The Result is empty.

# copy

The Arguments are of the form:

	{
		"Connection": Connection,
		"Source": string,		// required
		"Destination: string,	// required
		"Recursive": boolean
	}

This will copy the given file at the path Source to the path Destination. If Recursive is true then, in the case where the source path points to a directory, the contents of that directory will recursively copied. See [bitbucket.org/pcas/fs.Copy] and [bitbucket.org/pcas/fs.CopyAll] for details. The Result is empty.

# download

The Arguments are of the form:

	{
		"Connection": Connection,
		"Source: string,		// required
		"Destination": string	// requires
	}

Here Source is the source path on the connection; Destination is the destination path on the local file system. If the source path points to a file then the destination path must not point to an already existing file or directory. If the source path points to a directory then either the destination path must not exist, in which case it will be created, or the destination path must point to an empty directory. In each case the contents of the source path are downloaded recursively to the destination path. Any paths within the source directory whose final element begins with a '.' will be ignored. The Result is empty.

# upload

The Arguments are of the form:

	{
		"Connection": Connection,
		"Source: string,		// required
		"Destination": string	// requires
	}

Here Source is the source path on the local file system; Destination is the destination path on the connection. If the source path points to a file then the destination path must not point to an already existing file or directory. If the source path points to a directory then either the destination path must not exist, in which case it will be created, or the destination path must point to an empty directory. In each case the contents of the source path are uploaded recursively to the destination path. In the case that the source path points to a directory then symlinks within the source directory will be ignored. Similarly, any paths within the source directory whose final element begins with a '.' will be ignored. The Result is empty.
*/
package fs

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/
