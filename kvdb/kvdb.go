// Kvdb provides an endpoint handler for keyvalue database requests backed by kvdb.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package kvdb

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/kvdb/kvdbflag"
	"bitbucket.org/pcas/proxy"
	"bitbucket.org/pcas/proxy/internal/keyvalue/cache"
	proxykeyvalue "bitbucket.org/pcas/proxy/keyvalue"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/log"
	"context"
)

// endpoint is a proxy/keyvalue endpoint.
type endpoint struct {
	cfg   *kvdb.ClientConfig               // The default client config
	opts  *proxy.Config                    // The proxy options
	cache *cache.Cache[*kvdb.ClientConfig] // The cache of open connections
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init registers this endpoint.
func init() {
	flags := kvdbflag.NewSet(nil)
	proxykeyvalue.Description{
		Name: "kvdb",
		Create: func(_ context.Context, opts *proxy.Config, lg log.Interface) (proxykeyvalue.Endpoint, error) {
			return newEndpoint(flags.ClientConfig(), opts, lg), nil
		},
		FlagSet: flags,
	}.Register()
}

// createConfig modifies (a copy of) the client configuration, with values updated with those in msg.
func createConfig(cfg *kvdb.ClientConfig, msg proxy.Message) (*kvdb.ClientConfig, error) {
	// Sanity check on the keys
	if err := msg.OnlyKeys("Address"); err != nil {
		return nil, err
	}
	// Pass to a copy
	cfg = cfg.Copy()
	// Add any options set by the user
	if s, ok, err := msg.ToString("Address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	// Validate the configuration before returning
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	return cfg, nil
}

//////////////////////////////////////////////////////////////////////
// endpoint functions
//////////////////////////////////////////////////////////////////////

// newEndpoint returns a new proxy/keyvalue endpoint.
func newEndpoint(cfg *kvdb.ClientConfig, opts *proxy.Config, lg log.Interface) proxykeyvalue.Endpoint {
	lg = log.PrefixWith(lg, "[cache]")
	return &endpoint{
		cfg:   cfg,
		opts:  opts,
		cache: cache.New(kvdb.Open, kvdb.AreEqual, lg),
	}
}

// Close closes the endpoint.
func (e *endpoint) Close() error {
	return e.cache.Close()
}

// Connection returns a connection using the given connection values.
func (e *endpoint) Connection(ctx context.Context, msg proxy.Message) (keyvalue.Connection, error) {
	// Create the client config
	cfg, err := createConfig(e.cfg, msg)
	if err != nil {
		return nil, err
	}
	cfg.SSLCert = e.opts.SSLCert
	cfg.SSLDisabled = e.opts.SSLDisabled
	// Recover (or open) a connection from the cache
	return e.cache.Get(ctx, cfg)
}

// Defaults returns the default backend-specific connection values for this endpoint.
//
// The connection values JSON is of the form:
//
//	{
//		"Address": string
//	}
func (e *endpoint) Defaults() proxy.Message {
	return proxy.Message{
		"Address": e.cfg.Address.URI(),
	}
}
